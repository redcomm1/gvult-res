<?php
/*
Plugin Name: Info about cities
Description: You can find info about city district, subway and more
Author: Valentin Drany
Version: 0.0.3
*/

register_activation_hook(__FILE__, 'cities_install');
function cities_install()
{
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	// Проверяем наличие таблици стран, если нет создаем ее
	$table_name = $wpdb->prefix . 'infocities_country';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id_country MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
			name_country VARCHAR(255),
			PRIMARY KEY  (id_country)
		);";
		
		dbDelta($sql);

		// Заполняем таблицу первоночальными данными	
		$wpdb->query($wpdb->prepare("INSERT INTO $table_name VALUES (%d, %s)", 1, 'Украина'));

	}

	// Проверяем наличие таблици городов, если нет создаем ее
	$table_name = $wpdb->prefix . 'infocities_cities';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id_city MEDIUMINT(9)	NOT NULL AUTO_INCREMENT,
			id_country INT(11),
			name_city VARCHAR(255),
			PRIMARY KEY  (id_city)
		);";

		dbDelta($sql);

		// Заполняем таблицу первоночальными данными	
		$wpdb->query("INSERT INTO $table_name VALUES (1, 1, 'Киев')");
	}	

	// // Проверяем наличие таблици районов города, если нет создаем ее
	$table_name = $wpdb->prefix . 'infocities_district';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id_district MEDIUMINT(9)	NOT NULL AUTO_INCREMENT,
			id_city INT(11),
			name_district VARCHAR(255),
			PRIMARY KEY  (id_district)
		);";

		dbDelta($sql);

		// Заполняем таблицу первоночальными данными
			$districts = array
			(
				'Березняки', 'Борисполь', 'Бориспольское шоссе', 'Бровары', 
				'Гостомельское шоссе', 'Дарницкий', 'Деснянский', 'Днепровский', 
				'Житомирское шоссе', 'Новообуховское шоссе', 'Оболонский', 
				'Одесское шоссе', 'Окружная дорога', 'Печерский', 'Подольский', 
				'Русановка', 'Святошинский', 'Соломенский', 
				'Старообуховское шоссе', 'Черниговское шоссе', 'Шевченковский', 
				'Голосеевский'
			);

			for ( $i = 0; $i < count($districts); $i++ )
			{
				$wpdb->query( $wpdb->prepare("INSERT INTO $table_name 
					 VALUES (%d, %d, %s)", ($i+1), 1, $districts[$i]));
			}

		
	}

	// Проверяем наличие таблици станций метро города, если нет создаем ее
	$table_name = $wpdb->prefix . 'infocities_subway';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id_subway MEDIUMINT(9)	NOT NULL AUTO_INCREMENT,
			id_city INT(11),
			name_subway VARCHAR(255),
			PRIMARY KEY  (id_subway)
		);";

		dbDelta($sql);

		// Заполняем таблицу первоночальными данными
			$subway = array
			(
				'Академгородок', 'Арсенальная', 'Берестейская', 'Бориспольская',
				'Васильковская', 'Вокзальная', 'Выдубичи', 'Вырлица', 
				'Выставочный центр', 'Героев Днепра', 'Гидропарк', 
				'Голосеевская', 'Дарница', 'Дворец Спорта', 
				'Дворец Спорта / Площадь Льва Толстого', 'Дворец Украина',
				'Демеевская', 'Днепр', 'Дорогожичи', 'Дружбы Народов', 
				'Житомирская', 'Золотые Ворота', 'Золотые Ворота / Театральная',
				'Ипподром',  'Кловская', 'Контрактовая площадь', 'Крещатик',
				'Крещатик / Площадь Независимости', 'Левобережная', 'Лесная',
				'Лукьяновская', 'Лыбедская', 'Минская', 'Нивки', 'Оболонь',
				'Олимпийская', 'Осокорки', 'Петровка', 'Печерская',
				'Площадь Льва Толстого', 'Площадь Независимости', 'Позняки',
				'Политехнический институт', 'Почтовая площадь', 'Святошин',
				'Славутич', 'Сырец', 'Тараса Шевченко', 'Театральная',
				'Теремки', 'Университет', 'Харьковская', 'Черниговская',
				'Шулявская'
			);

			for ( $i = 0; $i < count($subway); $i++ )
			{
				$wpdb->query( $wpdb->prepare("INSERT INTO $table_name 
					 VALUES (%d, %d, %s)", ($i+1), 1, $subway[$i]));
			}
	}

	// Проверяем наличие таблици средних чеков заведений города, если нет создаем ее
	$table_name = $wpdb->prefix . 'infocities_averagecheck';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id_check MEDIUMINT(9)	NOT NULL AUTO_INCREMENT,
			min_price INT(11),
			average_check VARCHAR(255),
			PRIMARY KEY  (id_check)
		);";

		dbDelta($sql);

		// Заполняем таблицу первоночальными данными
			$minPrice = array(0, 100, 500, 1000, 1500, 2000);
			$average = array
			(
				'До 100', '100-500', '500-1000', '1000-1500',
				'1500-2000', 'От 2000'
			);

			for ( $i = 0; $i < count($average); $i++ )
			{
				$wpdb->query( $wpdb->prepare("INSERT INTO $table_name
					 VALUES (%d, %d, %s)", ($i+1), $minPrice[$i], $average[$i]));
			}
	}

	// Проверяем наличие таблици дополнительных заведений города, если нет создаем ее
	$table_name = $wpdb->prefix . 'infocities_addoptionsplaces';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id_option MEDIUMINT(9)	NOT NULL AUTO_INCREMENT,
			value_option VARCHAR(255),
			slug_option VARCHAR(255),
			PRIMARY KEY  (id_option)
		);";

		dbDelta($sql);

		// Заполняем таблицу первоночальными данными
			$options = array
			(
				array('label' => 'Парковка', 'id' => 'parking'),
				array('label' => 'Караоке', 'id' => 'karaoke'),
	        	array('label' => 'Летняя терраса', 'id' => 'summerterrace'),
	        	array('label' => 'Пластиковые карты', 'id' => 'visa'),
	        	array('label' => 'Бизнес ланч', 'id' => 'businesslunch'),
	        	array('label' => 'Круглосуточно', 'id' => 'aroundtheclock'),
	        	array('label' => 'Баня/Сауна', 'id' => 'sauna'),
	        	array('label' => 'Диджей', 'id' => 'dj'),
	        	array('label' => 'Бильярд', 'id' => 'billiards'),
	        	array('label' => 'Дартс', 'id' => 'darts'),
	        	array('label' => 'Сигары', 'id' => 'cigars'),
	        	array('label' => 'Катание на лошадях', 'id' => 'riding'),
	        	array('label' => 'Пляж', 'id' => 'beach'),
	        	array('label' => 'Танцпол', 'id' => 'dancefloor'),
	        	array('label' => 'WiFi', 'id' => 'wifi'),
	        	array('label' => 'Детская комната', 'id' => 'nursery'),
	        	array('label' => 'Винная карта', 'id' => 'winelist'),
	        	array('label' => 'Суши', 'id' => 'sushi'),
	        	array('label' => 'Отель', 'id' => 'hotel'),
	        	array('label' => 'Рыбалка', 'id' => 'fishing'),
	        	array('label' => 'Живая музыка', 'id' => 'livemusic'),
	        	array('label' => 'Бассейн', 'id' => 'pool'),
	        	array('label' => 'Боулинг', 'id' => 'bowling'),
	        	array('label' => 'Кальян', 'id' => 'hookah'),
	        	array('label' => 'Катание на лодке', 'id' => 'boating'),
	        	array('label' => 'Настольные игры', 'id' => 'games'),
	        	array('label' => 'Большой экран', 'id' => 'bigscreen'),
	        	array('label' => 'Стриптиз/Эротическое шоу', 'id' => 'stripping')
			);

			for ( $i = 0; $i < count($options); $i++ )
			{
				$wpdb->query( $wpdb->prepare("INSERT INTO $table_name
					 VALUES (%d, %s, %s)", ($i+1), $options[$i]['label'], $options[$i]['id']));
			}
	}

	// Проверяем наличие таблици оссобенностей заведения, если нет создаем ее
	$table_name = $wpdb->prefix . 'infocities_featureplaces';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id_feature MEDIUMINT(9)	NOT NULL AUTO_INCREMENT,
			value_feature VARCHAR(255),
			slug_feature VARCHAR(255),
			PRIMARY KEY  (id_feature)
		);";

		dbDelta($sql);

		// Заполняем таблицу первоночальными данными
			$feature = array
			(
				array('label' => 'трансляции', 'id' => 'streem'),
	        	array('label' => 'день рождения', 'id' => 'birthday'),
	        	array('label' => 'банкеты и корпоративы', 'id' => 'banquet'),
	        	array('label' => 'семейный ужин', 'id' => 'family'),
	        	array('label' => 'бизнес-ланч', 'id' => 'lunch'),
	        	array('label' => 'потусить', 'id' => 'club'),
	        	array('label' => 'романтический ужин', 'id' => 'romantic'),
	        	array('label' => 'детский праздник', 'id' => 'children'),
	        	array('label' => 'деловая встреча', 'id' => 'meeting'),
	        	array('label' => 'свадьба', 'id' => 'wedding')
			);

			for ( $i = 0; $i < count($feature); $i++ )
			{
				$wpdb->query( $wpdb->prepare("INSERT INTO $table_name
					 VALUES (%d, %s, %s)", ($i+1), $feature[$i]['label'], $feature[$i]['id']));
			}
	}
}

add_action('current_screen', 'init_scripts');
function init_scripts( $current_screen )
{
	if ( $current_screen->post_type == 'places' &&  $current_screen->base == 'post' )
	{	
		wp_register_script('info-script', plugin_dir_url(__FILE__).'/js/info-script.js', array('jquery')); 
	    wp_enqueue_script('info-script');
	}
}

include "includes/add-post-type-places.php";
include "includes/info-functions.php";
