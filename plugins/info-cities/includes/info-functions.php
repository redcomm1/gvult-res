<?php
/////////// Функции //////////////////

// Возвращает из базы данных массив средних чеков
	function get_average_checks()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_averagecheck';
		$result = $wpdb->get_results( "SELECT * FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$average_check[$i]['name'] = $result[$i]->average_check;	
			$average_check[$i]['id'] = $result[$i]->id_check;
			$average_check[$i]['min'] = $result[$i]->min_price;
		}

		return $average_check;
	}
	
	function get_average_check_values()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_averagecheck';
		$result = $wpdb->get_results( "SELECT average_check FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$average_check[$i] = $result[$i]->average_check;	
		}

		return $average_check;
	}

	// Возвращает минимальное цену по названию среднего чека
	function get_check_min( $averageCheck )
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_averagecheck';
		$query = "SELECT min_price FROM $table_name WHERE average_check ='".$averageCheck."'";
		return $wpdb->get_var( $query );
	}	

// Возвращает из базы данных массив id и названия районов города Киев
	function get_districts()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_district';
		$result = $wpdb->get_results( "SELECT * FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$districts[$i]['name'] = $result[$i]->name_district;
			$districts[$i]['id'] = $result[$i]->id_district;
		}

		return $districts;
	}

	function get_districts_name()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_district';
		$result = $wpdb->get_results( "SELECT name_district FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$districts[$i] = $result[$i]->name_district;	
		}

		return $districts;
	}

// Возвращает из базы данных массив станций метро города
	function get_subways()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_subway';
		$result = $wpdb->get_results( "SELECT * FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$subways[$i]['name'] = $result[$i]->name_subway;	
			$subways[$i]['id'] = $result[$i]->id_subway;
		}

		return $subways;
	}

	function get_subway_names()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_subway';
		$result = $wpdb->get_results( "SELECT name_subway FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$subways[$i] = $result[$i]->name_subway;	
		}

		return $subways;
	}

// Возвращает из базы данных массив возможностей заведения
	function get_feature_places()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_featureplaces';
		$result = $wpdb->get_results( "SELECT * FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$features[$i]['name'] = $result[$i]->value_feature;	
			$features[$i]['id'] = $result[$i]->id_feature;
			$features[$i]['slug'] = $result[$i]->slug_feature;
		}

		return $features;
	}

	function get_feature_places_name()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_featureplaces';
		$result = $wpdb->get_results( "SELECT value_feature FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
			$features[$i] = $result[$i]->value_feature;	
			
		return $features;
	}

	function get_feature_places_slug()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_featureplaces';
		$result = $wpdb->get_results( "SELECT slug_feature FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
			$features[$i] = $result[$i]->slug_feature;	
			
		return $features;
	}	

// Возвращает из базы данных массив дополнительных опций заведения
	function get_add_option_places()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_addoptionsplaces';
		$result = $wpdb->get_results( "SELECT * FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
		{ 
			$options[$i]['name'] = $result[$i]->value_option;	
			$options[$i]['id'] = $result[$i]->id_option;
			$options[$i]['slug'] = $result[$i]->slug_option;
		}

		return $options;
	}

	function get_add_option_places_slug()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'infocities_addoptionsplaces';
		$result = $wpdb->get_results( "SELECT slug_option FROM $table_name" );
		
		for ($i=0; $i < count($result); $i++)
			$options[$i] = $result[$i]->slug_option;	
			
		return $options;
	}

/////////// Функции ///////////////////
	function get_days( $lang = 'ru')
	{
	if ( $lang == 'ru' )
		$day = array('Пн','Вт','Ср','Чт','Пт','Сб','Вс');
	else
		$day = array('mon','tue','wed','thu','fri','sat','sun');

	return $day;
	}

	function _get_time_work( $days  /*Array*/)
	{
	$day_ru = get_days();
	$day_en = get_days('en');

	echo '<table>';
	for ( $i = 0; $i < count($days); $i++ )
	{
		echo '<tr>
			<td>'.$day_ru[$i].': </td>
			<td><label>c </label><input type="text" class="time_from" name="'.$day_en[$i].'_from" value="'.$days[$i]['from'].'" style="width: 50px;"></td>
			<td><label>до </label><input type="text" class="time_to" name="'.$day_en[$i].'_to"  value="'.$days[$i]['to'].'" style="width: 50px;"></td>
			<td><input class="place_work_switch" name type="checkbox">Выходной</td>
		<tr>';
	}
	echo '</table>';
	}

	function get_time_work($postID = '')
	{
		if ( empty($postID) )
		{	
			global $post;
			$postID = $post->ID;
		}

		$day_en = get_days('en');

		for ( $i = 0; $i < 7; $i++ )
		{
			$days[$i]['from1'] = get_post_meta($postID, $day_en[$i].'-from_1', true);
			$days[$i]['to1'] = get_post_meta($postID, $day_en[$i].'-to_1', true);

			$days[$i]['from2'] = get_post_meta($postID, $day_en[$i].'-from_2', true);
			$days[$i]['to2'] = get_post_meta($postID, $day_en[$i].'-to_2', true);

			$days[$i]['from'] = $days[$i]['from2'];
			$days[$i]['to'] = $days[$i]['to1'] != '00:00' ? $days[$i]['to1'] : $days[$i]['to2'];
		}

		return $days;
	}

	function get_field_add_options($name, $title = '')
	{
		global $post;
		$field = get_post_meta($post->ID, $name, true);

		echo '<div style="float: left; width: 22%; margin: 0; margin-right: 20px;">
			<label for="'.$name.'">'.$title.'</label>
		</div>
		<div style="float: left;">';
		if ( $name == 'country' )
			echo '<input type="text" name="'.$name.'" value="Украина" size="50" readonly /><br />';
		else if ( $name == 'region' )
			echo '<input type="text" name="'.$name.'" value="" size="50" readonly /><br />';
		else if ( $name == 'city' )
			echo '<input type="text" name="'.$name.'" value="Киев" size="50" readonly /><br />';
		else 
			echo '<input type="text" name="'.$name.'" value="'.$field.'" size="50" /><br />';
		echo '</div>
		<div style="clear: both;"></div><br />';
	}

	function get_field_time_work()
	{
		global $post;
		echo '<div style="float: left; width: 22%; margin: 0; margin-right: 20px;">
				<label for="time">Режим работы:</label>
			</div>
			<div style="float: left;">';

		$hours = get_post_meta($post->ID, 'hours', true);

		// if ( !empty($hours) )
		// {
		// 	$timeWork = explode(', ', $hours);

		// 	$days = _build_days($timeWork);
		// 	_get_time_work($days);
		// }
		// else
		// {
			$days = get_time_work($post->ID);
			_get_time_work($days);
		// }

		echo '</div>
			<div style="clear: both;"></div><br />';
	}

	function get_field_address()
	{
		global $post;
		$field = get_post_meta($post->ID, 'address', true);

		if ( !empty($field) )
		{
			$address = explode(',', $field);

			for ( $i = 0; $i < count($address); $i++ )
				$address[$i] = trim($address[$i]);

			if ( count($address) == 1 )
			{
				$addressName = $address[0];
				$addressNumber = '';
				$addressPrefix = '';
			}
			else
			{
				$addressName = $address[1];
				$addressNumber = $address[2];
				$addressPrefix = '';	
			}
		}
		else
		{
			$addressName = get_post_meta($post->ID, 'address_name', true);
			$addressNumber = get_post_meta($post->ID, 'address_number', true);
			$addressPrefix = get_post_meta($post->ID, 'address_prefix', true);		
		}

		echo '<div style="float: left; width: 22%; margin: 0; margin-right: 20px;">
				<labe>Адрес:</label>
			</div>
			<div style="float: left;">';

		echo '<table>';
			echo '<tr>
				<td></td>
				<td>Улица</td>
				<td>№ дома</td>
			</tr>';

		$prefix = array('', 'бульвар', 'переулок', 'проспект', 'улица', 'шоссе');
			echo '<tr>
				<td><select name="address_prefix">';
					for ( $i = 0; $i < count($prefix); $i++ )
					{ 
						echo '<option value="'.$prefix[$i]
							.'" '
							.selected($addressPrefix, $prefix[$i], false)
							.'>'
							.$prefix[$i]
							.'</option>';
					}

			echo '</select></td>
				<td><input type="text" name="address_name" value="'.$addressName.'" size=50></td>
				<td><input type="text" name="address_number" value="'.$addressNumber.'" style="width: 50px;"></td>
				
			<tr>';

		echo '</table>';

		echo '</div>
			<div style="clear: both;"></div><br />';
	}

	function get_field_with_select($name, $title = '')
	{
		global $post;

		if ( $name == 'district' )
		{
			$fields = get_districts_name();
			$curName = get_post_meta( $post->ID, 'rayon', false );

			if ( empty($curName) )
				$curName = get_post_meta( $post->ID, $name, false );			
		}
		else if ( $name == 'subway' )
		{
			$fields = get_subway_names();
			$curName = get_post_meta( $post->ID, 'metro', false );

			if ( empty($curName) )
				$curName = get_post_meta( $post->ID, $name, false );
		}
		else if ( $name == 'check' )
		{
			$fields = get_average_check_values();
			$curName = get_post_meta( $post->ID, $name, false );
		}

		array_unshift($fields, '--');

		echo '<div style="float: left; width: 22%; margin: 0; margin-right: 20px;">';
	  	echo '<label>'.$title.'</label>';
	  	echo '</div>';
	  	echo '<div style="float: left;">';
	  	echo '<select name="'.$name.'">';
			for( $i = 0; $i < count($fields); $i++ )
				echo '<option value="'.$fields[$i].'" '.selected($curName[0], $fields[$i], false).'>'.$fields[$i].'</option>'; 
		echo '</select>';
		echo '</div>';
	    echo '<div style="clear: both;"></div>';
	    echo '<br />'; 
	}

	function get_field_with_checkbox($name, $title = '')
	{
		global $post;

		if ( $name == 'feature' )
		{
			$fields = get_feature_places();
			$recommend = get_post_meta( $post->ID, 'recommend', false );

			if ( !empty($recommend) )
			{
				$curOptions = explode(', ', $recommend[0]);
			}
			else
			{
				$curOptions = _check_feature_places($fields);
			}

		}
		else if ( $name == 'additional' )
		{
			$fields = get_add_option_places();
			$curOptions = _check_feature_places($fields);
		}

		echo '<div style="float: left; width: 22%; margin: 0; margin-right: 20px;">';
		echo '<label>'.$title.'</label>';
		echo '</div>';
		echo '<div style="float: left;">';

		for ($i = 0; $i < count($fields); $i++)
		{
			$checked =false;

			for ( $j = 0; !empty($curOptions) && $j < count($curOptions); $j++ )
				if ( $fields[$i]['name'] == $curOptions[$j] )
					$checked = true;

			echo '<input type="checkbox" value="yes" name="'.$fields[$i]['slug'].'" '.($checked ? 'checked="checked"' : '').' />
			  			<label>'.$fields[$i]['name'].'</label><br />';
		}	

		echo '</div>';
		echo '<div style="clear: both;"></div>';
		echo '<br />';
	}


				///// -------------------------- //////////
				///// Внутренние функции плагина //////////
				///// -------------------------- //////////


// Функция возвращает массив названий возможностей, 
// которые установленны для этого заведения
	function _check_feature_places( $features /*array*/ )
	{
		global $post;

		// echo '<pre>';
		// print_r(get_post_meta($post->ID, $features[1]['slug'], false));
		// echo '</pre>';
		if ($post->post_status != 'publish')
			return 0;

		$count = 0;
		for ( $i = 0; $i < count( $features ); $i++ )
		{
			$feature = get_post_meta($post->ID, $features[$i]['slug'], false);
			if ( !empty($feature) && $feature[0] == 'yes' )
			{
				$curOptions[$count] = $features[$i]['name'];
				$count++;
			}
		}

		if ( !isset($curOptions) )
			return 0;

		return $curOptions;
	}

// Функция ничего не возвращает
	function _update_meta_feature_places( $slug /*array*/ )
	{
		global $post;

		for ( $i = 0; $i < count($slug); $i++ )
	    { 
	    	if ( isset( $_POST[$slug[$i]] ) )
	   			update_post_meta($post->ID, $slug[$i], $_POST[$slug[$i]]);
	   		else
	   			update_post_meta($post->ID, $slug[$i], 'no');
	    }
	}

// Функция возвращает массив со знач. времени по дням
function _build_days( $timeWork /*Array*/ ) 
{
	if ( count($timeWork) == 7)
		for ( $i = 0; $i < count($timeWork); $i++ )
		{
			$days[$i] = explode('-', $timeWork[$i]);
			$days[$i][0] = substr($days[$i][0], 6);
		}
	else
		for ( $i = 0; $i < 7; $i++ )
		{
			$days[$i][0] = '00:00';
			$days[$i][1] = '00:00';
		}
	return $days;
}
?>
