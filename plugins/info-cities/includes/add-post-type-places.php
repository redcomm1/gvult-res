<?php
// Регистрируем типы постов
  add_action( 'init', 'register_post_type_places' );
  
  function register_post_type_places()
  {
    $labels =array
    (
      'name'                => 'Заведения',
      'singular_name'       => 'Заведение',
      'add_new_item'        => 'Добавить новое заведение',
      'edit_item'           => 'Изменить заведение',
      'new_item'            => 'Новое заведение',
      'all_items'           => 'Все заведения',
      'view_item'           => 'Смотреть заведение',
      'search_items'        => 'Найти заведения',
      'not_found'           => 'Заведения не найдены',
      'not_found_in_trash'  => 'Заведения не найдены в карзине',
      'parent_item_colon'   => '',
      'menu_name'           => 'Заведения'
    );

    $args = array
    (
      'labels'        => $labels,
      'public'        => true,
      'has_archive'   => true,
      'hierarchical'  => true,
      'taxonomies'    => array('category'),
      'rewrite'       => array( 'slug' => 'place' ),
      'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'custom-fields'),
      'menu_position' => 4
    );

    register_post_type('places', $args);
  }

add_filter( 'post_updated_messages', 'true_post_type_messages' );
 
function true_post_type_messages( $messages ) {
	global $post, $post_ID;
 
	$messages['places'] = array( // zavedeniya - название созданного нами типа записей
		0 => '', // Данный индекс не используется.
		1 => sprintf( 'Заведение обновлено. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
		2 => 'Параметр обновлён.',
		3 => 'Параметр удалён.',
		4 => 'Заведение обновлено',
		5 => isset($_GET['revision']) ? sprintf( 'Заведение восстановлено из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( 'Заведение опубликовано на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
		7 => 'Заведение сохранено.',
		8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
 
	return $messages;
}

function true_post_type_help_tab() {
 
	$screen = get_current_screen();
 
	// Прекращаем выполнение функции, если находимся на страницах других типов постов
	if ( 'places' != $screen->post_type )
		return;
 
	// Массив параметров для первой вкладки
	$args = array(
		'id'      => 'tab_1',
		'title'   => 'Обзор',
		'content' => '<h3>Обзор</h3><p>Содержимое первой вкладки.</p>'
	);
 
	// Добавляем вкладку
	$screen->add_help_tab( $args );
 
	// Массив параметров для второй вкладки
	$args = array(
		'id'      => 'tab_2',
		'title'   => 'Доступные действия',
		'content' => '<h3>Доступные действия с типом постов &laquo;' . $screen->post_type . '&raquo;</h3><p>Содержимое второй вкладки</p>'
	);
 
	// Добавляем вторую вкладку
	$screen->add_help_tab( $args );
 
}
add_action('admin_head', 'true_post_type_help_tab');


////////////// ДОБАВЛЕНИЕ ДОПОЛНИТЕЛЬНЫХ ПОЛЕЙ В АДМИНПАНЕЛЬ //////////////////
add_action('admin_init', 'my_meta_box'); 
function my_meta_box() { 

        add_meta_box(  
              'my_meta_box', // Идентификатор(id)
              'Дополнительная информация о заведении', // Заголовок области с мета-полями(title)
              'show_my_metabox', // Вызов(callback)
              'places', // Где будет отображаться наше поле, в нашем случае в заведениях
              'normal',
              'high');
}

function show_my_metabox()
{ 
	global $post;

	// Выводим скрытый input, для верификации. Безопасность прежде всего!
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	echo '<div style="display: inline-block; margin: 0 90px 0 10px;">'; 

	get_field_add_options(
		'keywords',
		'Ключевые фразы (указывать через запятую): ');

	get_field_add_options(
		'phone',
		'Телефон [Формат: +38044XXXXXXX]:  ');

	get_field_time_work();

	get_field_add_options(
		'owner',
		'Владелец [Сеть]: ');

	get_field_add_options(
		'country',
		'Страна: ');

	get_field_add_options(
		'region',
		'Область / район: ');

	get_field_add_options(
		'city',
		'Город: ');

	get_field_address();

	get_field_add_options(
		'latitude',
		'Широта: ');

	get_field_add_options(
		'longitude',
		'Долгота: ');

	get_field_with_select(
		'district', 
		'Район Киева: ');

	get_field_with_select(
		'subway', 
		'Ближайшая станция метро: ');

	get_field_with_checkbox(
		'feature',
		'Рекомендуете для [Например: Pre-party, Активного отдыха, Банкетов]: ');

	get_field_with_select(
		'check', 
		'Средняя стоимость на персону, грн.: ');

	get_field_add_options(
		'hall',
		'Вместительность большого зала для фуршета: ');

	get_field_add_options(
		'discount_card',
		'Дисконтные карты [Например: Козырная карта, Мировая карта]: ');

	get_field_add_options(
		'website',
		'Сайт: ');

	get_field_add_options(
		'twitter',
		'Страница в Twitter: ');

	get_field_add_options(
		'fb',
		'Страница в Facebook: ');

	get_field_add_options(
		'contacts',
		'Контакты для связи (не отображаются на сайте): ');

	get_field_add_options(
		'restonid',
		'ID для брони: ');

	get_field_with_checkbox(
		'additional',
		'<b>Дополнительные опции: </b>');

	echo '</div>';
}

// Пишем функцию для сохранения
function save_my_meta_fields($post_id)
{
    if (!isset($_POST['custom_meta_box_nonce']) || !wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('places' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    } 
   	$meta_key = array
    	('keywords', 'phone', 'owner', 'latitude',
    		'longitude', 'address_name', 'address_number', 'address_prefix', 
    		'district', 'subway', 'check', 'website', 'twitter', 'fb', 'contacts', 'restonid');

    for ( $i = 0; $i < count($meta_key); $i++ )
    { 
    	if ( isset( $_POST[$meta_key[$i]] ) )
   			update_post_meta($post_id, $meta_key[$i], $_POST[$meta_key[$i]]);
    }

    // Добавляем время
    $days = get_days('en');

    for ( $i = 0; $i < count($days); $i++ )
    {
    	if ( $_POST[$days[$i].'_from'] < $_POST[$days[$i].'_to'] )
	    {
	    	update_post_meta($post_id, $days[$i].'-from_1', '00:00');
	    	update_post_meta($post_id, $days[$i].'-to_1', '00:00');
	  		update_post_meta($post_id, $days[$i].'-from_2', $_POST[$days[$i].'_from']);
	    	update_post_meta($post_id, $days[$i].'-to_2', $_POST[$days[$i].'_to']);
	    }
	    else if ( $_POST[$days[$i].'_from'] == '00:00' && 
	    			$_POST[$days[$i].'_to'] == '00:00')
	    {
	    	update_post_meta($post_id, $days[$i].'-from_1', '00:00');
	    	update_post_meta($post_id, $days[$i].'-to_1', '00:00');
	  		update_post_meta($post_id, $days[$i].'-from_2', '00:00');
	    	update_post_meta($post_id, $days[$i].'-to_2', '00:00');
	    }
	    else
	    {
	    	update_post_meta($post_id, $days[$i].'-from_1', '00:00');
	    	update_post_meta($post_id, $days[$i].'-to_1', $_POST[$days[$i].'_to']);
	  		update_post_meta($post_id, $days[$i].'-from_2', $_POST[$days[$i].'_from']);
	    	update_post_meta($post_id, $days[$i].'-to_2', '24:00');	
	    }
    }

    // Добавляем пост мета 'adsress'
    	if ( !empty($_POST['address_name']) )
    		update_metadata('post', $post_id, 'address_', $_POST['address_prefix'].', '.$_POST['address_name'].', '.$_POST['address_number']);
    	else
    	 	update_post_meta($post_id, 'address_', '');

    // Добавляем пост мета 'check_min'
    if ( $_POST['check'] != '--')
    	update_post_meta($post_id, 'check_min', get_check_min($_POST['check']));
    else
    	update_post_meta($post_id, 'check_min', 0);

    $hours = get_post_meta($post_id, 'hours', true);
    if ( !empty( $hours ) )
   		delete_post_meta($post_id, 'hours');

	$address = get_post_meta($post_id, 'address', true);
    if ( !empty( $address ) )
    	delete_post_meta($post_id, 'address');

    $rayon = get_post_meta($post_id, 'rayon', true);
    $metro = get_post_meta($post_id, 'metro', true);
    if ( !empty( $rayon ) || !empty( $rayon ) )
    {
    	delete_post_meta($post_id, 'rayon');
    	delete_post_meta($post_id, 'metro');
    }

    _update_meta_feature_places( get_feature_places_slug() );
    _update_meta_feature_places( get_add_option_places_slug() );

	$recommend = get_post_meta($post_id, 'recommend', true);
    if ( !empty( $recommend ) )
    	delete_post_meta($post_id, 'recommend');
} 

add_action('save_post', 'save_my_meta_fields'); // Запускаем функцию сохранения


