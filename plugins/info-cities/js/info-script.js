jQuery( document ).ready(function($)
{
	console.log( "File 'info-script.js' is started!" );

	$timeFromValue = []
	$timeToValue = []

	$('.time_from').each( function( index, value )
	{
		$timeFrom = $(this)
		$timeTo = $(this).parent().parent().find('.time_to')
		$checkBox = $(this).parent().parent().find('.place_work_switch')

		if ( $timeFrom.attr('value') == '00:00' && $timeTo.attr('value') == '00:00' )
		{
			$checkBox.attr('checked', 'checked')
			$timeFrom.attr('readonly', true)
			$timeTo.attr('readonly', true)
		}
	})

	$('.place_work_switch').each( function( index, el )
	{
		$(el).on('change', function(event)
		{
			console.log(this)
			console.log(this.checked)
			
			$timeFrom = $(this).parent().parent().find('.time_from')
			$timeTo = $(this).parent().parent().find('.time_to')

			if ( this.checked )
			{
				$timeFromValue[index] = $($timeFrom).attr('value')
				$timeToValue[index] = $($timeTo).attr('value')

				$timeFrom.attr(
					{
						value: '00:00',
						readonly: true
					})
				$timeTo.attr(
					{
						value: '00:00',
						readonly: true
					})
			}
			else
			{
				$timeFrom.attr(
					{
						value: $timeFromValue[index] ? $timeFromValue[index] : '00:00',
						readonly: false
					})
				$timeTo.attr(
					{
						value: $timeToValue[index] ? $timeToValue[index] : '00:00',
						readonly: false
					})
			}

			event.preventDefault()
		})
	})
});