<?php
require "soc-auth.php";

if (isset($_GET['code'])) {
    $result = false;

    $params = array(
        'client_id'     => CLIENT_ID_FB,
        'redirect_uri'  => REDIRECT_URL . 'fb.php',
        'client_secret' => CLIENT_SECRET_FB,
        'code'          => $_GET['code']
    );
	
    $url = 'https://graph.facebook.com/v2.9/oauth/access_token';
	
    $tokenInfo = null;
	
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_URL, $url. '?' . http_build_query($params));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
	$res = curl_exec($ch);
	curl_close($ch);
	$tokenInfo = json_decode($res);
         
	if ( property_exists( $tokenInfo, 'access_token' )) {
		$url = 'https://graph.facebook.com/me';
        $params = array(
            'access_token' => $tokenInfo->access_token,
            'fields' => 'id,first_name,last_name,email,picture,gender,link,location'
            );

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url. '?' .http_build_query($params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = curl_exec($ch);
		curl_close($ch);
		$userInfo = json_decode($res);
		
        if ( property_exists( $userInfo, 'id') ) {
            $result = true;
        }
    }

    echo '<pre>';
    print_r($result);
    echo '</pre>';

   if ($result) {
    $user_login = $userInfo->id;
    $user_pass = $userInfo->id;

    $user = username_exists($userInfo->id);

        if ($user) {
            signon_soc_user($user_login, $user_pass);
            
        } else {
            
            $userdata = array(
            'user_pass'       => $userInfo->id, 
            'user_login'      => $userInfo->id,
            'user_nicename'   => transliterate($userInfo->last_name),
            'display_name'    => transliterate($userInfo->last_name),
            'nickname'        => transliterate($userInfo->last_name),
            'first_name'      => $userInfo->first_name,
            'last_name'       => $userInfo->last_name
            );  

            $user_id = insert_soc_user($userdata);

            $sex = $userInfo->gender;
            $sex = sex_soc_user($sex);
            
            //add_user_meta($user_id, 'birthday', $userInfo['birthday'], true);
            add_user_meta($user_id, 'gender', $sex, true);
            add_user_meta($user_id, 'avatar_small', $userInfo->picture->data->url, true);
            add_user_meta($user_id, 'email', $userInfo->email, true);
            add_user_meta($user_id, 'enter_from', 'facebook', true);
        }
    }
}
?>
