<?php

/*
Plugin Name: Custom Social Authentification Plugin
Plugin URI: http://smart-site.pro
Description: Plugin for user social account login
Author: Alex Kravchenko
Version: 1.0.1
Author URI: http://smart-site.pro
*/

// include('../../../wp-load.php');
// require_once(ABSPATH.'/wp-load.php');
require_once(dirname(__FILE__).'/../../../wp-load.php');

//VK
define('CLIENT_ID_VK', '5436974');
define('CLIENT_SECRET_VK', 'KfhJM5C4lFGjeMxbDTvL');
define('URL_VK', 'http://oauth.vk.com/authorize');

//fb
define('CLIENT_ID_FB', '117897172124094');
define('CLIENT_SECRET_FB', '49069828f0679e72b6b8230f0c56df85');
//define('URL_FB', 'https://www.facebook.com/dialog/oauth');
define('URL_FB', 'https://www.facebook.com/v2.9/dialog/oauth');


//G+
define('CLIENT_ID_GOOGLE', '891511661447-a6ii2la7pg2b0qi5jvm3h3445nf6qja5.apps.googleusercontent.com');
define('CLIENT_SECRET_GOOGLE', 'uCUn-FSZMnk901VIGDgq17Vp');
define('URL_GOOGLE', 'https://accounts.google.com/o/oauth2/auth');

//twitter
define('CLIENT_ID_TWITTER', 'AEQkFt46w87U16aRqpaRlTp6I');
define('CLIENT_SECRET_TWITTER', '45Oe3Ou1yCYyNlp79BDwAnJ3eLrGDNUhw86M9wp58UzBmiA8nL');
define('REQUEST_TOKEN_URL', 'https://api.twitter.com/oauth/request_token');
define('AUTHORIZE_URL', 'https://api.twitter.com/oauth/authorize');
define('ACCESS_TOKEN_URL', 'https://api.twitter.com/oauth/access_token');
define('ACCOUNT_DATA_URL', 'https://api.twitter.com/1.1/users/show.json');
define('URL_SEPARATOR', '&');

define('REDIRECT_URL', plugins_url().'/soc-auth/');
// define('REDIRECT_URL_VK', home_url().'/index.php?soc-auth=vk.php');

//define("REDIRECT_URL", home_url().'/wp-content/plugins/soc-auth/');

// Авторизация через соц. сети
add_action('wp_ajax_nopriv_ajaxdatausers', 'login_soc');
function login_soc()
{
	if ( !wp_verify_nonce($_POST['nonce'], 'check-google') )
	  die();

	if ( username_exists($_POST['ID']) )
	  signon_soc_user($_POST['ID'], $_POST['ID']);
	else
	{
	  $userdata = array
	  (
	    'user_login'  => $_POST['ID'],
	    'user_pass'   => $_POST['ID'],//wp_generate_password(),
	    'user_nicename' => transliterate($_POST['firstName']),
	    'user_nicename' => transliterate($_POST['firstName']),
	    'nickname'    => transliterate($_POST['firstName']),
	    'first_name'  => $_POST['firstName'],
	    'last_name'   => $_POST['lastName'],
	  );

	  $user_id = insert_soc_user($userdata);

	  add_user_meta($user_id, 'avatar_small', $_POST['image'], true);
	  add_user_meta($user_id, 'email', $_POST['email'], true);
	  add_user_meta($user_id, 'enter_from', 'google', true);
	}

	echo json_encode( $_POST );
	die();
}



function get_socauth_link($social) {
	switch ($social) {
		case 'vk':
					$params = array(
				        'client_id'     => CLIENT_ID_VK,
				        'redirect_uri'  => REDIRECT_URL . 'vk.php',
				        'response_type' => 'code'
			    	);
					$link = URL_VK . '?' . urldecode(http_build_query($params));
		break;
		case 'fb':
					$params = array(
					    'client_id'     => CLIENT_ID_FB,
					    'redirect_uri'  => REDIRECT_URL . 'fb.php',
						'response_type' => 'code',
					    'scope'         => 'email'
					);
		
					$link = URL_FB . '?' . urldecode(http_build_query($params));
		break;
		case 'gp':
					$params = array(
					    'redirect_uri'  => REDIRECT_URL . 'google.php',
					    'response_type' => 'code',
					    'client_id'     => CLIENT_ID_GOOGLE,
					    'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
					);
					$link = URL_GOOGLE . '?' . urldecode(http_build_query($params));;
			break;
		case 'tw':
					// формируем подпись для получения токена доступа

					$oauth_nonce = md5(uniqid(rand(), true));
					$oauth_timestamp = time();

					$params = array(
					    'oauth_callback=' . urlencode(REDIRECT_URL . 'twitter.php') . URL_SEPARATOR,
					    'oauth_consumer_key=' . CLIENT_ID_TWITTER . URL_SEPARATOR,
					    'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
					    'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
					    'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
					    'oauth_version=1.0'
					);

					$oauth_base_text = implode('', array_map('urlencode', $params));
					$key = CLIENT_SECRET_TWITTER . URL_SEPARATOR;
					$oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(REQUEST_TOKEN_URL) . URL_SEPARATOR . $oauth_base_text;
					$oauth_signature = base64_encode(hash_hmac('sha1', $oauth_base_text, $key, true));


					// получаем токен запроса
					$params = array(
					    URL_SEPARATOR . 'oauth_consumer_key=' . CLIENT_ID_TWITTER,
					    'oauth_nonce=' . $oauth_nonce,
					    'oauth_signature=' . urlencode($oauth_signature),
					    'oauth_signature_method=HMAC-SHA1',
					    'oauth_timestamp=' . $oauth_timestamp,
					    'oauth_version=1.0'
					);
					$url = REQUEST_TOKEN_URL . '?oauth_callback=' . urlencode(REDIRECT_URL . 'twitter.php') . implode('&', $params);

					$response = file_get_contents($url);
					parse_str($response, $response);

					$oauth_token = $response['oauth_token'];
					$oauth_token_secret = $response['oauth_token_secret'];

					// генерируем ссылку аутентификации
					$link = AUTHORIZE_URL . '?oauth_token=' . $oauth_token;
			break;
	}
	return $link;
}

// function soc_auth_user($soc_user) {
// 	$wp_user = get_user_by('login', $soc_user['uid']);
// 	if ($wp_user) {
// 		wp_set_auth_cookie($wp_user->ID, true);
// 	} else {
// 		//register user
// 		//set auth cookie
// 	}
// return $wp_user;
// }

 function transliterate($input){
$gost = array(
   "Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"-","є"=>"ye","ѓ"=>"g",
   "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
   "Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
   "З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
   "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
   "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
   "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
   "Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
   "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
   "е"=>"e","ё"=>"yo","ж"=>"zh",
   "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
   "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
   "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
   "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
   " "=>"_","—"=>"_",","=>"_","!"=>"_","@"=>"_",
   "#"=>"-","$"=>"","%"=>"","^"=>"","&"=>"","*"=>"",
   "("=>"",")"=>"","+"=>"","="=>"",";"=>"",":"=>"",
   "'"=>""
  );

return strtr($input, $gost);
}

function insert_soc_user($userdata) {
	$user_login = $userdata['user_login'];
	$user_pass = $userdata['user_pass'];
	$user_id = wp_insert_user($userdata);

		if (!is_wp_error($user_id)) {
			//echo 'SUCCESS';

				signon_soc_user($user_login, $user_pass);
			
	    } else {
	        //echo 'ERROR';
	        print_var($user_id);
	    }
	    return $user_id;
	}

function signon_soc_user($user_login, $user_pass)
{

	$info = array();
    $info['user_login'] = $user_login;
    $info['user_password'] = $user_pass;
    $info['remember'] = true;
   $user_signon = wp_signon($info, false);
    echo '<script>window.opener.location.reload(); window.close();</script>';
 //     if (!is_wp_error($user_signon)) {
 //    	echo 'success auth';
	// } else {
 //        echo 'error auth';
 //    }

}

function sex_soc_user($sex) {

	    if ($sex == 'male' || $sex == 2) {
	    	$sex = 'M';
	    }elseif ($sex == 'female' || $sex == 1) {
	    	$sex = 'F';
	    }else{
	    	$sex = ' ';
	    }
	return $sex;
} 