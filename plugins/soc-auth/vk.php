<?php
require "soc-auth.php";

if (isset($_GET['code'])) {
    $result = false;
    $params = array(
        'client_id' => CLIENT_ID_VK,
        'client_secret' => CLIENT_SECRET_VK,
        'code' => $_GET['code'],
        'redirect_uri' => REDIRECT_URL . 'vk.php'
    );

    $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

    if (isset($token['access_token'])) {
        $params = array(
            'uids'         => $token['user_id'],
            'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,city,photo_big',
            'access_token' => $token['access_token']
        );

        $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
        if (isset($userInfo['response'][0]['uid'])) {
            $userInfo = $userInfo['response'][0];
            $result = true;
        }
    }

    if ($result) {

        $user_login = $userInfo['uid'];
        $user_pass = $userInfo['screen_name'];

        $user = username_exists($userInfo['uid']);

            if ($user) {
                signon_soc_user($user_login, $user_pass);
                
            }else{
                
                $userdata = array(
                'user_pass'       => $userInfo['screen_name'], 
                'user_login'      => $userInfo['uid'], 
                'user_nicename'   => transliterate($userInfo['last_name']),
                'display_name'    => transliterate($userInfo['last_name']),
                'nickname'        => transliterate($userInfo['last_name']),
                'first_name'      => $userInfo['first_name'],
                'last_name'       => $userInfo['last_name'],

                );  

                $user_id = insert_soc_user($userdata);

                $sex = $userInfo['sex'];
                $sex = sex_soc_user($sex);

                add_user_meta($user_id, 'birthday', $userInfo['bdate'], true);
                add_user_meta($user_id, 'gender', $sex, true);
                add_user_meta($user_id, 'avatar_small', $userInfo['photo_big'], true);
            }
             
        }
    }
?>