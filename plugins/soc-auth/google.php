<?php
require "soc-auth.php";
if (isset($_GET['code'])) {
	$result = false;

    $params = array(
        'client_id'     => CLIENT_ID_GOOGLE,
        'client_secret' => CLIENT_SECRET_GOOGLE,
        'redirect_uri'  => REDIRECT_URL . 'google.php',
        'grant_type'    => 'authorization_code',
        'code'          => $_GET['code']
    );

	$url = 'https://accounts.google.com/o/oauth2/token';
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
    curl_close($curl);

    $tokenInfo = json_decode($result, true);

            if (isset($tokenInfo['access_token'])) {
        $params['access_token'] = $tokenInfo['access_token'];

        $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
        if (isset($userInfo['id'])) {
            $userInfo = $userInfo;
            $result = true;
        }
    }

    if ($result) {
        $user_login = $userInfo['id'];
        $user_pass = $userInfo['id'];

        $user = username_exists($userInfo['id']);

            if ($user) {
                signon_soc_user($user_login, $user_pass);
                
            }else{
                
                $userdata = array(
                'user_pass'       => $userInfo['id'], 
                'user_login'      => $userInfo['id'], 
                // 'user_email'      => $userInfo['email'],
                'user_nicename'   => transliterate($userInfo['family_name']),
                'display_name'    => transliterate($userInfo['family_name']),
                'nickname'        => transliterate($userInfo['family_name']),
                'first_name'      => $userInfo['given_name'],
                'last_name'       => $userInfo['family_name'],

                );  

                $user_id = insert_soc_user($userdata);

                $sex = $userInfo['gender'];
                $sex = sex_soc_user($sex);

                add_user_meta($user_id, 'gender', $sex, true);
                add_user_meta($user_id, 'avatar_small', $userInfo['picture'], true);
                add_user_mate($user_id, 'enter_from', 'google', true);
            }
    }