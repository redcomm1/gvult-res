<?php

$settings = array(
	'media_buttons' => false,
	'editor_class' => 'form'
);

$current_user = wp_get_current_user();
$user_id = $current_user->data->ID;

$inbox_url = add_query_arg('action', 'inbox', home_url().'/soobshheniya');
$sent_url = add_query_arg('action', 'sent', home_url().'/soobshheniya');
$send_message_url = add_query_arg('action', 'send', home_url().'/soobshheniya');

if (isset($_GET['action'])) $action = $_GET['action']; else $action = '';
if (isset($_GET['to'])) {
	$to = $_GET['to'];
	$recipient_name = get_user_by('ID', $to)->user_login;
	} else {
		$to = '';
		$recipient_name = '';
	}
if (isset($_GET['title'])) {
	$preset_title = $_GET['title'];
} else $preset_title = '';
if (isset($_GET['type'])) $type = $_GET['type']; else $type = '';
if (isset($_GET['message_id'])) $message_id = $_GET['message_id']; else $message_id = 0;

function place_message_bar($message) {
	// print_var($message);
	$user = get_user_by('ID', $message->sender);
	$metadata = get_user_meta($user->ID);
	$user->first_name = $metadata['first_name'][0];
	$user->last_name = $metadata['last_name'][0];
	// print_var($user);
	if (isset($_GET['action'])) $action = $_GET['action']; else $action = 'inbox';
	$view_message_url = add_query_arg(array('action' => 'view', 'type' => $action, 'message_id' => $message->id), home_url().'/soobshheniya');
	// print_var($view_message_url);
	if (function_exists('get_small_avatar')) $avatar_url = get_small_avatar($message->sender); else $avatar_url = '';
	?>
	<a href="<?php echo $view_message_url; ?>" class="message" data-id="<?php echo $message->id; ?>">
    	<div class="img" style="background-image: url(<?php echo $avatar_url; ?>)"></div>
    	<div class="text">
    		<p class="info"><span class="name"><?php echo $user->user_login.' '.$user->first_name.' '.$user->last_name; ?></span><span class="time icon-clock"><?php echo $message->msg_date; ?></span></p>
    		<p class="message-text"><?php if ($message->status == 'unread') echo '<b>'.$message->title.'</b>'; else echo $message->title; ?></p>
    	</div>
		<div class="checkbox checkbox-success">
    		<input type="checkbox" id="checkbox<?php echo $message->id; ?>" name="<?php echo $message->id; ?>" class="">
		    <label for="checkbox<?php echo $message->id; ?>"></label>
		</div>
    </a>
    <?php
}

?>

<style>
.mce-edit-area {
    background-color: #eee!important;
    padding: 13px;
    font-family: 'PTSansRegular';
    font-size: 14px;
    color: #888;
    border: none;
}
#recipient-wrapper {
	position: relative;
}
#user-list {
	display: none;
	position: absolute;
	z-index: 100;
	top: 40px;
	list-style-type: none;
}
#user-list li {
	background-color: white;
	border: 1px solid #ccc;
}
#user-list li:hover {
	cursor: pointer;
	background-color: blue;
	color: white;
}
</style>

<div class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<?php
			if (is_user_logged_in()) {
				switch ($action) {
					case 'send': ?>
					<div class="profile-block">
						<h1>Написать сообщение</h1>
						<div class="profile padding_bot_0">
							<div class="row">
								<form action="" id="new-message">
									<div id="message-edit">
										<div class="col-md-10 col-sm-10">
											<input type="hidden" id="sender_id" value="<?php echo get_current_user_id(); ?>">
											<div class="form-group" id="recipient-wrapper">
												<input type="text" class="form-control" placeholder="Кому" value="<?php echo $recipient_name; ?>" id="recipient_name">
												<input type="hidden" name="recipient_id" id="recipient_id" value="<?php echo $to; ?>">
												<ul id="user-list"></ul>
												<input type="hidden" id="user_ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
											</div>
										</div>
										<div class="col-md-10 col-sm-10">
											<div class="form-group">
												<input type="text" class="form-control" placeholder="Тема сообщения" name="message_title" id="message_title" value="<?php echo $preset_title; ?>">
											</div>
										</div>
										<div class="col-md-12 col-sm-12">
											<div class="form-group">
												<?php wp_editor('', 'new-message-editor', $settings); ?>
								        		<div class="checkbox checkbox-success">
									        		<input type="checkbox" id="save-sent" name="save-sent">
												    <label for="save-sent">Сохранить сообщение в папке «Отправленные»</label>
												</div>
											</div>
										</div>
									</div>
									<div id="message-preview" style="display:none">
										<div class="tab-content">
										    <div id="enter" class="tab-pane fade in active">
										        <div class="header_message">
										        	<div class="man">
										        		<?php if (function_exists('get_small_avatar')) $avatar_url = get_small_avatar($message->sender); else $avatar_url = ''; ?>
											        	<div class="img" style="background-image: url(<?php echo $avatar_url; ?>)"></div>
											        	<div class="text">
											        		<p class="info"><span class="name"><?php echo $current_user->user_login.' '.$current_user->first_name.' '.$current_user->last_name; ?></span><br><span class="time icon-clock"><?php echo date("Y-m-d H:i:s"); ?></span></p>
											        	</div>	
										        	</div>
										        	<div class="group-btn col-xs-0">
										        	</div>
										        </div>
										        <div class="body_message" id="preview-body">
										        	<p class="title"><?php echo $message->title; ?></p>
										        	<?php echo $message->message; ?>
										        </div>
										    </div>
										</div>
									</div>
									<div class="col-md-3 col-md-offset-0 col-sm-offset-0 col-sm-3 col-xs-8 col-xs-offset-2">
										<a href="javascript:void(0)" class="btn btn-default col-xs-mb" id="preview-button">Просмотр</a>
									</div>
									<div class="col-md-3 col-md-offset-6 col-sm-3 col-sm-offset-6 col-xs-8 col-xs-offset-2">
										<a href="javascript:void(0)" class="btn btn-send" onclick="jQuery('#new-message').submit()">Отправить</a>
									</div>
									<p id="new-message-status"></p>
									<?php wp_nonce_field('ajax-message-nonce', 'security'); ?>
								</form>
							</div>	
						</div>
					</div> <?php
					break;
					case 'sent':
					$messages = get_sent_messages();
					?>
					<div class="profile-block">
						<h1>Ваши сообщения</h1>
						<div class="profile">
							<div class="row">
								<form class="col-md-12 col-sm-12" id="messages-list-form">
									<ul class="nav nav-tabs messages">
									    <li><a href="<?php echo $inbox_url; ?>">Входящие</a></li>
									    <li class="active"><a href="<?php echo $sent_url; ?>">Отправленные</a></li>
									    <li><a href="<?php echo $send_message_url; ?>">Отправить</a></li>
									</ul>
									<div class="tab-content">
									    <div class="tab-pane fade in active">
									    	<?php
									        foreach ($messages as $message) {
									        	place_message_bar($message);
									        }
									        ?>
									    </div>
									</div>
									<div class="row">
										<div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9">
											<select class="form-control" name="method">
											  <option value="delete">Удалить</option>
											</select>
											<a href="javascript:void(0)" class="btn-done" id="messages-list-action" data-type="sent">Выполнить</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php
					break;
					case 'view':
					$back_url = add_query_arg('action', $type, home_url().'/soobshheniya');
					$message = get_private_message($message_id);
					$user = get_user_by('ID', $message->sender);
					if (($type == 'sent' && $message->sender != $user_id) || ($type == 'inbox' && $message->recipient != $user_id)) {
						?>
						<div class="profile-block">
							<h1>Вы не имеете прав на просмотр этого сообщения.</h1>
						</div>
						<?php
						break;
					}
					$metadata = get_user_meta($user->ID);
					$user->first_name = $metadata['first_name'][0];
					$user->last_name = $metadata['last_name'][0];
					?>
					<div class="profile-block">
						<h1>Ваши сообщения</h1>
						<div class="profile">
							<div class="row">
								<div class="col-md-12">
									<ul class="nav nav-tabs messages">
									    <li<?php if ($type == 'inbox') echo ' class="active"'; ?>><a href="<?php echo $inbox_url; ?>">Входящие</a></li>
									    <li<?php if ($type == 'sent') echo ' class="active"'; ?>><a href="<?php echo $sent_url; ?>">Отправленные</a></li>
									    <li><a href="<?php echo $send_message_url; ?>">Отправить</a></li>
									</ul>
									<div class="tab-content">
									    <div id="enter" class="tab-pane fade in active">
									        <div class="header_message">
									        	<div class="man">
									        		<a href="<?php echo $back_url; ?>" class="left_arrow">&#8249;</a>
									        		<?php if (function_exists('get_small_avatar')) $avatar_url = get_small_avatar($message->sender); else $avatar_url = ''; ?>
										        	<div class="img" style="background-image: url(<?php echo $avatar_url; ?>)"></div>
										        	<div class="text">
										        		<p class="info"><span class="name"><?php echo $user->user_login.' '.$user->first_name.' '.$user->last_name; ?></span><br><span class="time icon-clock"><?php echo $message->msg_date; ?></span></p>
										        	</div>	
									        	</div>
									        	<div class="group-btn col-xs-0">
									        		<?php if ($type == 'sent') { ?>
									        			<a href="" id="message-delete" data-type="<?php echo $type; ?>" data-id="<?php echo $message_id; ?>">Удалить</a>
									        		<?php } else {
									        			$send_message_to_url = add_query_arg(array('action' => 'send', 'to' => $message->sender), home_url().'/soobshheniya');
									        			$report_address = get_user_by('login', 'dmitriythomas')->ID;
									        			$report_message_url = add_query_arg(array('action' => 'send', 'to' => $report_address, 'title' => 'Жалоба на сообщение id '.$message->id), home_url().'/soobshheniya');
								        			?>
									        		<a href="<?php echo $report_message_url; ?>">Пожаловаться</a>
									        		<a href="<?php echo $send_message_to_url; ?>">Ответить</a>
									        		<a href="javascript:void(0)" id="message-delete" data-type="<?php echo $type; ?>" data-id="<?php echo $message_id; ?>">Удалить</a>
									        		<?php } ?>
									        	</div>
									        </div>
									        <div class="body_message">
									        	<p class="title"><?php echo $message->title; ?></p>
									        	<?php echo $message->message; ?>
									        </div>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
					break;
					case 'inbox':
					default:
					$messages = get_inbox_messages();
					?>
					<div class="profile-block">
						<h1>Ваши сообщения</h1>
						<div class="profile">
							<div class="row">
								<form class="col-md-12 col-sm-12" id="messages-list-form">
									<ul class="nav nav-tabs messages">
									    <li class="active"><a href="<?php echo $inbox_url; ?>">Входящие</a></li>
									    <li><a href="<?php echo $sent_url; ?>">Отправленные</a></li>
									    <li><a href="<?php echo $send_message_url; ?>">Отправить</a></li>
									</ul>
									<div class="tab-content">
									    <div class="tab-pane fade in active">
									    	<?php
									        foreach ($messages as $message) {
									        	place_message_bar($message);
									        }
									        ?>
									    </div>
									</div>
									<div class="row">
										<div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9">
											<select class="form-control" name="method">
											  <option value="delete">Удалить</option>
											  <option value="mark_read">Пометить как прочитанные</option>
											  <option value="mark_unread">Пометить как непрочитанные</option>
											</select>
											<a href="javascript:void(0)" class="btn-done" id="messages-list-action" data-type="inbox">Выполнить</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php
					break;
				} ?>
				
				<?php } else { ?>
				<div class="profile-block">
					<h1>Пожалуйста, войдите в систему, чтобы пользоваться личными сообщениями.</h1>
				</div>
				<?php } ?>
		</div>
	</div>
</div>