<?php
/*
Plugin Name: Private Messages
Description: Private messages system
Author: Alex Kravchenko
Version: 1.0.1
*/

register_activation_hook(__FILE__, 'pe_poll_install');

global $pe_privat_messages_db_version;
$pe_privat_messages_db_version = '1.0';

function pe_poll_install() {
	global $wpdb;
	global $pe_privat_messages_db_version;
	$table_name = $wpdb->prefix . 'messages';
	if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE " . $table_name . " (
			id MEDIUMINT(9)	NOT NULL AUTO_INCREMENT,
			sender INT(11),
			recipient INT(11),
			msg_date DATETIME,
			title VARCHAR(255),
			message TEXT,
			status VARCHAR(70),
			owner INT(11),
			ip VARCHAR(15),
			UNIQUE KEY id (id)
		);";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		add_option('pe_privat_messages_db_version', $pe_privat_messages_db_version);
	}
	$installed_version = get_option( 'pe_privat_messages_db_version' );
	if ( $installed_version != $pe_privat_messages_db_version ) {
		// Create new table
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		add_option('pe_privat_messages_db_version', $pe_privat_messages_db_version);
	}
}

function get_messages_page_template() {
	include(plugin_dir_path( __FILE__ ).'/page_template.php');
}

function ajax_sendmessage_script_init() {
	global $pagename;
	if ($pagename == 'soobshheniya') {// && $_GET['action'] == 'send'
		wp_register_script('ajax-message-script', plugins_url('/private-messages').'/ajax-message-script.js', array('jquery')); 
	    wp_enqueue_script('ajax-message-script');
	    wp_localize_script('ajax-message-script', 'ajax_message_object', array( 
	        'ajaxurl' => admin_url('admin-ajax.php'),
	        'redirecturl' => home_url().'/soobshheniya',
	        'loadingmessage' => __('Обработка запроса...')
	    ));
	    wp_localize_script('ajax-message-script', 'ajax_search_user_object', array( 
	        'ajaxurl' => admin_url('admin-ajax.php'),
	        'redirecturl' => home_url().'/soobshheniya',
	        'loadingmessage' => __('Обработка запроса...')
	    ));
	}
}
add_action('get_header', 'ajax_sendmessage_script_init');

function ajax_sendmessage_ajax_init() {
	add_action('wp_ajax_ajaxsearchuser', 'ajax_search_user');
	add_action('wp_ajax_ajaxmessage', 'ajax_new_message');
	add_action('wp_ajax_ajaxdeletemessage', 'ajax_delete_message');
	add_action('wp_ajax_ajaxmarkreadmessage', 'ajax_mark_read_message');
	add_action('wp_ajax_ajaxmessagesaction', 'ajax_messages_action');
}
add_action('init', 'ajax_sendmessage_ajax_init');

function ajax_new_message() {
	check_ajax_referer('ajax-message-nonce', 'security');
	$sender_id = $_POST['sender_id'];
	$recipient_id = $_POST['recipient_id'];
	$title = $_POST['title'];
	$content = $_POST['content'];
	$save_to_sent = ($_POST['save_to_sent'] == 'false') ? false : true;
	$userip = $_POST['user_ip'];
	echo send_private_message($sender_id, $recipient_id, $title, $content, $userip, $save_to_sent);
	die();
}

function ajax_search_user() {
	$search = $_POST['user'];
	$users_query = new WP_User_Query( array(
	    'search'         => '*'.esc_attr($search).'*',
	    'search_columns' => array(
	        'user_login',
	        'user_nicename',
	        'user_email',
	        'user_url',
	    ),
	) );
	$users = $users_query->get_results();
	$results = array();
	foreach ($users as $user) {
		$metadata = get_user_meta($user->ID);
		$user->first_name = $metadata['first_name'][0];
		$user->last_name = $metadata['last_name'][0];
		array_push($results, $user->data);
	}
	echo json_encode($results);
	die();
}

function ajax_delete_message() {
	echo delete_messages($_POST['id']);
	die();
}

function ajax_mark_read_message() {
	$message_id = $_POST['id'];
	$message = get_private_message($message_id);
	if ($message->status != 'sent') {
		echo mark_messages('read', $message_id);
	} else echo 'sent';
}

function ajax_messages_action() {
	$method = $_POST['method'];
	unset($_POST['action']);
	unset($_POST['method']);
	$ids = array();
    foreach($_POST as $key => $value) {
        array_push($ids, $key);
    }
    switch ($method) {
    	case 'delete':
    		$result = delete_messages($ids);
    		break;
		case 'mark_read':
			$result = mark_messages('read', $ids);
			break;
		case 'mark_unread':
			$result = mark_messages('unread', $ids);
			break;
    	default:
    		break;
    }
    echo $result;
	die();
}

function send_private_message($sender_id, $recipient_id, $title, $message, $userip, $save_in_sent = false) {
	global $wpdb;
	$date = date("Y-m-d H:i:s");
	$status = 'unread';
	$query = $wpdb->prepare("INSERT INTO ".$wpdb->prefix."messages(sender, recipient, msg_date, title, message, status, owner, ip) VALUES (%d, %d, %s, %s, %s, %s, %d, %s)", $sender_id, $recipient_id, $date, $title, $message, $status, $recipient_id, $userip);
	$wpdb->query($query);
	if($wpdb->last_error !== '') $wpdb->print_error();
	$wpdb->flush();
	if ($save_in_sent) {
		$status = 'sent';
		$wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix."messages(sender, recipient, msg_date, title, message, status, owner, ip) VALUES (%d, %d, %s, %s, %s, %s, %d, %s)", $sender_id, $recipient_id, $date, $title, $message, $status, $sender_id, $userip));
		if($wpdb->last_error !== '') $wpdb->print_error();
		$wpdb->flush();
	}
	return true;
}

function get_inbox_messages($filter = 'all') {
	global $wpdb;
	$current_user = wp_get_current_user();
	if ($filter == 'unread') $query_option = " AND status = 'unread'"; else $query_option = '';
	$messages = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."messages WHERE recipient = '".$current_user->ID."'".$query_option." AND NOT status = 'sent' ORDER BY msg_date DESC");
	return $messages;
}

function get_sent_messages() {
	global $wpdb;
	$current_user = wp_get_current_user();
	$messages = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."messages WHERE sender = '".$current_user->ID."' AND status = 'sent' ORDER BY msg_date DESC");
	return $messages;
}

function get_private_message($id) {
	global $wpdb;
	$messages = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."messages WHERE id = '".$id."' LIMIT 1");
	return $messages[0];
}

function delete_messages($ids) {
	global $wpdb;
	if (is_array($ids)) {
		$in = '(';
		foreach ($ids as $id) {
			$in .= $id.',';
		}
		$in = substr_replace($in, ')', strlen($in) - 1);
		$query = $wpdb->prepare("DELETE FROM ".$wpdb->prefix."messages WHERE id IN $in");
		$result = $wpdb->query($query);
		if($wpdb->last_error !== '') $wpdb->print_error();
		$wpdb->flush();
	} else $in = $ids;
	$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."messages WHERE id = '$in'");
	return true;
}

function mark_messages($status, $ids) {
	global $wpdb;
	if (is_array($ids)) {
		$in = '(';
		foreach ($ids as $id) {
			$in .= $id.',';
		}
		$in = substr_replace($in, ')', strlen($in) - 1);
		$query = $wpdb->prepare("UPDATE ".$wpdb->prefix."messages SET status = '$status' WHERE id IN $in");
		$result = $wpdb->query($query);
		if($wpdb->last_error !== '') $wpdb->print_error();
		$wpdb->flush();
	} else $in = $ids;
	$result = $wpdb->query("UPDATE ".$wpdb->prefix."messages SET status = '$status' WHERE id = '$in'");
	return true;
}