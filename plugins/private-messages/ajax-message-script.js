jQuery(document).ready(function($) {
    $('form#new-message').on('submit', function(e) {
        if ($('form#new-message #recipient_id').val() == '') {
            e.preventDefault();
            $('form#new-message p#new-message-status').show().text("Пожалуйста, выберите адресата!");
            return;
        }
        $('form#new-message p#new-message-status').show().text(ajax_message_object.loadingmessage);
        tinyMCE.triggerSave();
        var content = get_tinymce_content();
        var d = {
            'action'  : 'ajaxmessage',
            'sender_id': $('form#new-message #sender_id').val(),
            'user_ip': $('form#new-message #user_ip').val(),
            'recipient_id': $('form#new-message #recipient_id').val(),
            'title': $('form#new-message #message_title').val(),
            'content' : content,
            'save_to_sent' : $('form#new-message #save-sent').is(':checked'),
            'security': $('form#new-message #security').val()
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_message_object.ajaxurl,
            data: d,
            success: function(data) {
                // console.log(data);
                if (data) document.location.href = ajax_message_object.redirecturl;
                else $('form#new-message p#new-message-status').show().text("Произошла ошибка! Попробуйте еще раз.");
            },
            error: function(data) {
                console.log(data);
            }
        });
        e.preventDefault();
    });
    $('form#new-message #recipient_name').on('keyup', function(e) {
        $('form#new-message #recipient_id').val('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_search_user_object.ajaxurl,
            data: {
                'action': 'ajaxsearchuser',
                'user': $('form#new-message #recipient_name').val(), },
            success: function(data) {
                var userList = $('#user-list');
                userList.fadeOut();
                userList.html('');
                if (data.length > 0) {
                    userList.fadeIn();
                    for (var i = 0; i < data.length; i++) {
                        var user = "<li data-user='" + data[i].ID + "' onclick='selectUser(this)'>" + data[i].user_login + " " + (data[i].first_name || "") + " " + (data[i].last_name || "") + "</li>";
                        userList.append(user);
                    }
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
        e.preventDefault();
    });
    $('form#new-message #recipient_name').on('focusout', function(e) {
        $('#user-list').fadeOut();
        e.preventDefault();
    });
    $('form#new-message #recipient_name').on('focus', function(e) {
        if ($('form#new-message #recipient_name').val() != '') {
            $('#user-list').fadeIn();
        }
        e.preventDefault();
    });
    $('.message').on('click', function(e) {
        var id = this.dataset.id;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_message_object.ajaxurl,
            data: {
                'action': 'ajaxmarkreadmessage',
                'id': id },
            success: function(data) {
                // console.log(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
    $('#message-delete').on('click', function(e) {
        var type = this.dataset.type;
        var id = this.dataset.id;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_message_object.ajaxurl,
            data: {
                'action': 'ajaxdeletemessage',
                'type': type,
                'id': id },
            success: function(data) {
                // console.log(data);
                if (data) window.location.href = '/soobshheniya?action=' + type;
            },
            error: function(data) {
                console.log(data);
            }
        });
        e.preventDefault();
    });
    $.fn.serializeObject = function() {
       var o = {};
       var a = this.serializeArray();
       $.each(a, function() {
           if (o[this.name]) {
               if (!o[this.name].push) {
                   o[this.name] = [o[this.name]];
               }
               o[this.name].push(this.value || '');
           } else {
               o[this.name] = this.value || '';
           }
       });
       return o;
    };
    $('#messages-list-action').on('click', function(e) {
        var type = this.dataset.type;
        // console.log($('form#messages-list-form').serializeObject());
        var data = $('form#messages-list-form').serializeObject();
        data['action'] = 'ajaxmessagesaction'
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_message_object.ajaxurl,
            data: data,
            success: function(data) {
                // console.log(data);
                if (data) window.location.href = '/soobshheniya?action=' + type;
            },
            error: function(data) {
                console.log(data);
            }
        });
        e.preventDefault();
    });
    $('#preview-button').on('click', function(e) {
        if ($('#preview-button').html() == 'Просмотр') {
            $('#preview-button').html('Редактирование');
            $('#message-edit').hide();
            tinyMCE.triggerSave();
            var content = get_tinymce_content();
            $('#preview-body').html('<p class="title">' + $('form#new-message #message_title').val() + '</p>' + content);
            $('#message-preview').show();
        } else {
            $('#preview-button').html('Просмотр');
            $('#message-preview').hide();
            $('#message-edit').show();
        }
        e.preventDefault();
    });
});

function selectUser(listItem) {
    var user;
    jQuery('#user-list').fadeOut();
    jQuery('form#new-message #recipient_name').val(listItem.innerHTML);
    if (listItem.dataset.user != '') {
        user = '<a href="' + listItem.dataset.user + '">' + listItem.innerHTML + '</a>';
    }
    jQuery('#recipient_id').val(listItem.dataset.user);
}

function get_tinymce_content() {
    if (jQuery("#new-message-editor").hasClass("tmce-active")) {
        return tinyMCE.activeEditor.getContent();
    } else {
        return jQuery('#new-message-editor').val();
    }
}