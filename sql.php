<?php

////*
//  
//	Версия: 1.0.0
//	Автор: Драный В.В.
//  E-mail: navshop@ukr.net
//	Описание: Скрипт копирует данные из базы данных dle в базу данных wordpress
//				(решает узко конкретную задачу)
//
//	ВАЖНО! Скрипт затирает все данных в таблицах term_relationships,
//			term_taxonomy, term_terms в новой базе данных перед вставкой
//			данных их старой базы данных
//
//  Перед запуском нужно определить константы
//
///*/

// Доступ к базам данных
define(HOST, '127.0.0.1');
define(USER_NAME, 'root');
define(PASSWORD, '');
define(PORT, 3306);

define(NEW_DB, 'gvult');
define(OLD_DB, 'dle');
define(PREFIX, 'gv_'); // префикс таблиц в новой базе данных

$mysqli = new mysqli(HOST, USER_NAME, PASSWORD, "", PORT);

if ($mysqli->connect_errno)
{
    echo "Can't connect to MySQL: \n";
    echo "Number error: " . $mysqli->connect_errno;
    echo "Error: " . $mysqli->connect_error . "\n";

    exit;
}

//Ф-ции
	function ss_show_message( $table = '' )
	{
		if ( !empty($table) )
		{
			echo '<pre>';
			print_r("The table '".$table."' migration is complete!\n");
			echo '</pre>';	
		}
	}

	function ss_check_result($result, $table = '')
	{
		global $sql;
		global $mysqli;

		if ( !$result )
		{
			echo "Error: Your requst don't work:<br>";
		    echo "Requst: " . $sql . "<br>";
		    echo "# error: " . $mysqli->connect_errno . "<br>";
		    echo "Error: " . $mysqli->connect_error . "<br>";

		    exit;
		}

		ss_show_message( $table );
	}

	// Ф-ция возвращает ассоциативный массив, на входе $arr - обычный массив массивов, $indexKey - индекс места где хранится имя ключа, $indexValue - индекс места где хранитца значение для данного ключа
		function ss_createAssociativeArray($arr, $indexKey, $indexValue)
		{
			$arrLength = count($arr);
			for ( $i = 0; $i < $arrLength; $i++ )
				$arrNew[$arr[$i][$indexKey]] = $arr[$i][$indexValue];

			return $arrNew;
		}

		

// // Копируем юзеров из dle.dle_users ы gvult.ss_users
// 	$sql = "INSERT INTO ".NEW_DB.".".PREFIX."users (ID, user_login, user_pass, user_nicename, user_email, user_registered, display_name)
// 			SELECT user_id, name, password, LOWER(name), email, FROM_UNIXTIME(reg_date), name
// 			FROM ".OLD_DB.".dle_users
// 			WHERE user_id != 1";

// 	$result = $mysqli->query($sql);
// 	ss_check_result($result, 'users');

// // Получаем данные из OLD_DB таблицы dle_users для добавления в таблицу gvult.ss_usermeta
// 	$sql = "SELECT user_id, name, fullname, info, user_group
// 			FROM ".OLD_DB.".dle_users
// 			WHERE user_id != 1";

// 	$result = $mysqli->query($sql);
// 	ss_check_result($result);
// 	$arrValueFromDBDle = $result->fetch_all();
// 	$arrValueFromDBDleLength = count($arrValueFromDBDle);

// // Добавляем поля в NEW_DB таблицы ss_usermeta
// 	$sql = "INSERT INTO ".NEW_DB.".".PREFIX."usermeta ( user_id, meta_key, meta_value )
// 			VALUES ( ?, ?, ? )";
// 	$stmt = $mysqli->prepare($sql);

// 	if ( !$stmt )
// 		echo "Can't prepared request: (" . $mysqli->errno . ") " . $mysqli->error;

// 	$user_id = 0;
// 	$meta_key = '';
// 	$meta_value = '';

// 	if ( !$stmt->bind_param('iss', $user_id, $meta_key, $meta_value ) )
// 		echo "Can't bind params: (" . $mysqli->errno . ") " . $mysqli->error;

// 	$objMetaKey = array
// 	(
// 		'nickname'								=> '',
// 		'first_name'							=> '',
// 		'last_name'								=> '',
// 		'description'							=> '',
// 		'rich_editing'							=> '',
// 		'comment_shortcuts'						=> true,
// 		'admin_color'							=> 'fresh',
// 		'use_ssl'								=> 0,
// 		'show_admin_bar_front'					=> true,
// 		'locale'								=> '',
// 		'ss_capabilities'						=> '',
// 		'ss_user_level'							=> 0,
// 		'dismissed_wp_pointers'					=> '',
// 		'show_welcome_panel'					=> 1,
// 		'ss_dashboard_quick_press_last_post_id'	=> 3
// 	);

// 	for ($i=0; $i < $arrValueFromDBDleLength; $i++)
// 	{ 
// 		foreach ($objMetaKey as $metaKey => $metaValue)
// 		{
// 			if ( $metaKey == 'nickname' )
// 				$metaValue = $arrValueFromDBDle[$i][1];
// 			else if ( $metaKey == 'first_name' )
// 				$metaValue = $arrValueFromDBDle[$i][2];
// 			else if ( $metaKey == 'description' )
// 				$metaValue = $arrValueFromDBDle[$i][3];
// 			else if ( $metaKey == 'ss_capabilities' )
// 			{
// 				if ( $arrValueFromDBDle[$i][4] == 3 )
// 					$metaValue = 'a:1:{s:13:"administrator";b:1;}';
// 				else
// 					$metaValue = 'a:1:{s:10:"subscriber";b:1;}';
// 			}
// 			else if ( $metaKey == 'ss_user_level')
// 			{
// 				if ( $arrValueFromDBDle[$i][4] == 3 )
// 					$metaValue = 10;
// 			}

// 			$user_id = $arrValueFromDBDle[$i][0];
// 			$meta_key = $metaKey;
// 			$meta_value = $metaValue;
		
// 			if ( !$stmt->execute() )
// 				echo "Can't complete request: (" . $mysqli->errno . ") " . $mysqli->error;
// 		}
// 	}

// 	ss_show_message( 'usermeta' );

// // Копируем посты
// 	// Формируем ссылки для постов
// 		// Вытаскиваем id постов и категорий
// 			$sql = "SELECT id, category
// 				FROM ".OLD_DB.".dle_post";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);
// 			$arrIdCategoriesOfPost = $result->fetch_all();
// 			$arrIdCategoriesOfPostLength = count($arrIdCategoriesOfPost);

// 		// Вытаскиваем id, названия и родителя категорий
// 			$sql = "SELECT id, alt_name, parentid
// 				FROM ".OLD_DB.".dle_category";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result); 
// 			$arrCategoriesAndParentCat = $result->fetch_all();
			
// 		// Вытаскиваем id постов и ссылку на картинку 
// 			$sql = "SELECT id, xfields
// 				FROM ".OLD_DB.".dle_post";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);
// 			$arrLinksImgs = $result->fetch_all();
// 			$arrLinksImgs = ss_createAssociativeArray($arrLinksImgs, 0, 1);
// 			$arrLinksImgsLength = count($arrLinksImgs);

// 		// // Вытаскиваем id постов и ссылку на картинку из старой базы данных таблицы dle_images
// 			$sql = "SELECT news_id, images
// 				FROM ".OLD_DB.".dle_images";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);
// 			$arrImgs = $result->fetch_all();
// 			$arrImgs = ss_createAssociativeArray($arrImgs, 0, 1);
// 			$arrImgsLength = count($arrImgs);

// 			foreach ($arrImgs as $post_id => $linksImgs)
// 			{
// 				$arrImgs[$post_id] = explode('|||', $linksImgs)[0];	
// 			}

// 		// Вытаскиваем контент
// 			$sql = "SELECT id, short_story, full_story
// 				FROM ".OLD_DB.".dle_post";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);

// 			$contents = $result->fetch_all();

// 			foreach ($contents as $key => $content)
// 			{
// 				if ( !empty($content[1]) )
// 				{
// 					// удаляем первую картинку из короткой записи старой базы данных
// 					$startPos = strpos($content[1], '<img');

// 					if ( $startPos !== false )
// 					{
// 						$endPos = strpos($content[1], '>', $startPos);
// 						$lengthStr = $endPos - $startPos +1;

// 						$subStr = substr($content[1], $startPos, $lengthStr);

// 						$content[1] = str_replace($subStr, '', $content[1]);	
// 					}

// 					$contents[$key][1] = $content[1].' '.$content[2];
// 				}
// 				else
// 					$contents[$key][1] = $content[2];
// 			}

// 			$arrContents = ss_createAssociativeArray($contents, 0, 1);

// 			foreach ($arrContents as $post_id => $content)
// 			{
// 				$arrContents[$post_id] = stripslashes($content);
// 			}   	

// 		// Формируем массив ключ - id категории, значение - название категории
// 			$arrNameCategories = ss_createAssociativeArray($arrCategoriesAndParentCat, 0, 1);
// 			$arrIdParents = ss_createAssociativeArray($arrCategoriesAndParentCat, 0, 2);

// 		// Формируем текстовую ссылку $strPath
// 			$copyArrIdCategoriesOfPost = $arrIdCategoriesOfPost;
// 			$copyArrIdCategoriesOfPostLength = $arrIdCategoriesOfPostLength;

// 		 	for ( $i = 0; $i < $copyArrIdCategoriesOfPostLength; $i++ )
// 			{
// 				// Оставляем первую id категории
// 					$pos = strpos($copyArrIdCategoriesOfPost[$i][1], ',');

// 					if ( $pos )
// 						$copyArrIdCategoriesOfPost[$i][1] = substr($copyArrIdCategoriesOfPost[$i][1], 0, strpos($copyArrIdCategoriesOfPost[$i][1], ','));

// 				// Составляем ссылку
// 					$idParent = $arrIdParents[$copyArrIdCategoriesOfPost[$i][1]];
// 						$strPath[$copyArrIdCategoriesOfPost[$i][0]] = '/'.$arrNameCategories[$copyArrIdCategoriesOfPost[$i][1]];
// 					if ( $copyArrIdCategoriesOfPost[$i][1] != 2 )
// 					{
// 						while ( $idParent )
// 						{
// 							$strPath[$copyArrIdCategoriesOfPost[$i][0]] .= '/'.$arrNameCategories[$idParent];

// 							$idParent = $arrIdParents[$idParent];
// 						}

// 						$strPath[$copyArrIdCategoriesOfPost[$i][0]] = $_SERVER['HTTP_HOST'].'/'.implode('/', array_reverse(explode('/', $strPath[$copyArrIdCategoriesOfPost[$i][0]])));
// 					}
// 			}

// 		// Формируем типы постов
// 			$arrTypes = [];
// 			for ($i = 0; $i < $copyArrIdCategoriesOfPostLength; $i++)
// 			{
// 				if ( $copyArrIdCategoriesOfPost[$i][1] == 39 || !array_key_exists($copyArrIdCategoriesOfPost[$i][1], $arrNameCategories) )
// 					$arrTypes[$copyArrIdCategoriesOfPost[$i][0]] = 'post';
// 				else if ( $copyArrIdCategoriesOfPost[$i][1] == 607 )
// 					$arrTypes[$copyArrIdCategoriesOfPost[$i][0]] = 'review';
// 				else
// 					$arrTypes[$copyArrIdCategoriesOfPost[$i][0]] = 'places';
// 			}

// 		// Формируем ссылки на картинки
// 			// получаем наибольший id
// 			$sql = "SELECT MAX(id)
// 					FROM ".OLD_DB.".dle_post";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);
// 			$maxId = $result->fetch_all()[0][0];

// 			//
// 			$serverProtocol = $_SERVER['SERVER_PROTOCOL'];
// 			$httpHost = $_SERVER['HTTP_HOST'];

// 			foreach ($arrLinksImgs as $post_id => $link)
// 			{

// 				// получаем мета поля
// 					$metaFields =  explode('||', $link);
// 					array_shift($metaFields);
					
// 					$tempMetaBig = [];
// 					foreach ($metaFields as $key => $meta)
// 					{
// 						$tempMeta = explode('|',  $meta);
// 						$tempMetaBig = array_merge($tempMetaBig, array($tempMeta[0] => $tempMeta[1]));
// 					}

// 					$n_metaFields[$post_id] = $tempMetaBig;
				
// 				// получаем картинки
// 					$link = explode('|', explode('||', $link)[0]);

// 					if ( !empty($link[0]) && $link[0] == 'logo' )
// 					{
// 						$link[1] = explode('/', explode('//', $link[1])[1]);
// 						array_shift($link[1]);



// 						$arrTypeImg = explode('.', $link[1][count($link[1]) - 1]);
// 						$arrTypeImgLength = count($arrTypeImg);

// 						$typeImg[$post_id] = 'image/'.$arrTypeImg[$arrTypeImgLength - 1];

// 						if ( $typeImg[$post_id] == 'image/jpg' )
// 							$typeImg[$post_id] = 'image/jpeg';

// 						$link[1] = implode('/', $link[1]);

// 						$arrLinks[$post_id] = explode('/',$serverProtocol)[0].'://'.$httpHost.'/wp-content/'.$link[1];
// 						$arrLinksShort[$post_id] = substr($link[1], strpos($link[1], 'posts'));
// 					}

// 					if ( empty($link[0]) || $link[0] != 'logo' )
// 					{
// 						$arrTypeImg = explode('.', $arrImgs[$post_id]);
// 						$arrTypeImgLength = count($arrTypeImg);

// 						$typeImg[$post_id] = 'image/'.$arrTypeImg[$arrTypeImgLength - 1];

// 						if ( $typeImg[$post_id] == 'image/jpg' )
// 							$typeImg[$post_id] = 'image/jpeg';

// 						$arrLinks[$post_id] = explode('/',$serverProtocol)[0].'://'.$httpHost.'/wp-content/uploads/posts/'.$arrImgs[$post_id];
// 						$arrLinksShort[$post_id] = 'posts/'.$arrImgs[$post_id];
// 					}
// 			}

// 		//Вытаскиваем id авторов
// 			$sql = "SELECT id, autor
// 				FROM ".OLD_DB.".dle_post";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);
// 			$nameUsers = $result->fetch_all();

// 			$nameUsers = ss_createAssociativeArray($nameUsers, 0, 1);

// 			$sql = "SELECT user_id, name
// 				FROM ".OLD_DB.".dle_users";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);
// 			$idUsers = $result->fetch_all();

// 			$idUsers = ss_createAssociativeArray($idUsers, 1, 0);

// 			foreach ($nameUsers as $post_id => $autor)
// 			{
// 				if ( array_key_exists($autor, $idUsers) )
// 					$nameUsers[$post_id] = $idUsers[$autor];
// 				else
// 					$nameUsers[$post_id] = 1;
// 			}

// 		// Создаем временную таблицу в старой базе данных
// 			$sql = "CREATE TABLE dle.temppost 
// 				(
// 					id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
// 					id_post INT(6),
// 					idauthor INT(6),
// 					postcontent LONGTEXT,
// 					postpath VARCHAR(255),
// 					posttype VARCHAR(255),
// 					linkimg VARCHAR(255),
// 					shortlinkimg VARCHAR(255),
// 					imgtype VARCHAR(255),
// 					uniqueid INT(6)
// 				)";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result); 
		
// 		// Копируем id постов и путь к постав в таблицу temppost OLD_DB 
// 			$sql = "INSERT INTO ".OLD_DB.".temppost ( id_post, idauthor, postcontent, postpath, posttype, linkimg, shortlinkimg, imgtype, uniqueid )
// 			VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )";
// 			$stmt = $mysqli->prepare($sql);

// 			if ( !$stmt )
// 				echo "Can't prepared request: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;

// 			$post_id = '';
// 			$author_id = '';
// 			$post_content = '';
// 			$post_path = '';
// 			$post_type = '';
// 			$link_img = '';
// 			$short_link_img = '';
// 			$img_type = '';
// 			$unique_id = $maxId;

// 			if ( !$stmt->bind_param( 'iissssssi', $post_id, $author_id, $post_content, $post_path, $post_type, $link_img, $short_link_img, $img_type, $unique_id ) )
// 				echo "Can't bind params: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;

// 			foreach ($strPath as $post_id => $post_path)
// 			{
// 				$author_id = $nameUsers[$post_id];
// 				$post_content = $arrContents[$post_id];
// 				$post_type = $arrTypes[$post_id];
// 				$link_img = $arrLinks[$post_id];
// 				$short_link_img = $arrLinksShort[$post_id];
// 				$img_type = $typeImg[$post_id];
// 				$unique_id += 1;

// 				if ( !$stmt->execute() )
// 					echo "Can't complete request: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
// 			}

// 	// Копирование постов в новую базу данных		
// 		$sql = "INSERT INTO ".NEW_DB.".".PREFIX."posts 
// 			(
// 				ID,
// 				post_author, 
// 				post_date, 
// 				post_date_gmt, 
// 				post_content, 
// 				post_title, 
// 				post_name, 
// 				post_modified, 
// 				post_modified_gmt,
// 				guid,
// 				post_type
// 			)
// 			SELECT 
// 				dle_posts.id,
// 				temppost.idauthor,
// 				dle_posts.date, 
// 				DATE_SUB(dle_posts.date, INTERVAL 4 HOUR), 
// 				temppost.postcontent,
// 				dle_posts.title, 
// 				dle_posts.alt_name, 
// 				dle_posts.date, 
// 				DATE_SUB(dle_posts.date, INTERVAL 4 HOUR),
// 				CONCAT(temppost.postpath, dle_posts.alt_name),
// 				temppost.posttype
// 			FROM 
// 				".OLD_DB.".dle_post as dle_posts,
// 				".OLD_DB.".temppost as temppost
// 			WHERE dle_posts.id = temppost.id_post";

// 		$result = $mysqli->query($sql);
// 		ss_check_result($result, 'posts');

// 	// Копируем картинки в новую базу данных
// 		// Копируем данные в ss_posts	
// 			$sql = "INSERT INTO ".NEW_DB.".".PREFIX."posts 
// 				(
// 					ID,
// 					post_author, 
// 					post_date, 
// 					post_date_gmt, 
// 					post_title, 
// 					post_status,
// 					comment_status,
// 					ping_status,
// 					post_name, 
// 					post_modified, 
// 					post_modified_gmt,
// 					post_parent,
// 					guid,
// 					post_type,
// 					post_mime_type
// 				)
// 				SELECT 
// 					temppost.uniqueid,
// 					temppost.idauthor,
// 					dle_posts.date, 
// 					DATE_SUB(dle_posts.date, INTERVAL 4 HOUR), 
// 					dle_posts.alt_name, 
// 					'inherit',
// 					'open',
// 					'closed',
// 					dle_posts.alt_name, 
// 					dle_posts.date, 
// 					DATE_SUB(dle_posts.date, INTERVAL 4 HOUR),
// 					dle_posts.id,
// 					temppost.linkimg,
// 					'attachment',
// 					temppost.imgtype
// 				FROM 
// 					".OLD_DB.".dle_post as dle_posts,
// 					".OLD_DB.".temppost as temppost
// 				WHERE dle_posts.id = temppost.id_post";

// 			$result = $mysqli->query($sql);
// 			ss_check_result($result, 'posts attacment');

// 		// Копируем данные в ss_postmeta
// 			// Вставляем _thumbnail_id
	
// 				$sql = "INSERT INTO ".NEW_DB.".".PREFIX."postmeta 
// 				(
// 					post_id,
// 					meta_key, 
// 					meta_value
// 				)
// 				SELECT 
// 					id_post,
// 					'_thumbnail_id',
// 					uniqueid
// 				FROM 
// 					".OLD_DB.".temppost
// 				WHERE linkimg != 'NULL'";
				
// 				$result = $mysqli->query($sql);
// 				ss_check_result($result, 'postmeta thumbnail');

// 			// Вставляем _wp_attached_file
// 				$sql = "INSERT INTO ".NEW_DB.".".PREFIX."postmeta 
// 				(
// 					post_id,
// 					meta_key, 
// 					meta_value
// 				)
// 				SELECT 
// 					uniqueid,
// 					'_wp_attached_file',
// 					shortlinkimg
// 				FROM 
// 					".OLD_DB.".temppost
// 				WHERE linkimg != 'NULL'";
				
// 				$result = $mysqli->query($sql);
// 				ss_check_result($result, 'postmeta file attacment');

// 			// Вставляем Мета поля
// 				$sql = "INSERT INTO ".NEW_DB.".".PREFIX."postmeta
// 				(
// 					post_id,
// 					meta_key,
// 					meta_value
// 				)
// 				VALUES ( ?, ?, ? )";

// 				$stmt = $mysqli->prepare($sql);

// 				if ( !$stmt )
// 					echo "Can't prepared request: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;

// 				$post_id = '';
// 				$meta_key = '';
// 				$meta_value = '';
				
// 				if ( !$stmt->bind_param( 'iss', $post_id, $meta_key, $meta_value ) )
// 					echo "Can't bind params: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;

// 				foreach ($n_metaFields as $post_id => $arrMeta)
// 					foreach ($arrMeta as $meta_key => $meta_value)
// 						if ( !$stmt->execute() )
// 							echo "Can't complete request: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;

// 				ss_show_message( 'postmeta metafiedls' );

// 	// Удаляем временную таблицу temppost
// 		$sql = "DROP TABLE ".OLD_DB.".temppost";
// 		$result = $mysqli->query($sql);
// 		ss_check_result($result);

// // Копируем таксономии
// 	// Копируем данные в таблицу terms
// 		$sql = "TRUNCATE ".NEW_DB.".".PREFIX."terms";
// 		$result = $mysqli->query($sql);
// 		ss_check_result($result);

// 		$sql = "INSERT INTO ".NEW_DB.".".PREFIX."terms (term_id, name, slug)
// 				SELECT id, name, alt_name
// 				FROM ".OLD_DB.".dle_category";

// 		$result = $mysqli->query($sql);
// 		ss_check_result($result, 'terms');

// 	// Копируем данные в таблицу term_taxonomy
// 		$sql = "TRUNCATE ".NEW_DB.".".PREFIX."term_taxonomy";
// 		$result = $mysqli->query($sql);
// 		ss_check_result($result);

// 		$sql = "INSERT INTO ".NEW_DB.".".PREFIX."term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, parent)
// 				SELECT id, id, 'category', descr, parentid
// 				FROM ".OLD_DB.".dle_category";

// 		$result = $mysqli->query($sql);
// 		ss_check_result($result);

// 		// Подкатегории Киева, делаем основными категориями
// 			$sql = "UPDATE ".NEW_DB.".".PREFIX."term_taxonomy
// 					SET parent = 0
// 					WHERE parent = 2";
// 			$result = $mysqli->query($sql);
// 			ss_check_result($result);

// 		// Создаем новую категорию Города
// 			// получаем наибольший id
// 				$sql = "SELECT MAX(term_id)
// 						FROM ".NEW_DB.".".PREFIX."term_taxonomy";
// 				$result = $mysqli->query($sql);
// 				ss_check_result($result);
// 				$maxId = $result->fetch_all()[0][0];

// 			// добавляем новую запись в term_taxonomy
// 				$sql = "INSERT INTO ".NEW_DB.".".PREFIX."term_taxonomy (term_taxonomy_id, term_id, taxonomy)
// 						VALUES (".($maxId+1).", ".($maxId+1).", 'category')";
// 				$result = $mysqli->query($sql);
// 				ss_check_result($result);

// 			// добавляем новую запись в terms
// 				$sql = "INSERT INTO ".NEW_DB.".".PREFIX."terms (term_id, name, slug)
// 						VALUES (".($maxId+1).", 'Города', 'cities')";
// 				$result = $mysqli->query($sql);
// 				ss_check_result($result);
		
// 		// Переносим Киев в города
// 		$sql = "UPDATE ".NEW_DB.".".PREFIX."term_taxonomy
// 				SET parent = ".($maxId+1)."
// 				WHERE term_id = 2";
// 		$result = $mysqli->query($sql);
// 		ss_check_result($result, 'term_taxonomy');	

// 	// Копируем данные в таблицу term_relationships
// 		$sql = "TRUNCATE ".NEW_DB.".".PREFIX."term_relationships";
// 		$result = $mysqli->query($sql);
// 		ss_check_result($result);

// 		$sql = "INSERT INTO ".NEW_DB.".".PREFIX."term_relationships ( object_id, term_taxonomy_id )
// 			VALUES ( ?, ? )";
// 		$stmt = $mysqli->prepare($sql);

// 		if ( !$stmt )
// 			echo "Can't prepared request: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;

// 		$post_id = '';
// 		$term_id = '';
		
// 		if ( !$stmt->bind_param( 'ii', $post_id, $term_id ) )
// 			echo "Can't bind params: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;

// 		$arrPostId = ss_createAssociativeArray($arrIdCategoriesOfPost, 0, 1);

// 		foreach ($arrPostId as $post_id => $catID)
// 		{
// 			$catID = explode(',', $catID);

// 				$catIDLength = count($catID);
// 				for ($i = 0; $i < $catIDLength; $i++)
// 				{ 
// 					$term_id = $catID[$i];
// 					if ( !$stmt->execute() )
// 						echo "\nCan't complete request: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "\n";
// 				}
			
// 				if ( empty($catID) )
// 				{
// 					$term_id = 0;

// 					if ( !$stmt->execute() )
// 					echo "Can't complete request: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "\n";
// 				}
// 		}

// 		ss_show_message( 'term_relationships' );

// // Копируем комментарии 
// 	$sql = "INSERT INTO ".NEW_DB.".".PREFIX."comments
// 		(
// 			comment_ID,
// 			comment_post_ID,
// 			comment_author,
// 			comment_author_email,
// 			comment_author_IP,
// 			comment_date,
// 			comment_date,
// 			comment_content,
// 			comment_karma,
// 			comment_approved,
// 			comment_parent,
// 			user_id
// 		)
// 		SELECT 
// 			id,
// 			post_id,
// 			autor,
// 			email,
// 			ip,
// 			date,
// 			date,
// 			text,
// 			rating,
// 			1,
// 			0,
// 			user_id
// 			FROM ".OLD_DB.".dle_comments";

// 	$result = $mysqli->query($sql);
// 	ss_check_result($result, 'users');

// // Копируем рейтинг в пост мета
// 	$sql = "INSERT INTO ".NEW_DB.".".PREFIX."postmeta
// 		(
// 			post_id,
// 			meta_key,
// 			meta_value
// 		)
// 		SELECT 
// 			news_id,
// 			'rating_total',
// 			rating
// 		FROM ".OLD_DB.".dle_post_extras";

// 	$result = $mysqli->query($sql);
// 	ss_check_result($result, 'users');


// Формируем режим работы заведений
	$sql = "SELECT post_id, meta_value FROM ".NEW_DB.".".PREFIX."postmeta WHERE meta_key = 'hours'";

	$result = $mysqli->query($sql);
	ss_check_result($result, 'users');
	$arrValues = $result->fetch_all();
	$arrTemp = ss_createAssociativeArray($arrValues, 0, 1);


	function _get_opening_hours_place( $arrValues )
    {
      $day = array('mon','tue','wed','thu','fri','sat','sun');
      $workTime = explode(', ', $arrValues);
      $dayTimeArr = [];

      if ( count($workTime) == 1 && empty($workTime[0][0]) )
        for ( $i = 0; $i < 7; $i++ )
        {
          $dayTimeArr[$day[$i]][0] = '00:00';
          $dayTimeArr[$day[$i]][1] = '00:00';
        }
      else
      {
        for ( $i=0; $i<count($workTime); $i++ )
        {
          $day = strstr($workTime[$i], ': ', true);
          $time = substr(strstr($workTime[$i], ': '), 2);
          
          $dayTimeArr[$day] =  explode('-', $time);

          if ( $dayTimeArr[$day][0] == 'Выходной' )
          {
            $dayTimeArr[$day][0] = '00:00';
            $dayTimeArr[$day][1] = '00:00';
          }
        }
      }

      return $dayTimeArr;
    }





echo '<pre>';
print_r(count($arrValues));
print_r($arrTemp);
print_r('<b>The database migration is complete!</b>');
echo '</pre>';

// // Добавляет количество постов к категориям
// UPDATE wp_term_taxonomy SET count = (
// SELECT COUNT(*) FROM wp_term_relationships rel
//     LEFT JOIN wp_posts po ON (po.ID = rel.object_id)
//     WHERE
//         rel.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
//         AND
//         wp_term_taxonomy.taxonomy NOT IN (‘link_category’)
//         AND
//         po.post_status IN (‘publish’, ‘future’)
// )
?>


