<?php
	get_header();
?>

<div class="container-fluid ">
	<div class="row padding_zero notfnd-maincont">
		<div class="col-sm-12">
			<div class="col-sm-12">
				<p class="col-sm-12 notfnd-hOne">Кажется, вы заблудились</p>
				<p class="notfnd-descrp">Сообщите нам, как Вы сюда попали, используя <a href="<?php get_template_directory_uri(); ?>/feedback">форму</a> обратной связи</p>
				<p class="notfnd-descrp">Попробуйте начать с главной <a href="<?php echo home_url();?>">gvult.com</a></p>
				<p class="notfnd-descrp">Или найти на сайте:</p>

				<form class="col-sm-12 col-xs-12 ntfnd-search">
					<input type="text" name="404search"  class="form-group col-sm-11 col-xs-9  ntfnd-searchform">
					<a href="#" class="btn-small-blue btn-loupe col-sm-1 col-xs-3"><img src="<?php echo get_template_directory_uri(); ?>/img/magnifying-glass-white.png" class="btn-loupe-icon"></a>
				</form>
			</div>

			<div class="col-sm-12 bigHeader">
				<p class="ntfnd-big-parag">самое</p>
			</div>


			<div class="col-sm-12 col-xs-12 card-block404">

			<?php
			$args = array
				(
					'posts_per_page'	=> 3,
					'offset'			=> 20,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);
				$count = 0;
				
				while ( $query->have_posts() )
				{
					$query->the_post();
					
					if ( $count != 3 )
					{
						echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink typeBlock_300x335">';
						echo getCardSimpleSingle($post);
						echo '</div>';
					}
			
					$count++;
				}
				
				wp_reset_postdata();?>
				

			</div>
		</div>	
	</div>
</div>

<?php 
	get_footer();
	
?>