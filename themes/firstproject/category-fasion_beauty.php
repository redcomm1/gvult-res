<?php 
	get_header();
?>

<div class="container-fluid category-desc-fluid">

<?php
	include 'php/slider-main.php';



	$args = array
	(
		'posts_per_page'	=> 3,
		'offset'			=> 6,
		'cat'				=> -611,
		'post_type'			=> array('post', 'places')
	);

	$query = new WP_Query($args);
?>
	<div class="row catdest-row">
		<div class="col-sm-8 cat-margintop">
			<?php 
			$count = 0;
			while ( $query->have_posts() )
			{
				$query->the_post();
										
				switch ( $count )
				{
					case 0:
						echo '<div class="col-sm-12">';
						echo getCardSimpleTriple($post);
						echo '</div>';
						break;
					case 1:
					case 2:
						echo '<div class="col-sm-6 blackoutPictureLink typeBlock_300x335 cat-margintop">';
						echo getCardSimpleSingle($post);
						echo '</div>';
						break;
					default:
						print_var('Что не так. Первый цикл новостей. Файл category.php.');
						break;
				}
				
				$count++;
			}

			wp_reset_postdata(); ?>
		</div>
		<div class="col-sm-4 catdest-newsblock">
			<div class="col-sm-12">
				<?php
					include 'php/slider-news.php';
				?>
			</div>
		</div>
		<div class="row catdest-row">
			<div class="col-sm-12 catdest-socialrow">
				<div class="col-sm-4 restCard1">
					<a href="#">
							<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages">
					</a>
				</div>
				<div class="col-sm-4 restCard1">
					<a href="#">
							<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages">
					</a>
				</div>
				<div class="col-sm-4 catdest-socialblock">
					<div class="fb-page" 
					  data-href="https://www.facebook.com/GVULT/"
					  data-width="282" 
					  data-hide-cover="false"
					  data-show-facepile="true" 
					  data-show-posts="false">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<?php
			$args = array
			(
				'posts_per_page'	=> 5,
				'offset'			=> 9,
				'cat'				=> -611,
				'post_type'			=> array('post', 'places')
			);

			$query = new WP_Query($args);

			$count = 0;
			while ( $query->have_posts() ){
				$query->the_post();
										
				switch ( $count )
				{
					case 0:
					case 4:
						echo '<div class="col-sm-8 cat-margintop">';
						echo getCardSimpleDouble($post);;
						echo '</div>';
						break;
					case 1:
						echo '<div class="col-sm-4 blackoutPictureLink typeBlock_300x335 padding_zero cat-margintop">';
						echo getCardSimpleSingle($post);;
						echo '</div>';
						break;
					case 2:
						echo '<div class="col-sm-12 blackoutPictureLink typeBlock_300x335 cat-margintop">';
						echo getCardSimpleTriple($post);;
						echo '</div>';
						break;
					case 3:
						echo '<div class="col-sm-4 blackoutPictureLink typeBlock_300x335 cat-margintop">';
						echo getCardSimpleSingle($post);;
						echo '</div>';
						break;	
					default:
						print_var('Что не так. Второй цикл новостей. Файл category.php.');
						break;
				}
				
				$count++;
			}

			wp_reset_postdata();?>
		</div>
		<div class="row catdest-row">
			<div class="col-sm-8 cat-margintop">
				<?php
				$args = array
				(
					'posts_per_page'	=> 3,
					'offset'			=> 14,
					'cat'				=> -611,
					'post_type'			=> array('post', 'places')
				);

				$query = new WP_Query($args);

				$count = 0;
				while ( $query->have_posts() ){
					$query->the_post();
											
					switch ( $count )
					{
						case 0:
						case 1:
							echo '<div class="col-sm-6 blackoutPictureLink typeBlock_300x335 cat-margintop">';
							echo getCardSimpleSingle($post);
							echo '</div>';
							break;
						case 2:
							echo '<div class="col-sm-12">';
							echo getCardSimpleSingle($post);
							echo '</div>';
							break;
						default:
							print_var('Что не так. Третий цикл новостей. Файл category.php.');
							break;
					}
					
					$count++;
				}

				wp_reset_postdata();?>
			</div>
			<div class="col-sm-4 catdest-newsblock">
				<div class="col-sm-12">
					<?php
						include 'slider-bestofweek.php';
					?>
				</div>
			</div>
			<div id="wr_btnMorePosts" class="col-sm-12 item-lastBtnBlk category-lastBtn">
				<?php include 'php/btn_load_more_posts.php';?>
			</div>
		</div>
	</div>
</div>

<?php 
	get_footer(); 
?>