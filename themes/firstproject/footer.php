<footer class="footer">
		<div class="container-fluid footerMainContainer">
			

			<div class="row">
				<div  class="col-xs-12 footer_phone_logo_block">
					<a href="#"><img class="footerLogo" src="<?php bloginfo('template_url'); ?>/img/Logo-white.png" alt="footer logo"></a>
				</div>
			</div>
			

			<div class="row">
				<div class="col-xs-3 footerBlocks footer_phone_lists">
					
					<p class="footerHeaders">Navigate</p>
						<nav class="footerNav">
							<ul>
								<li><a class="footerNavElements"  href="#">City guide</a></li>
								<li><a class="footerNavElements"  href="#">City news</a></li>
								<li><a class="footerNavElements"  href="#">Gastro life</a></li>
								<li><a class="footerNavElements"  href="#">Culture</a></li>
								<li><a class="footerNavElements"  href="#">Fasion</a></li>
								<li><a class="footerNavElements"  href="#">Добавить заведение</a></li>
								<li><a class="footerNavElements"  href="#">Добавить отзыв</a></li>
							</ul>
						</nav>

				</div>
				<div class="col-xs-3 footerBlocks footer_phone_lists">
						<p class="footerHeaders">Information</p>
						<nav class="footerNav">
							<ul>
								<li><a class="footerNavElements"  href="#">Контакты</a></li>
								<li><a class="footerNavElements"  href="#">Реклама</a></li>
								<li><a class="footerNavElements"  href="#">О нас</a></li>
								<li><a class="footerNavElements"  href="#">Вакансии</a></li>
								<li><a class="footerNavElements"  href="#">Соглашение</a></li>
								<li><a class="footerNavElements"  href="#">Правила общения</a></li>
								<li><a class="footerNavElements"  href="#">Карта сайта</a></li>
							</ul>
						</nav>

				</div>
				<div class="col-xs-6 footerBlocks footer_phone_social">
							
							<p class="footerHeaders">Social</p>
							
							<form>
							  <div class="form-group footerEmail-form">
							    <label class="exampleInputEmail1 footerEmail-label">Еженедельная рассылка</label>
							    <div>
								    <input type="email" class="form-control" id="footerInputEmail" placeholder="Электронная почта">
								    <a href="#" class="btn-small-blue footerButton">ПОДПИСАТЬСЯ</a>
							    </div>
							  </div>
							</form>
							
							<div class="footerSocials">
								<a href="#"><div class="footerSocialsIcons"><img src="<?php bloginfo('template_url'); ?>/img/icon-vk-copy-13.png" alt="..."></div></a>
								<a href="#"><div class="footerSocialsIcons"><img src="<?php bloginfo('template_url'); ?>/img/icon-fb-copy-13.png" alt="..."></div></a>
								<a href="#"><div class="footerSocialsIcons"><img src="<?php bloginfo('template_url'); ?>/img/icon-tw-copy-13.png" alt="..."></div></a>

							</div>
							
		
				</div>
				
			</div>
		
		
		<div class="row footerBlockRigths">
			<p>© 2016 Gvult. Все права защищены. На сайте не разрешается размещать контент, на который вы не имеете прав и/или согласия правообладателя. Просим ознакомиться с <a href="#" class="footer-agreement">Соглашением</a> и <a href="#" class="footer-agreement">Правилами общения.</a></p>
		</div>
		</div>
</footer>
<div class="modal_window">
		<div class="modal-window-wrapper">
			<ul class="pgwSlideshow">
				    <li>
				    	<img src="http://lorempixel.com/180/190" alt="Header Head erHead erHeade rHeaderH eaderHea der">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/280/290" alt="HeaderHeaderH eaderHea derHeader Header Header">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/380/390" alt="Header HeaderHead erHeaderH eaderHeaderH eader">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/480/490" alt="HeaderHe r HeaderH eaderHe aderHeader">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/580/590" alt="Header Header HeaderHe aderHea derHead erHeade rHeader">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/680/690" alt="Header  HeaderHeade rHeaderHe aderHead erHe aderHeader">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/780/790" alt="Head erHea derHea derHeade rHead erHeade rHeader Header">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/880/890" alt="Heade rHea derHeaderH eaderHead erHeade rHeaderHeader">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/980/590" alt="Head erHeade rHeaderH eaderHea derHead erHeader">
				    </li>
				    <li>
				    	<img src="http://lorempixel.com/880/590" alt="HeaderHead erHead erHeader HeaderHead erHeader Header">
				    </li>
	
				</ul>
			<div class="modal-window-count">
				<img src="http://lorempixel.com/22/13" alt="count">
				<span>4 / 16</span>
			</div>
			<span class="modal-close-icon"></span>
		</div>
</div>
<?php wp_footer(); ?>
<script type="text/javascript">
/* <![CDATA[ */
// js, если надо вставить в футер
/* ]]> */
</script>
<script>

/*

jQuery(document).ready(function($){

	var owl = $('.owl-carousel');
      	owl.owlCarousel({
      	items: 4,
        loop: true,
        autoplay:true,
        dots: false,
        pagination: false,
        autoplayTimeout:5000,
        slideSpeed: 2000,
        
    })
});



var $divModal = document.querySelector('.modal_window')
function showModal(event) 
{

   console.log(this)
    $divModal.style.display = 'block'
    document.body.style.overflow = 'hidden'
    document.body.style.paddingRight = '18px'

  //  $divModal.addEventListener('click', closeModal, false)
  	var pgwSlideshow = jQuery('.pgwSlideshow').pgwSlideshow();
  
    pgwSlideshow.reload(
    {
	    autoSlide: false,
	    transitionEffect: 'fading'
	});


    event.stopPropagation()
    event.preventDefault()
}

function closeModal(event)
{
	$divModal.style.display = 'none'
    document.body.style.overflow = ''
    document.body.style.paddingRight = ''
    document.body.removeEventListener('click', closeModal, false)
    // this.parentNode.removeChild(this)

    event.preventDefault()
}

//btnShowModal.addEventListener('click', showModal, false)
*/


</script>



<?php if (WORKING_VARIANT) :?>
	<!-- Вход через google -->
	<script>
		startApp();
	</script>

	<!-- Вход через facebook -->
	<script>
	jQuery('.soc-auth-link').click(function(e) {
		e.preventDefault();
		var width  = 575,
	        height = 400,
	        left   = (jQuery(window).width()  - width)  / 2,
	        top    = (jQuery(window).height() - height) / 2,
	        url    = this.href,
	        opts   = 'status=1' +
	                 ',width='  + width  +
	                 ',height=' + height +
	                 ',top='    + top    +
	                 ',left='   + left;
	    
	    window.open(url, 'Авторизация', opts);
	});
	</script>
<?php endif;?>

</body>
</html>