<div class="container-fluid">
	<div class="row wr_switcher">
		<div class="col-sm-offset-9 col-sm-3 catFiltMapSwitcherBlock">
			<p class="catFiltMapSwitcherPar">Списком</p>
			<div id="switcher_list_map" class="catFiltMapSwitcherImg turn_map" data-switch="map"><span></span></div>
			<p class="catFiltMapSwitcherPar">На карте</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">	
			<h1 class="addPlaceH1 h1fullmap">все места на карте</h1>
			<?php
				$countCat = count($MAIN_CATEGORY);
				$arrCatImgs = array(
					'3'		=> get_template_directory_uri() . '/img/group.svg',
					'4'		=> get_template_directory_uri() . '/img/group-32.png',
					'5'		=> get_template_directory_uri() . '/img/group-7.svg',
					'6'		=> get_template_directory_uri() . '/img/group-4.svg',
					'7'		=> get_template_directory_uri() . '/img/group-6.svg',
					'24'	=> get_template_directory_uri() . '/img/group-5.svg',
					'27'	=> get_template_directory_uri() . '/img/group-12.svg',
					'31'	=> get_template_directory_uri() . '/img/group-31.png',
					'36'	=> get_template_directory_uri() . '/img/group-33.png'
				);
			
				parse_str($_SERVER['QUERY_STRING'], $output);
			
				foreach ($output as $key => $value)
					if ( $key != 'category' )
						$getRequest[$key] = $value;
				
				$link = home_url('/category/city-guide/?').http_build_query($getRequest);

				for ( $i = 0; $i < $countCat; $i++ ) :
					$category = ss_get_category_info($MAIN_CATEGORY[$i]);
					$linkWithCat = $link.'&category='.$MAIN_CATEGORY[$i];
				?>
					<div class="col-sm-4 col-xs-6 fullMapCategory">
						<img src="<?php echo $arrCatImgs[$MAIN_CATEGORY[$i]];?>">
						<a href="<?php echo ((isset($_GET['category']) && !empty($_GET['category']) && $_GET['category'] == $MAIN_CATEGORY[$i]) ? '' : $linkWithCat); ?>" 
							data-id="<?php echo $MAIN_CATEGORY[$i]?>">
							<p class="fullmapCategoryNames <?php echo ((isset($_GET['category']) && !empty($_GET['category']) && $_GET['category'] == $MAIN_CATEGORY[$i]) ? 'active' : '') ?>"><?php echo $category['name']?>
							</p>
						</a>
					</div>
			<?php endfor;?>	
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div  class="col-sm-12 col-xs-12 padding_zero">
			<form class="form-group col-sm-10 col-xs-7 citguidemap-search-phone">
				<label class="addNewPlaceFont">Быстрый поиск</label>
				<input 
					id="field_search" 
					type="search" 
					class="form-control border_radius_zero fullmapSearch" 
					placeholder=""
					value="<?php echo isset($_GET['search']) ? $_GET['search'] : '';?>">
	  		</form>
	  		<div class="col-sm-2  col-xs-5 padding_zero">
				<a id="btn_fast_search" href="#" class="btn-small-blue fullmapSearchButt">ИСКАТЬ</a>
			</div>
			</div>
			<form class="col-sm-12 col-xs-12 form_checkbox">
				<div class=""> 
					<input 
						id="field_check_all_cat" 
						type="checkbox" 
						name="check" 
						value="check" 
						class="fullMapCheckbox" 
						<?php echo isset($_GET['allcat']) ? 'checked' : '';?>>
					<label for="field_check_all_cat">По всем категориям</label>
				</div>
			</form>
			<div class="col-sm-12 col-xs-12 fullmapParametrs-block">
				<?php $baseLink = home_url('/category/city-guide/?maps&orderby=date');?>
				<p class="fullmapParametrs"><a href="<?php echo $baseLink?>" title="Выбрать все">Выбрать всё</a></p>
				<p class="fullmapParametrs"><a id="btn_clean_all" href title="Очистить всё">Очистить всё</a></p>
				<p class="fullmapParametrs"><a href title="Подогнать масштаб">Подогнать масштаб</a></p>
			</div>
		</div>
	</div>
	<hr class="paragraph_line">
	<div class="row">
		<div class="col-sm-12 wr_sort_param">
		<?php $linkSort = getLinks();?>
			<a href="<?php echo (isset($_GET['orderby']) && $_GET['orderby'] == 'date') || !isset($_GET['orderby']) ? $linkSort['without_date'] : $linkSort['date'];?>" class="fullmapParametrs2 <?php echo (isset($_GET['orderby']) && $_GET['orderby'] == 'date') || !isset($_GET['orderby']) ? 'active' : '';?>">По дате</a>
			<a href="<?php echo isset($_GET['meta']) && $_GET['meta'] == 'rating_total' ? $linkSort['without_rating'] : $linkSort['rating'];?>" class="fullmapParametrs2 <?php echo isset($_GET['meta']) && $_GET['meta'] == 'rating_total' ? 'active' : '';?>">По рейтингу</a>
			<a href="<?php echo isset($_GET['meta']) && $_GET['meta'] == 'check_min' ? $linkSort['without_check'] : $linkSort['check'];?>" class="fullmapParametrs2 <?php echo isset($_GET['meta']) && $_GET['meta'] == 'check_min' ? 'active' : '';?>">По среднему чеку</a>
		</div>
	</div>	
	<div class="row">
		<div class="col-sm-12 city-guide-map-phone">
			<div id="map" class="city_map">
			</div>
		</div>
	</div>
	<?php 
		$temp = $wp_query;
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$wp_query = null;
		$posts_per_page = 15;
		$post_type = 'places';

		$args = array
			(
				'posts_per_page'	=> $posts_per_page,
				'post_type'			=> $post_type,
				'cat'				=> isset($_GET['category']) ? $_GET['category'] : $MAIN_CATEGORY,
				's' 				=> isset($_GET['search']) ? $_GET['search'] : '',
				'paged'             => $paged						
			);

		// Формируем дополнительный запрос
		if ( !empty($_GET) )
			$args = array_merge($args, create_query_sort_filter($_GET));

		$wp_query = new WP_Query($args);
	?>
	<div id="query_data" class="row" data-maxposts="<?php echo $wp_query->found_posts?>" data-query='<?php echo json_encode($args);?>'>
		<div class="col-sm-12 padding_zero">
			<div class="col-sm-8 padding_zero">
				<?php 
				$count = 0;
				while ( $wp_query ->have_posts() && $count < 9 )
				{
					$wp_query ->the_post();
					
					if($count == 8 && isMobile()){
						echo '<div class="col-sm-4 col-xs-6" style="display: none;">';
						echo getCardPlace($post);
						echo '</div>';
					}
					else{
						echo '<div class="col-sm-4 col-xs-6">';
						echo getCardPlace($post);
						echo '</div>';
					}
					

					$count++;
				}?>
			</div>
			<div class="col-sm-4 fullmap-banner-phone">
				<div class="big-banner">
					<a href="#">
						<img src="http://lorempixel.com/300/600" class="img-responsive">
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="restAddPlaceBanner col-sm-12 col-xs-12" style="margin-top: 11px;">
			<p class="restAddPlaceBannerHead">Добавьте своё заведение</p>
			<p class="restAddPlaceBannerPar">Если вы владелец магазина, ресторана или коворкинг площадки, добавьте свое заведение на «Гвалт».</p>
			<a href="<?php echo home_url().'/dobavit-zavedenie'?>" class="btn-standart-border restAddPlaceBannerBtn">ДОБАВИТЬ</a>
		</div>
	</div>
	<div class="row fullmap-invert-block">
		<div class=" col-sm-8 col-xs-12 padding_zero fullmap-newsslider-placeblock">
			<?php
			if ( $wp_query ->post_count > 9 )
				while ( $wp_query ->have_posts())
				{
					$wp_query ->the_post();
						
					echo '<div class="col-sm-4 col-xs-6">';
					echo getCardPlace($post);
					echo '</div>';
				}
			?>
		</div>
		<div class="col-sm-4  col-xs-12 fullmap-newsslider-block">
			<div id="myCarousel1" class="carousel slide newsFirstDiv carousel_gastro_life" data-ride="carousel">
				<div class=" news-block-buttons">
					<h4>Новости Gastro Life</h4>
					<div class="switcher-block-fullmap">	
						<a class="more-news-switchrs carousel-control left" href="#myCarousel1" role="button" data-slide="prev">
							<img src="<?php bloginfo('template_url'); ?>/img/right-arrow.png">
						</a>
						<a class=" more-news-switchrs carousel-control right" href="#myCarousel1" role="button" data-slide="next">
							<img src="<?php bloginfo('template_url'); ?>/img/left-arrow.png">
						</a>
					</div>
				</div>
				<div class="carousel-inner " role="listbox">
					<?php
					$args = array(
						'posts_per_page'	=> 6,
						'post_type'			=> array('post', 'review', 'places'),
						'cat'				=> '613, 3'
					);

					$gvPost = new WP_Query($args);

					if ( $gvPost->have_posts() ) :
						for($i = 0; $i < 2; $i++) :?> 
							<div class="item <?php if (!$i) echo 'active';?>">
							<?php
							for($j = 0; $j < 2; $j++) :
							
								$gvPost->the_post();
								?>
								<figure class="moreNewsblocks">
									<a href="<?php echo get_post_permalink($post);?>">
										<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive categoryImagesNews slider2-img">
										<figcaption><?php the_title(); ?></figcaption>
									</a>
								</figure>
							<?php endfor;?>
							</div>
							<?php
						endfor;
					endif;
					wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<?php 
			echo '<div class="col-sm-12 col-xs-12 fullmap-pagination-block">
				<div class="fullmap-pagination">';
			the_posts_pagination( 
				array(
				'mid_size' => 2,
				'show_all'     => false,
				'prev_next'    => true,
				'prev_text'    => '',
				'next_text'    => ''
				) 
			);
			echo '</div>
				</div>';
			$wp_query= $temp;
			wp_reset_postdata();
		?>
	</div>
</div>