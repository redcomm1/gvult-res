<div class="container-fluid">
<div class="col-sm-12">
		<div class="col-sm-12">
			<h1 class="cityGuideHeader">каталог лучших заведений киева</h1>
		</div>
	</div>
<div class="row">
	
<?php
	include ROOT_PATH.'/php/slider-main.php';
?>
</div>
<div class="col-sm-12">
	<div class="col-sm-12">
		<h1 class="cityGuideHeader"><?php echo ss_get_title_category( $catID );?></h1>
	</div>
</div>
	<div class="row">
		<div class="col-sm-12 cat-places-paramer1">
			<div class="col-sm-12">
				<div class="col-sm-4">
					<p class="catfilter-top5-par"><?php echo ss_get_top_title_category( $catID );?></p>
				</div>
				<?php $links = getLinks();	?>
				<div class="col-sm-5 padding_zero">
					<div class="col-sm-5 margin_zero padding_zero">
						<a href="<?php echo isset($_GET['orderby']) && $_GET['orderby'] == 'date' ? $links['without_date'] : $links['date'];?>" class="categoryFilterName catFilterSorting <?php echo isset($_GET['orderby']) && $_GET['orderby'] == 'date' ? 'active' : '';?>">По дате добавления</a>
					</div>
					<div class="col-sm-4 margin_zero padding_zero">
						<a href="<?php echo isset($_GET['meta']) && $_GET['meta'] == 'rating_total' ? $links['without_rating'] : $links['rating'];?>" class="categoryFilterName catFilterSorting <?php echo isset($_GET['meta']) && $_GET['meta'] == 'rating_total' ? 'active' : '';?>">По рейтингу</a>
					</div>
					<div class="col-sm-3 margin_zero padding_zero">
						<a href="<?php echo isset($_GET['meta']) && $_GET['meta'] == 'check_min' ? $links['without_check'] : $links['check'];?>" class="categoryFilterName catFilterSorting <?php echo isset($_GET['meta']) && $_GET['meta'] == 'check_min' ? 'active' : '';?>">По цене</a>
					</div>
				</div>
				<div class="col-sm-3 catFiltMapSwitcherBlock">
					<p class="catFiltMapSwitcherPar">Списком</p>
					<div id="switcher_list_map" class="catFiltMapSwitcherImg" data-switch="list"><span></span></div>
					<p class="catFiltMapSwitcherPar">На карте</p>
					
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-12">
				<div class="col-sm-8 padding_zero">
					<?php
						$args = array
							(
								'posts_per_page'	=> 14,
								'post_type'			=> 'places',
								'cat'				=> $catID
							);

						// if ( isset($argsGet) )
						//   	$args = array_merge($args, $argsGet);

						// Формируем дополнительный запрос
						if ( !empty($_GET) )
							$args = array_merge($args, create_query_sort_filter($_GET));

						$query = new WP_Query($args);

						$countPosts = 0;
						while ( $query->have_posts() && $countPosts < 5 )
						{
							$query->the_post();
														
							?><div class="col-sm-4"><?php 
							echo getCardPlace($post);
							?></div><?php
							$countPosts++;
						}
					?>

							<div class="col-sm-4 ">
								<div class="container_add_place">
									<p>Нет вашего заведения?</p>
									<img src="<?php echo get_template_directory_uri().'/img/add_new_one_icon.png'?>" alt="Добавить свое заведение">
									<a id="btn_add_place" href="<?php echo home_url().'/dobavit-zavedenie'?>" class="btn-small-blue" title="Добавить свое заведение">ДОБАВИТЬ</a>
								</div>
							</div>

						
						<div class="col-sm-12 <?php echo $query->found_posts <= 5 ? 'hide' : ''?>">
							<div class="restParagrafHead col-sm-12">
							<p class="paragraf catPlacesHeader2"><?php echo ss_get_all_title_category( $catID );?></p>
						</div>
						</div>
							<?php   
								$count = 0;
								if ( $query->have_posts() )
								while ( $query->have_posts() && $query->found_posts > 5)
								{
									$query->the_post();
																				
									?><div class="col-sm-4"><?php 
									echo getCardPlace($post);
									?></div><?php
								
									$count++;
								}
							
								wp_reset_postdata();?>

							<div id="wr_btnMorePlaces" class="col-sm-12 margin_zero padding_zero categoryFilterViewMoreBtn <?php echo $query->found_posts <= 15 ? 'hide' : ''?>">
								<a id="btnMorePlaces" 
									href="#" 
									class="cityGuideMoreViewBtn" 
									data-maxcountposts="0" 
									data-offset="14" 
									data-posttype="<?php echo 'places';?>"
									data-countposts="9"
									data-search="<?php echo empty($_GET) ? '' : 'true';?>"
									data-type='place'>
									<span class="MoreViewBtnPar">ПОКАЗАТЬ ЕЩЕ</span>
									<span class="MoreViewBtPoint"></span>
									<span class="MoreViewBtPoint"></span>
									<span class="MoreViewBtPoint"></span>
								</a>
							</div>
						<div class="col-sm-12">
							<div class="row categoryPlacesBanners">
								<div class="col-sm-6 restCard1">
									<a href="#">
											<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages">
									</a>
								</div>
								<div class="col-sm-6 restCard1">
									<a href="#">
											<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages">
									</a>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="restParagrafHead col-sm-12">
								<p class="paragraf catPlacesHeader2"><?php echo ss_get_comment_title_category( $catID );?></p>
							</div>
						</div>
						<div class="col-sm-12">
							<?php echo ss_getComments(8, 0, '', 'col-sm-6 category-commentBlocks');?>
								<div id="wr_btnMoreComments" class="col-sm-12 cityGuideMoreViewBtnBlock">
								<a id="btnMoreComments" href="#" 
									class="cityGuideMoreViewBtn"
									data-maxcountcomments="<?php echo ss_get_category_info($catID)['count_comments']?>"
									data-offset="8"
									data-countcomments="8"
									data-catid="<?php echo $catID;?>"
									data-classes="col-sm-6 category-commentBlocks">
									<span class="MoreViewBtnPar">ПОКАЗАТЬ ЕЩЕ</span>
									<span  class="MoreViewBtPoint"></span>
									<span class="MoreViewBtPoint"></span>
									<span class="MoreViewBtPoint"></span>
								</a>
							</div>
						</div>
					</div>
				<div class="col-sm-4">
					<div id="filter_block" class="col-sm-12 categoryFilterBlock">
						<div class="col-sm-12">
							<p class="categoryFilterBlockHead">Категории заведений</p>
						</div>
						<div class="col-sm-6">
							<?php echo get_list_main_category('categoryFilterName');?>
						</div>
						<div class="col-sm-6 categoryFilterValueBlock">
							<?php echo get_list_count_post_main_category( 'categoryFilterValue' );?>
						</div>
						<p id="btn_additional_sort" class="col-sm-12 btn_additional_sort" <?php if (check_add_filters()) echo 'style="display: none;"'; ?>>Дополтинельно</p>
						
						<form id="form_additional_sort" class="col-sm-12 form_additional_sort" <?php if (!check_add_filters()) echo 'style="display: none;"'; ?>>
							<div>
								<label>
								    <input id="checkbox_place_open" class="checkbox" type="checkbox" name="checkbox-test" data-type="openig" <?php echo ((isset($_GET['openig']) && $_GET['openig'] == 'true') ? 'checked' : '');?>>
								    <span class="checkbox-custom"></span>
								    <span class="fullsch-ckb-sp1">Заведение открыто</span>
								</label>
							</div>
							<div>
								<label>
								    <input class="checkbox1" type="checkbox" name="checkbox-test">
								    <span class="checkbox-custom1"></span>
								    <span class="fullsch-ckb-sp2">Наличие 3D-тура</span>
								</label>
							</div>
							<div class="wr_sub_category">
								<span></span>
								<input id="sub_category" type="text" class="form-control categoryFilterForms  input_list" 
									placeholder="<?php echo (isset($_GET['subcat']) ? get_cat_name($_GET['subcat']) : 'Подкатегории');?>" readonly>
								<ul class="list">
									<?php 
										$categories = ss_get_sub_categories( $catID );

										for ( $i = 0; $i < count($categories);  $i++ )
										{ 
											echo '<li data-value="'.$categories[$i]['name'].'" data-id="'.$categories[$i]['id'].'" data-type="subcat">'.$categories[$i]['name'].'</li>';
										}
									?>
								</ul>
							</div>
							<div class="wr_sub_category">
								<input id="subway" class="form-control categoryFilterForms input_list" type="text" readonly placeholder="<?php echo (isset($_GET['subway']) ? $_GET['subway'] : 'Станция метро');?>">
							    <span></span>
								<ul class="list">
									<?php 
										$subways = get_subway_names();
										for ( $i = 0; $i < count($subways);  $i++ )
										{ 
											echo '<li data-value="'.$subways[$i].'" data-type="subway">'.$subways[$i].'</li>';
										}
									?>
								</ul>
							</div>
							<div class="wr_sub_category">
								<input id="input_district" class="form-control categoryFilterForms input_list" type="text" readonly placeholder="<?php echo (isset($_GET['district']) ? $_GET['district'] : 'Район города');?>">
							    <span></span>
								<ul class="list">
									<?php 
										$districts = get_districts_name();
										for ( $i = 0; $i < count($districts);  $i++ )
										{ 
											echo '<li data-value="'.$districts[$i].'" data-type="district">'.$districts[$i].'</li>';
										}
									?>
								</ul>
							</div>
							<div class="fillYourStreetInput categoryFilterForms">
								<p>Укажите улицу</p>
								<span class="wall_hide"></span>
								<select id="select-address" class="form-control categoryFilterForms field_street"></select>
								<!-- <input type="text" class="form-control categoryFilterForms field_street"> -->
							</div>
						</form>
					</div>
					<div class="col-sm-12" style="z-index: 0;">
							<a href="<?php echo home_url().'/?s&add'?>" class="cityGuideFullSearchPar">Расширенный поиск</a>
					</div>
					<div class="col-sm-12" style="z-index: 0;">
						<a href="#" class="cat-place-img-banner"><img src="http://lorempixel.com/300/600" class="img-responsive "></a>
					</div>
					
					<div class="col-sm-12 padding_zero">
					<?php
						include ROOT_PATH.'/php/slider-news.php';

					?>
					</div>
				</div>
			</div>
		</div>
	
	
		<div class="row contentDiv7">	
			

			<?php
			$categoryID = get_query_var('cat');
			$args = array
				(
					'posts_per_page'	=> 5,
					'offset'			=> 50,
					'post_type'			=> array('post', 'places'),
					// 'cat'				=> $categoryID
				);

			$query = new WP_Query($args);
			
			$count = 0;
			while ( $query->have_posts() )
			{
				$query->the_post();
									
				if ( $count != 3 )
				{
					echo '<div class="col-xs-4 blackoutPictureLink typeBlock_300x335">';
					echo getCardSimpleSingle($post);
					echo '</div>';
				}
				else
				{
					echo '<div class="col-xs-8 blackoutPictureLink typeBlock_300x335">';
					echo getCardSimpleDouble($post);
					echo '</div>';
				}
				
				$count++;
			}
			
			wp_reset_postdata();?>
			<div id="wr_btnMorePosts" class="col-xs-12 item-lastBtnBlk">
				<?php include ROOT_PATH.'/php/btn_load_more_posts.php';?>
			</div>		
			
		</div>


	</div>
</div>
