<?php 
/*            Популярные функции Gvult
*/
$MAIN_CATEGORY = array(3,5,6,7,24,31,4,36,27);
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/gvult');

function print_var($message)
{
  echo '<pre>';
  print_r($message);
  echo '</pre>';
}

// Функция возвращает текущее время в формате hh:mm
function get_current_time()
{
  date_default_timezone_set('Europe/Kiev');
  return date('H:i');
}

// Функция возвращает текущий день недели время в амер. формате
function get_current_day()
{
  $day = array('', 'mon','tue','wed','thu','fri','sat','sun');
  return $day[date('N')];
}


// Ф-ция возвращает массив id и название главной категории
  function ss_get_parent_categories()
  {
    global $wpdb;
    $t1 = $wpdb->prefix . 'terms';
    $t2 = $wpdb->prefix . 'term_taxonomy';
    $result = $wpdb->get_results( 
          "SELECT A.term_id, A.name
            FROM $t1 A, $t2 B
            WHERE A.term_id = B.term_id
            AND B.term_id < 40
            AND A.name != 'Киев' AND A.name != 'Новости'
            ORDER BY A.name;" );

    return $result;
  }

/*
// Ф-ция возвращает информацию о категории:
//  - Название категории
//  - Количество постов категории
//  - Суммарное количество комментариев постов прикрепленных к этой категории
*/ 
  function ss_get_category_info($id_cat)
  {
    $arr = [];

    $category = get_category($id_cat);
    $arr['name'] = $category->name;
    $arr['link'] = get_category_link($id_cat);
    $arr['count_posts'] =  $category->count;
    
    global $wpdb;

    $query = "SELECT SUM(p.comment_count) AS count, t.name FROM gv_posts p
          JOIN gv_term_relationships tr ON tr.object_id = p.ID
          JOIN gv_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
          JOIN gv_terms t ON t.term_id = tt.term_id
          WHERE t.term_id in ($id_cat)
          AND p.post_status = 'publish'
          GROUP BY t.term_id";

    $categories = $wpdb->get_results($query);

    $arr['count_comments'] = $categories[0]->count;

    wp_reset_postdata();

    return $arr;
  }

// Ф-ция возвращает массив id переданной категории и ее подкатегорий
// можно указывать тип постов
  function  get_subcategories_of_category($id_cat, $type = 'places')
  {
    $args = array
    (
      'taxonomy'  => 'category',
      'type'  => $type,
      'parent' => $id_cat,
      'fields'  => 'ids'
    );
    
    $categories_id = get_terms($args);

    if ( empty($categories_id) )
      return array( $id_cat );

    array_push($categories_id, $id_cat);
    return $categories_id;
  }

// Ф-ция возвращает количесвто постов категории. Учитываются и подкатегории
  function  get_count_posts_of_category($id_cat)
  {
    $categories_id = implode(',',get_subcategories_of_category($id_cat));
    global $wpdb;

    $query = "SELECT gv_posts.*
        FROM gv_posts 
        LEFT JOIN gv_term_relationships ON (gv_posts.ID = gv_term_relationships.object_id) WHERE 1=1  AND 
        ( gv_term_relationships.term_taxonomy_id IN ($categories_id) ) AND 
        gv_posts.post_type = 'places' AND 
        gv_posts.post_status = 'publish' 
        GROUP BY gv_posts.ID ORDER BY gv_posts.post_date DESC";

    $posts = $wpdb->get_results($query);
    $countPosts = count($posts);


    return $countPosts;
  }  

//Ф-ция возвращает массив с id постов принадлежащие категории, если id
// категории не указать вернет все id постов
  function getPostsIdOfCategory( $categoryId = '' )
  {
    global $wpdb;

   if ( empty($categoryId) )
      $query = "SELECT object_id FROM gv_term_relationships";
    else    
     $query = "SELECT object_id FROM gv_term_relationships
          WHERE term_taxonomy_id = ".$categoryId;

     $result = $wpdb->get_results($query);

     $resultLength = count($result);
      for ( $i = 0; $i < $resultLength; $i++ )
      {
        $postsId[$i] = $result[$i]->object_id;
      }

     if ( empty($category_Id) )
        $postsId = array_values(array_unique($postsId));

     return $postsId;
  }

// Функция вывода комментария
  function ss_getComment($comment)
  {
    $title = get_the_title( $comment->comment_post_ID );

     $content = '<div class="cityGuidesRewiewBlocks padding_zero">
         <a href="'.get_comments_link($comment->comment_post_ID).'" class="">
           <div class="ctguide-lastrew-cont">
            <p class="rewiewTimeParag">'. get_comment_date( "j F Y в H:i", $comment->comment_ID ).'</p>
            <p class="reviewHeadParag">'.( (strlen($title) < 55) 
                                            ? strip_tags($title)
                                            : strip_tags(mb_substr( $title, 0, 55 )).'...').'</p>
            <p class="secondNewsParagrafs">';
        
              $strLeght = strlen($comment->comment_content);

              if($strLeght <= 60){
               $content = $content . strip_tags($comment->comment_content);
              }
              else {
                $content = $content . strip_tags(mb_substr($comment->comment_content, 0, 60)) . '...';
              };
              $content .= '</p>
         </div>
        </a>
      </div>';
    
    return $content;
  }

  function ss_getComments($commentCount = 1, $offset = 0, $categoryID = '', $className = '')
  {
    if( empty($categoryID) )
    {
      $categoryID = get_query_var('cat');

    }

    $args = array(
      'number'      => $commentCount,
      'offset'      => $offset,
      'orderby'     => 'comment_date',
      'order'       => 'DESC',
      'type'        => '',
      'post__in'     => getPostsIdOfCategory($categoryID)
    );    

    $comments = get_comments( $args );
    $content = '';
    foreach( $comments as $comment )
    {
      $content .= '<div class="'. $className .'">';
      $content .= ss_getComment($comment);
      $content .= '</div>';
    }

    return $content;
  }

//Функция склонения подежа для количества комментариев на карточке поста
  function plural_form($number, $after)
  {
    $cases = array (2, 0, 1, 1, 1, 2);
    return $number.' '.$after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
  }

//Функция вывода рейтинга на карточке поста
  function ss_getPlaceRating($post)
  {
    $args = array(
    'post_id' => $post->ID,
    'count' => true
    );
    $comments = get_comments($args);

    $content = '<p class="restRatinP1">'.plural_form($comments, array('ОТЗЫВ','ОТЗЫВА','ОТЗЫВОВ')).'</p>';
                       
    return $content;        
  }

// Функции по работе с режимом работы заведения
  // Ф-ция проверяет открыто или закрыто заведение
    function check_opening_place($postID)
    {
      $currentDay = get_current_day();
      $currentTime = get_current_time();
      $from1  = get_post_meta($postID, $currentDay.'-from_1', true);
      $to1    = get_post_meta($postID, $currentDay.'-to_1', true);
      $from2  = get_post_meta($postID, $currentDay.'-from_2', true);
      $to2    = get_post_meta($postID, $currentDay.'-to_2', true);

      if  ( 
            (
              $from1 < $currentTime && $currentTime < $to1
            )
            ||
            (
              $from2 < $currentTime && $currentTime < $to2 
            )
          )
        return true;

      
      return false;
    }


  function get_working_time_place($post){
    $dayArr = array(
      'mon'  =>  'Пн',
      'tue'  =>  'Вт',
      'wed'  =>  'Ср',
      'thu'  =>  'Чт',
      'fri'  =>  'Пт',
      'sat'  =>  'Сб',
      'sun'  =>  'Вс');

    $currentDay = get_current_day();
    $content = '';
     foreach ($dayArr as $dayEng => $dayRus) {
        $from1  = get_post_meta($post->ID, $dayEng.'-from_1', true);
        $to1    = get_post_meta($post->ID, $dayEng.'-to_1', true);
        $from2  = get_post_meta($post->ID, $dayEng.'-from_2', true);
        $to2    = get_post_meta($post->ID, $dayEng.'-to_2', true);
          if($currentDay == $dayEng){
            $content .= '<tr class="current-day">';
          }
          else{
             $content .= '<tr class="restTable_properties_font">';
          }
            $content .= '<td class="restDayWork">'. $dayRus . '</td>';
            
         if($from1 == '00:00' && $to1 == '00:00' && $from2 == '00:00' && $to2 == '00:00'){
           $content .= '<td>выходной</td>';
         }
         else{
          $content .= '<td>' .$from2 . ' - ';
              if($to2 == '24:00' && $to1 != '00:00'){
                $content .= $to1 . '</td>';
              }
              else{
                $content .= $to2 . '</td> ';
              }  
         $content .= '</tr>';
         }

        }
    
    return $content;
  }


          

// Функция выводит время работы заведения в разделе single-content
  function getPlaceWorkingTime($post){
    date_default_timezone_set('Europe/Kiev');
      $workTime = explode(', ', get_post_meta($post->ID, 'hours', true));
      $dayTimeArr = [];
     
      $dayArr = array('Пн','Вт','Ср','Чт','Пт','Сб','Вс');
      $currentDayStr = $dayArr[date('N')-1];

      for ($i=0; $i<count($workTime); $i++){

        $day = strstr($workTime[$i], ': ', true);
        $time = substr(strstr($workTime[$i], ': '), 2);
        
        $dayTimeArr[$day] =  $time;
        
        }

      $content = '';
      foreach ($dayTimeArr as $day => $time) {
       $content .=  '<tr>
               <td class="restDayWork';
               if($currentDayStr === $day){
                $content .= ' current-day">'.$day.'</td>
                <td class="current-day">'.$time.'</td>
              </tr>';;
                
               }
               else{
                $content .=  ' restTable_properties_font">'.$day.'</td>
                <td class="restTable_properties_font">'.$time.'</td>
              </tr>';
               }  
          }
   return $content;
  }



//Функция вывода услуг заведения в карточке ресторанов
  function getPlaceProrerty($post){
    $restProperties = array(
      'wifi'              =>'Wi-Fi', 
      'visa'              =>'Пластиковые карты', 
      'businesslunch'     =>'Бизнес ланч', 
      'livemusic'         =>'Живая музыка', 
      'hookah'            =>'Кальян', 
      'bigscreen'         =>'Большой экран',
      'dancefloor'        =>'Танцпол',
      'winelist'          =>'Винная карта',
      'billiards'         =>'Бильярд',
      'karaoke'           =>'Караоке',
      'summerterrace'     =>'Летняя терраса',
      'parking'           =>'Парковка',
      'aroundtheclock'    =>'Круглосуточно',
      'sauna'             =>'Баня/Сауна',
      'dj'                =>'Диджей',
      'darts'             =>'Дартс',
      'cigars'            =>'Сигары',
      'riding'            =>'Катание на лошадях',
      'beach'             =>'Пляж',
      'nursery'           =>'Детская комната',
      'sushi'             =>'Суши',
      'hotel'             =>'Отель',
      'fishing'           =>'Рыбалка',
      'pool'              =>'Бассейн',
      'bowling'           =>'Боулинг',
      'boating'           =>'Катание на лодке',
      'games'             =>'астольные игры',
      'stripping'         =>'Стриптиз'
      );

        $metaRestPropert = get_post_meta($post->ID);
        $content = '';
        foreach ($restProperties as $keyProper => $valueProper) {
          
          foreach ($metaRestPropert as $keyMeta => $valueMeta) {
            if($keyProper == $keyMeta && $valueMeta[0] == 'yes'){

              $content .= '<div>  
              <p class="rest_properties_font restCharacters">'.$valueProper.'</p></div>';
              
            }
          }
        }
              
              
  return $content;
  }

//Функция подсчета количества просмотров поста
  function ss_getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key,  true);

    if( empty($count) ){
          $count = 0;
          add_post_meta($postID, $count_key, '0');
    }
    else{
          $count++;
          update_post_meta($postID, $count_key, $count);
    }

    return $count;
  }

// Возвращает id и name подкатегорий категории переданной в параметре
  function ss_get_sub_categories( $id /*number*/, $name = '')
  {
    $args = array
    (
      'parent'    => $id,
      'taxonomy'  => 'category',
      'orderby'   => 'name'
    );
    $categories = get_terms( $args );

    $count = 0;
    foreach ($categories as $value)
    {
      $subCategoriesID[$count] = $value->term_id;
      $subCategoriesName[$count] = $value->name;
      $subCategories[$count]['id'] = $value->term_id;
      $subCategories[$count]['name'] = $value->name;
      $count++;
    }

    if ( $name == 'id' )
      return $subCategoriesID;
    if ( $name == 'name' )
      return $subCategoriesName;

    return $subCategories;
  }

// Ф-ция возвращает адрес заведения
 function getAddress( $post )
 {
    $placeAddress = get_post_meta($post->ID, 'address', true);
    
    if ( empty($placeAddress) )
    {
      $placeAddress = get_post_meta($post->ID, 'address_prefix', true) .' '. get_post_meta($post->ID, 'address_name', true) . ', '. get_post_meta($post->ID, 'address_number', true);
    }
    
    return $placeAddress;
 }

// Ф-ция возвращает адрес, станцию метро, район заведения,
// в html тегах
  function getThePlaceAdress($post){
    $placeAdress = '<p class="rest_properties_font restAddresPar1">';
    $placeAdress .= getAddress( $post );
    $placeAdress .= '</p>';

    $placeSub = '<p class="rest_properties_font restAddresPar1">';
    if(get_post_meta(get_the_ID(), 'metro', TRUE) != ''){
      $placeSub .= 'Метро ' . get_post_meta(get_the_ID(), 'metro', TRUE) . '</p>';
    }
    else{
      $placeSub .=  'Метро ' . get_post_meta(get_the_ID(), 'subway', TRUE) . '</p>';
    }


    $placeDistr = '<p class="rest_properties_font restAddresPar1">';
    if((get_post_meta(get_the_ID(), 'rayon' , TRUE) != '')){
      $placeDistr .= get_post_meta(get_the_ID(), 'rayon' , TRUE) . ' район</p>';
    }
    else{
      $placeDistr .= get_post_meta(get_the_ID(), 'district' , TRUE) . ' район</p>';
    }
    $placeDestination = $placeAdress . $placeSub . $placeDistr;
    return $placeDestination;
  }

// Функция добавляюет в массив c ключем 'meta_query' другой массив,
// тем самым формируя запрос meta_query     
  function add_in_meta_query($arrMeta /*массив массивов*/, $metaQuery /*подготовленный массив с ключем "meta_query"*/)
  {
    return isset( $metaQuery['meta_query'] ) 
          ? array_merge( $metaQuery['meta_query'], $arrMeta )
          : $metaQuery['meta_query'] = $arrMeta;
  }  

// Функция возвращает запрос к базе данных в виде массива
// На входе параметр в виде массива или строки(GET-запрос)
  function create_query_sort_filter( $GET /*массив или строка*/ )
  {
    $argsTemp = $GET;
    $argsGet = [];

    if ( is_string($GET) )
      parse_str($GET, $argsTemp);
     
    if ( isset($argsTemp['subcat']) || isset($argsTemp['cat']))
      $argsGet['cat'] = empty($argsTemp['subcat']) ? $argsTemp['cat'] : $argsTemp['subcat'];

    if ( isset($argsTemp['orderby']) )
    {
      $argsGet['orderby'] = $argsTemp['orderby'];
      $argsGet['order'] = 'DESC';
    }

    if ( isset($argsTemp['meta']) )
    {
      $argsGet['meta_key'] = $argsTemp['meta'];
    }

    if ( isset( $argsTemp['openig'] ) && $argsTemp['openig'] == 'true')
    {
      $currentTime = get_current_time();
      $currentDay = get_current_day();

      if ( $currentTime < '08:00' )
        $argsMeta['openig'] = array
        ( 
          array
          (
            'relation'  => 'AND',
            array
            (
              'key'   => $currentDay.'-from_1',
              'value'   => $currentTime,
              'compare' => '<'
            ),
            array
            (
              'key'   => $currentDay.'-to_1',
              'value'   => $currentTime,
              'compare' => '>'
            ) 
          )
        );
      else
        $argsMeta['openig'] = array
        ( 
          array
          (
            'relation'  => 'AND',
            array
            (
              'key'   => $currentDay.'-from_2',
              'value'   => $currentTime,
              'compare' => '<'
            ),
            array
            (
              'key'   => $currentDay.'-to_2',
              'value'   => $currentTime,
              'compare' => '>'
            ) 
          )
        );

      $argsGet['meta_query'] = add_in_meta_query($argsMeta['openig'], $argsGet);
    }

    if ( isset($argsTemp['district']) && !empty($argsTemp['district']) )
    {
      $argsMeta['district'] = array
      (
        array
        (
          'relation' => 'OR',
          array
          (
            'key' => 'district',
            'value' => $argsTemp['district']
          ),
          array
          (
            'key' => 'rayon',
            'value' => $argsTemp['district']
          )
        )
      );

      $argsGet['meta_query'] = add_in_meta_query($argsMeta['district'], $argsGet);
    }
    
    if ( isset($argsTemp['subway']) && !empty($argsTemp['subway']) )
    {
      $argsMeta['subway'] = array
      ( 
        array
        (
          'relation'  => 'OR',
          array
          (
            'key' => 'subway',
            'value' => $argsTemp['subway']
          ),
          array
          (
            'key' => 'metro',
            'value' => $argsTemp['subway']
          )
        )
      );

      $argsGet['meta_query'] = add_in_meta_query($argsMeta['subway'], $argsGet);
    }

    if ( isset($argsTemp['address']) && !empty($argsTemp['address']) )
    {
      $argsMeta['address'] = array
      ( 
        array
        (
          'relation'  => 'OR',
          array
          (
            'key'     => 'address',
            'value'   => $argsTemp['address'],
            'compare' => 'LIKE'
          ),
          array
          (
            'key' => 'address_name',
            'value' => $argsTemp['address'],
            'compare' => 'LIKE'
          )
        )
      );

      $argsGet['meta_query'] = add_in_meta_query($argsMeta['address'], $argsGet);
    }

    if ( isset($argsTemp['check']) && !empty($argsTemp['check']) )
    {
      $argsMeta['check'] = array
      ( 
        array
        (
          'key' => 'check_min',
          'value' => $argsTemp['check']
        )
      );

      $argsGet['meta_query'] = add_in_meta_query($argsMeta['check'], $argsGet);
    }

    if ( !isset($argsGet) )
      return 0;

    return $argsGet;
  }


// Функциb возвраща.т html код карточек
// Используется в кнопках LoadMore
  function _get_content_card_post($posts, $maxPosts/*макс. число постов*/)
  {
    foreach ($posts as $index => $post)
    {
      if ( $index != 3 )
      {
        $content[$index] = '<div class="col-xs-12 col-sm-4 blackoutPictureLink typeBlock_300x335">';
        $content[$index] .= getCardSimpleSingle($post);
        $content[$index] .= '</div>';
      }
      else
      {
        $content[$index] = '<div class="col-xs-12 col-sm-8 blackoutPictureLink typeBlock_300x335">';
        $content[$index] .= getCardSimpleDouble($post);
        $content[$index] .= '</div>';
      }

      if ( $index+1 == $maxPosts ) break;
    }

    return $content;
  }

  function _get_content_card_place($posts, $maxPosts/*макс. число постов*/)
  {
    foreach ($posts as $index => $post)
    {
        $content[$index] = '<div class="col-sm-4 col-xs-6">';
        $content[$index] .= getCardPlace($post);
        $content[$index] .= '</div>';

      if ( $index+1 == $maxPosts ) break;
    }

    return $content;
  }

// функция возвращает список категорий и количество постов в каждой категории
// используется в category-city-guide.php и category.php
function get_list_main_category( $classes /*строка*/, $page = '' /*string*/, $isMap = false )
{
  global $MAIN_CATEGORY;
  $arrIDCat = $MAIN_CATEGORY;
  $countArrIDCat = count($arrIDCat);
  $content = '';

  for ( $i = 0; $i < $countArrIDCat; $i++  )
  {
    $category = ss_get_category_info($arrIDCat[$i]);
    $content .= '<a '.($page != 'guide' ? ( $isMap ? 'href="'.$category['link'].'?map"' : 'href="'.$category['link'].'"' ) : '' ).' class="cg-top5-block '.(($page == 'guide' && !$i) ? 'active' : '' ).'" '.($page == 'guide' ? ' data-id="'.$arrIDCat[$i].'" data-page="cityguide"' : '' ).'>';
    $content .= '<span class="'.$classes.' '. ((get_query_var('cat') == $arrIDCat[$i]) ? 'active' : '').(($page == 'guide' && !$i) ? ' active' : '').'">'.$category['name'].'</span>';
    $content .= '</a>';
  }
  
  return $content;  
}

// функция возвращает список количества постов в каждой категории
// используется в category-city-guide.php и category.php
function get_list_count_post_main_category( $classes /*строка*/ )
{
  global $MAIN_CATEGORY;
  $arrIDCat = $MAIN_CATEGORY;
  $countArrIDCat = count($arrIDCat);
  $content = '';

  for ( $i = 0; $i < $countArrIDCat; $i++  )
  {
    $countPosts = get_count_posts_of_category($arrIDCat[$i]);
    $content .= '<a class="cg-top5-block">';
    $content .= '<span class="'.$classes.'">'.$countPosts.'</span>';
    $content .= '</a>';
  }
  
  return $content;  
}

// функция возвращает топ 5 карточек заведений для страницы city-guide.php
function get_top_five_cards_places( $cat /*number*/ )
{
  $args = array
  (
    'posts_per_page'  => 5,
    'post_type'       => 'places',
    'orderby'         => 'meta_value_num',
    'meta_key'        => 'rating_total',
    'cat'             => $cat
  );

  $query = new WP_Query($args);
  $content = '';

  foreach ($query->posts as $post)
  {
    $content .= '<div class="col-sm-4 col-xs-6 top_cards">';
    $content .= getCardPlace($post);
    $content .= '</div>';
  }
   
  return $content;  
}

// Возвращает массив ссылок для сортировки по дате, рейтингу и чеку
  function getLinks()
  {
    if ( strpos($_SERVER['REQUEST_URI'], '?') !== false )
      $linkcategory = strstr($_SERVER['REQUEST_URI'], '?', true);
    else
      $linkcategory = $_SERVER['REQUEST_URI'];
    
    if ( strpos($linkcategory, 'page') !== false )
      $linkcategory = strstr($_SERVER['REQUEST_URI'], 'page', true);

    $link = [];
    if( empty( $_GET ) )
    {
      $link['date'] = home_url().$linkcategory.'?orderby=date';
      $link['rating'] = home_url().$linkcategory.'?orderby=meta_value&meta=rating_total';
      $link['check'] = home_url().$linkcategory.'?orderby=meta_value_num&meta=check_min';
      $link['type']['all'] = home_url().$linkcategory.'?type=all';
      $link['type']['places'] = home_url().$linkcategory.'?type=places';
      $link['type']['post'] = home_url().$linkcategory.'?type=post';
      $link['type']['review'] = home_url().$linkcategory.'?type=review';
    }
    else
    {
      $temp_GET = $_GET;                
      $temp_GET['orderby'] = 'date';
      $temp_GET['meta'] = '';
      $link['date'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);
      
      $temp_GET['orderby'] = '';
      $link['without_date'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);


      $temp_GET = $_GET;                
      $temp_GET['orderby'] = 'meta_value_num';
      $temp_GET['meta'] = 'rating_total';
      $link['rating'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);

      $temp_GET['orderby'] = '';
      $temp_GET['meta'] = '';
      $link['without_rating'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);
      $link['without_check'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);

      $temp_GET = $_GET;                
      $temp_GET['orderby'] = 'meta_value_num';
      $temp_GET['meta'] = 'check_min';
      $link['check'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);

      $temp_GET = $_GET;
      $temp_GET['type'] = 'all';
      $link['type']['all'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);

      $temp_GET = $_GET;
      $temp_GET['type'] = 'places';
      $link['type']['places'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);

      $temp_GET = $_GET;
      $temp_GET['type'] = 'post';
      $link['type']['post'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);

      $temp_GET = $_GET;
      $temp_GET['type'] = 'review';
      $link['type']['review'] = home_url().$linkcategory.'?'.http_build_query($temp_GET);
    }
    
    return  $link;
  }

//Определение мобильного устройства при просмотре
  function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
  }

function _block_comment_life($is_parent, $className, $comment)
{
  echo '<div class="'.$className.'" data-idcommentblock="'.$comment->comment_ID.'">
    <div class="col-sm-1 padding_zero comment_avatar_block">
        <img src="'.get_avatar_url(get_comment_author_email($comment->comment_ID)).'" class="restMemberAvatar">
    </div>
    <div class="col-sm-11 padding_zero restCommBody">
      <div class="comment_header">
        <p class="restRewNickname">'.get_comment_author($comment->comment_ID).'</p>
        <p class="restCommDate">';
  
  if(get_comment_date('d', $comment->comment_ID) == date('d'))
    echo get_comment_date('СЕГОДНЯ в H:i', $comment->comment_ID);
  else
    echo get_comment_date( "d M Y в H:i", $comment->comment_ID);
  
  echo '</p>
        <div class="restCommRating '.(is_user_logged_in() ? 'user-logged' : 'user-logout').'" data-userid="'.get_current_user_id().'">';
  
  $comMeta = get_comment_meta($comment->comment_ID, 'voted_userID');

  if( !in_array(get_current_user_id(), $comMeta ) )
    echo '<span class="minus_vote_rating" data-idcommentrating="'.$comment->comment_ID.'"></span>';

  if( $comment->comment_karma > 0 )
    $className = "positiveRating";
  else if ( $comment->comment_karma < 0 )
    $className = "negativeRating";
  else
    $className = "ratingNull";
  
  echo '<span class="'.$className.'">'.$comment->comment_karma.'</span>';

  if( !in_array(get_current_user_id(), $comMeta ) )
    echo '<span class="plus_vote_rating"></span>';
          
  echo '</div>
      </div>
      <div class="restCommText">';

  if($comment->comment_approved == 1) 
    echo '<p>'.$comment->comment_content.'</p>';
  else if($comment->comment_approved == 0)
  {
    echo '<div class="restCommStatus comment_wait_moder">
            <p class="restCommWait">ОЖИДАЕТ МОДЕРАЦИИ</p>
          </div>';
  }

  echo '</div>';

  if ( $is_parent )
    echo '<div class="restCommStatus answer_comment_block" data-idcomment="'.$comment->comment_ID.'" data-iduser="'.(wp_get_current_user()->ID).'">
          <span class="btn_answer_comment '.(!is_user_logged_in() ? 'noactive' :'').'">ОТВЕТИТЬ</span>
        </div>';

  echo '</div> 
    </div>';
}

// Ф-ция выводит на экран блок оставленного комментария
// Используется в single страницах
function get_the_comment_life( $comments )
{
  foreach( $comments as $comment )
  {
    if ( empty($comment->comment_parent) )
    {
      $className = 'restCommentBlock col-sm-11 padding_zero';
      _block_comment_life(true, $className, $comment);
      
      $commentsChildren = get_comments('parent='.$comment->comment_ID);
      if ( !empty($commentsChildren) )
        get_the_comment_life($commentsChildren);
    }
    else
    { 
      $className = 'answer_block col-sm-11';
      _block_comment_life(false, $className, $comment);
    }
  }
}

