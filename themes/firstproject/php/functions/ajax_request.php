<?php

// Авторизация пользователя
  add_action('wp_ajax_ajaxuploaddataauthenticate', 'ajax_load_authenticate');
  add_action('wp_ajax_nopriv_ajaxuploaddataauthenticate', 'ajax_load_authenticate');
  function ajax_load_authenticate()
  {
    $user = wp_signon();

    if ( is_wp_error($user) )
      $err_mes = 'Неверный логин или пароль';

    isset($err_mes) 
      ? $arr['err'] = $err_mes
      : $arr['url'] = $_POST['curUrl'];  

    echo json_encode( $arr );

      die();
  }

// Регистрация пользователя
  add_action('wp_ajax_ajaxuploaddataregister', 'ajax_load_register');
  add_action('wp_ajax_nopriv_ajaxuploaddataregister', 'ajax_load_register');

  function ajax_load_register()
  {
    check_ajax_referer('secure_code_reg', 'security');

    $userData = array
      (
        'user_login'  => $_POST['login'],
        'user_pass'   => $_POST['password'],
        'user_email'  => $_POST['email']
      );

    $userId = wp_insert_user($userData);

    if ( !is_wp_error( $userId ) )
    {
      $arr['dataUser']['login'] = $_POST['login'];
      $arr['dataUser']['password'] = $_POST['password'];
      $arr['url'] = $_POST['curUrl'];
     echo json_encode( $arr );
    }
    else
    {
      if ( username_exists( $_POST['login'] ) )
        $arr['err']['name'] = 'Пользователь с таким именем уже существует!';
      else if ( email_exists( $_POST['email'] ) && !array_key_exists('err', $arr) )
        $arr['err']['email'] = 'Такой электронный адрес уже существует!';
      else
        $arr['err'] = 'Что-то не так, попробуйте еще раз!';

      echo json_encode( $arr );
    }

      die();
  }

// Восстановление пароля
  add_action('wp_ajax_ajaxrecoverypass', 'recovery_pass');
  add_action('wp_ajax_nopriv_ajaxrecoverypass', 'recovery_pass');

  function recovery_pass()
  {
    check_ajax_referer('security_recovery', 'security');

    $user = get_user_by('email', $_POST['email']);

    if ( !$user )
    {
      $arr['err'] = 'E-mail не зарегистрирован!';
      echo json_encode( $arr );
      die();
    }

    $newPass = wp_generate_password( 12, false);

    $args = array
    (
      'ID'  => $user->ID,
      'user_pass' => $newPass
    );

    add_user_meta($user->ID, 'password', $newPass, true);

    $user_id = wp_update_user( $args );

    if ( is_wp_error( $user_id ) ) {
      $arr['err'] = 'Какае-то ошибка!(';
    } else {
      $arr['suc'] = 'All ok!)';
    }

   // echo json_encode( $arr );
    die();
  }

// Возвращаем клиенту название подкатегорий
  add_action('wp_ajax_ajaxsubcategory', 'subCategories');
  add_action('wp_ajax_ajaxsubcategory', 'subCategories');
  function subCategories()
  {
    echo json_encode(ss_get_sub_categories( $_POST['id'] ));
    die();
  }

// Добавляем новое заведение
  add_action('wp_ajax_ajaxnewplace', 'newPlace');
  add_action('wp_ajax_ajaxnewplace', 'newPlace');
  function newPlace()
  {
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );

    wp_verify_nonce($_POST['security'], 'secret_key');
    
    $args = array
    (
      'comment_status' => 'open',
      'post_author' => get_current_user_id(),
      'post_category' => array($_POST['main_category'], $_POST['sub_category']),
      'post_content' => $_POST['description'],
      'post_status' => 'publish',
      'post_title' => $_POST['title'],
      'post_type' => 'places'      
    );

    $post_id = wp_insert_post($args);

    if ( !empty($_POST['averagecheck']) )
      add_post_meta($post_id, 'check', $_POST['averagecheck'], true);
    if ( !empty($_POST['check_min']) )
      add_post_meta($post_id, 'check_min', $_POST['check_min'], true);
    if ( !empty($_POST['number_tel']) )
      add_post_meta($post_id, 'phone', $_POST['number_tel'], true);
    if ( !empty($_POST['hours']) )
      add_post_meta($post_id, 'hours', $_POST['time_work'], true);
    if ( !empty($_POST['owner']) )
      add_post_meta($post_id, 'owner', $_POST['owner'], true);
    if ( !empty($_POST['country']) )
      add_post_meta($post_id, 'country', $_POST['country'], true);
    if ( !empty($_POST['city']) )
      add_post_meta($post_id, 'city', $_POST['city'], true);
    if ( !empty($_POST['address']) )
    {
      add_post_meta($post_id, 'address_', $_POST['address'].', '.$_POST['number_building'], true);
      add_post_meta($post_id, 'address_name', $_POST['address_name'], true);
      add_post_meta($post_id, 'address_number', $_POST['address_number'], true);
    }


    if ( !empty($_POST['latitude']) )
      add_post_meta($post_id, 'latitude', $_POST['latitude'], true);
    if ( !empty($_POST['longitude']) )
      add_post_meta($post_id, 'longitude', $_POST['longitude'], true);
    if ( !empty($_POST['district']) )
      add_post_meta($post_id, 'rayon', $_POST['district'], true);
    if ( !empty($_POST['subway']) )      
      add_post_meta($post_id, 'metro', $_POST['subway'], true);
    if ( !empty($_POST['website']) )            
      add_post_meta($post_id, 'website', $_POST['website'], true);
    if ( !empty($_POST['twitter']) )
      add_post_meta($post_id, 'twitter', $_POST['twitter'], true);
    if ( !empty($_POST['facebook']) )      
      add_post_meta($post_id, 'fb', $_POST['facebook'], true);
    if ( !empty($_POST['contacts']) )
      add_post_meta($post_id, 'admin_contacts', $_POST['contacts'], true);

    // добавляем миниатюру и фото к посту
      $count = 0;
      foreach ( $_FILES as $file_array )
      {
        $id = media_handle_sideload($file_array, $post_id);
        
        if (is_wp_error($id))
        {
          @unlink($file_array['tmp_name']);
          echo json_encode($id);
          die();
        }
        else if ( $count == 0)
          update_post_meta($post_id, '_thumbnail_id', $id);

        @unlink( $file_array['tmp_name'] );
        $count++;
      }

    echo json_encode($_POST);
    die();
  }

/*
//  Для кнопки LoadMore. Формируем контен и отсылаем клиенту
*/
  add_action('wp_ajax_ajaxuploaddataposts', 'ajax_morePosts');
  add_action('wp_ajax_nopriv_ajaxuploaddataposts', 'ajax_morePosts');

// Функция возвращает клиенту карточки в виде html кода
  function ajax_morePosts()
  {
    $maxCountPosts          = $_POST['maxcountposts'];
    if ( empty($maxCountPosts) )
    {
      $postType = explode(',',$_POST['posttype']);

      $maxCountPosts = 0;
      for ($i = 0; $i < count($postType); $i++)
      {
        $statusComment = wp_count_posts($postType[$i]);
        $maxCountPosts += $statusComment->publish;
      }  
    }
  
    $countPosts             = ($_POST['countposts'] != 'undefined') 
                            ? $_POST['countposts'] 
                            : 8;
    $content['offset']      = $_POST['offset'];
    $postType               = $_POST['posttype'];
    $content['showButton']  = true;

    $args = array
    (
      'posts_per_page'  => $countPosts,
      'offset'          => $content['offset'],
      'post_type'       => explode(', ', $postType)
    );

    if ( isset($_POST['search']) && !empty( $_POST['search'] ) )
    {
      parse_str(substr($_POST['search'], 1), $args_add);
      $args = array_merge( $args, create_query_sort_filter( $args_add ) );
    }

    $query = new WP_Query( $args );
    $posts = $query->posts;

    $max = $query->found_posts < $countPosts
        ? $query->found_posts
        : $countPosts;

    if ( isset($_POST['cardtype']) && $_POST['cardtype'] == 'place' )
      $content['content'] = _get_content_card_place($posts, $max);
    else
      $content['content'] = _get_content_card_post($posts, $max);

    wp_reset_postdata();

    $content['offset'] += $countPosts;

    if ( empty($_POST['maxcountposts']) )
      $content['maxcountposts'] = $maxCountPosts;

    if ( $content['offset'] >= $maxCountPosts )
    {
      $content['offset'] = $maxCountPosts;
      $content['showButton'] = false;
    }

    echo json_encode(  $content );
    //echo json_encode( $_POST );

    die();
  }

/*
//  Для кнопки LoadMore Comments. Формируем контен и отсылаем клиенту
*/
  add_action('wp_ajax_ajaxuploaddatacomments', 'ajax_moreComments');
  add_action('wp_ajax_nopriv_ajaxuploaddatacomments', 'ajax_moreComments');

// Функция возвращает клиенту карточки в виде html кода
  function ajax_moreComments()
  {
    $maxCountComments          = $_POST['maxcountcomments'];
    if ( empty($maxCountComments) )
      $maxCountComments = 0;
  
    $countComments          = ($_POST['countcomments'] != 'undefined') 
                            ? $_POST['countcomments'] 
                            : 8;


    $content['showButton']  = true;

    $content['content'] = ss_getComments(
                                          $countComments,
                                          $_POST['offset'],
                                          $_POST['catid'],
                                          $_POST['classes']
                                        );
    
    $content['offset'] = $_POST['offset'] + $countComments;

    $content['maxcountcomments'] = $maxCountComments;

    if ( $content['offset'] >= $maxCountComments )
    {
      $content['offset'] = $maxCountComments;
      $content['showButton'] = false;
    }

   echo json_encode( $content );
 //echo json_encode( $countComments );

    die();
  }

// По запросу возвращаем загаловки и ссылки постов
  add_action('wp_ajax_ajaxpoststіtle', 'ss_get_title_posts');
  add_action('wp_ajax_nopriv_ajaxpoststіtle', 'ss_get_title_posts');
  function ss_get_title_posts()
  {
    global $wpdb;

    $posts_per_page = 500;
    $offset = $_POST['offset'];
    $query = "SELECT ID, post_title, guid 
              FROM gvult.gv_posts 
              WHERE post_type IN ('places') 
                AND post_status = 'publish' LIMIT $offset, $posts_per_page";

    $posts['data'] = $wpdb->get_results($query);

    $offset += $posts_per_page;
    
    $posts['offset'] = $offset;

    $posts['nextRequest'] = true;
    if ( $posts_per_page > count($posts['data']) )
      $posts['nextRequest'] = false;

    echo json_encode($posts);  
    die();
  }

// По запросу возвращаем id и адреса заведений
  add_action('wp_ajax_ajaxpostsaddress', 'ss_get_address_places');
  add_action('wp_ajax_nopriv_ajaxpostsaddress', 'ss_get_address_places');
  function ss_get_address_places()
  {
    global $wpdb;
    $query = "SELECT post_id, meta_value 
              FROM gvult.gv_postmeta 
              WHERE meta_key = 'address'";
                

    $addresses = $wpdb->get_results($query);

    echo json_encode($addresses);  
    die();
  }

// Функция для отрисовки нового комментария
  add_action('wp_ajax_ajaxcomment', 'ss_get_comment_content');
  function ss_get_comment_content()
    { 

      date_default_timezone_set('Europe/Kiev');
      $currentUs = wp_get_current_user();
  
      if ( is_user_logged_in() ) {

          $post_data = array(
            'comment_post_ID'       =>  $_POST['postID'],
            'comment_content'       =>  $_POST['commentContent'],
            'comment_author'        =>  $currentUs->user_login,
            'comment_author_email'  =>  $currentUs->user_email,
            'comment_approved'      =>  1,
          );
        $commentID =  wp_insert_comment($post_data);
      }

          $commentParams = array(
            'comment_post_ID'       =>  $_POST['postID'],
            'comment_content'       =>  $_POST['commentContent'],
            'comment_author'        =>  $currentUs->user_login,
            'comment_author_email'  =>  $currentUs->user_email,
            'comment_approved'      =>  1,   
            'comment_author_ava'    =>  get_avatar_url($currentUs->user_email),
            'comment_date'          =>  'СЕГОДНЯ в ' . date('H:i'),
            'comment_karma'         =>  $commentKarma,
            'comment_ID'            =>  $commentID 
          );

      echo json_encode($commentParams);    
      die();
    }

// Функция добавления отрицательного рейтинга для комментария
  add_action('wp_ajax_ajaxcommentratingminus', 'ss_update_comment_rating_minus');
  function ss_update_comment_rating_minus(){
      $commentArr;
      if($_POST['userID'] != 0){
        $commentArr = array(
        'comment_ID'      => $_POST['commentID'],
        'comment_karma'   => $_POST['commentKarma'],
        'userID'          => $_POST['userID'],
      );
      }
      else{
        $commentArr = array(
        'comment_ID'      => $_POST['commentID'],
        'comment_karma'   => $_POST['commentKarma']
        );
      }
      
      wp_update_comment($commentArr); 
      $metaArr = get_comment_meta($_POST['commentID'], 'voted_userID');
      
      if(in_array($_POST['userID'], $metaArr) == false){
          add_comment_meta( $_POST['commentID'], 'voted_userID', $_POST['userID']);
        }


      $post_data = array(
          'comment_karma'   => $commentArr['comment_karma'],
        );

      echo json_encode($commentArr);
      die();
    }

// Функция добавления положительного рейтинга для комментария
  add_action('wp_ajax_ajaxcommentratingplus', 'ss_update_comment_rating_plus');
  function ss_update_comment_rating_plus(){
      
      $commentArr;
      if($_POST['userID'] != 0){
        $commentArr = array(
        'comment_ID'      => $_POST['commentID'],
        'comment_karma'   => $_POST['commentKarma'],
        'userID'          => $_POST['userID'],
      );
      }
      else{
        $commentArr = array(
        'comment_ID'      => $_POST['commentID'],
        'comment_karma'   => $_POST['commentKarma']
        );
      }

      wp_update_comment($commentArr);
      $metaArr = get_comment_meta($_POST['commentID'], 'voted_userID');
      
        if(in_array($_POST['userID'], $metaArr) == false){
          add_comment_meta( $_POST['commentID'], 'voted_userID', $_POST['userID']);
        }
      
      $post_data = array(
          'comment_karma'   => $commentArr['comment_karma']
        );
      echo json_encode($commentArr);
      die();
    }

// По запросу возвращаем топ 5 карточек заведений
  add_action('wp_ajax_ajaxtopcards', 'ajax_send_top_cards');
  add_action('wp_ajax_nopriv_ajaxtopcards', 'ajax_send_top_cards');
  function ajax_send_top_cards()
  {
    echo json_encode(get_top_five_cards_places( $_POST['catid'] ));  
    die();
  }

// По запросу возвращаем координаты заведения
  add_action('wp_ajax_coordinates', 'ajax_send_coordinates');
  add_action('wp_ajax_nopriv_coordinates', 'ajax_send_coordinates');
  function ajax_send_coordinates()
  {
    $data['imgPath'] = get_template_directory_uri().'/img/google-map/';
    $data['coordinates']['lat'] = (float) get_post_meta($_POST['postid'], 'latitude', true);
    $data['coordinates']['lng'] = (float) get_post_meta($_POST['postid'], 'longitude', true);

    $data['title'] = get_the_title($_POST['postid']);
    $data['address'] = getAddress( get_post($_POST['postid']) );
    $data['email'] = get_post_meta($_POST['postid'], 'email', true);
    $data['phone'] = get_post_meta($_POST['postid'], 'phone', true);

    echo json_encode($data);  
    die();
  }

// По запросу возвращаем заведения выбранной категории
  add_action('wp_ajax_placesofcategory', 'ajax_send_placesofcategory');
  add_action('wp_ajax_nopriv_placesofcategory', 'ajax_send_placesofcategory');
  function ajax_send_placesofcategory()
  {
    $args = array
    (
      'posts_per_page'  => 500,
      'offset'          => (int) $_POST['offset'],
      'post_type'       => 'places',
      'cat'             => $_POST['catid']
    );


    if ( !empty($_POST['search_string']) && $_POST['search_string'] != '?map' )
    {
      $searchString = parse_url($_POST['search_string'], PHP_URL_QUERY);
      $args = array_merge($args, create_query_sort_filter($searchString));
    }
              
    echo json_encode(_getDataMap( $args ));  
    die();
  }

// По запросу возвращаем заведения при поиске
  add_action('wp_ajax_searchposts', 'ajax_send_searchposts');
  add_action('wp_ajax_nopriv_searchposts', 'ajax_send_searchposts');
  function ajax_send_searchposts()
  {
    $postsID = explode(',', $_POST['postid']);

    $args = array
      (
        'posts_per_page'  => -1,
        'post_type'       => 'places',
        'post__in'        => $postsID
      );

    echo json_encode(_getDataMap( $args ));  
    die();
  }

// По запросу возвращаем заведения. Страница City Guide
  add_action('wp_ajax_placesofcityguide', 'ajax_send_placesofcityguide');
  add_action('wp_ajax_nopriv_placesofcityguide', 'ajax_send_placesofcityguide');
  function ajax_send_placesofcityguide()
  {
    $args = json_decode(stripcslashes($_POST['query']));
    unset($args->paged);

    $args->posts_per_page = 500;
    $args->offset = $_POST['offset'];
    
    echo json_encode(_getDataMap( (array) $args ));    
    die();
  }
  
  // Функция возвращает массив данных для отображения на карте
  function _getDataMap( $args /*array*/ )
  {
    $query = new WP_Query($args);

    $count = 0;
    foreach ($query->posts as $i => $post)
    {
      if ( (float) get_post_meta($post->ID, 'latitude', true) == 0 ) continue;

      $data['coordinates'][$count]['lat'] = (float) get_post_meta($post->ID, 'latitude', true);
      $data['coordinates'][$count]['lng'] = (float) get_post_meta($post->ID, 'longitude', true);

      $data['address'][$count] = getAddress( $post );
      $data['title'][$count] = $post->post_title;
      $data['email'][$count] = get_post_meta($post->ID, 'email', true);
      $data['phone'][$count] = get_post_meta($post->ID, 'phone', true);

      $count++;
    }

    if ( isset($args['offset']) )
      $args['offset'] += 500;
    else
      $args['offset'] = 0;

    $data['offset'] = $args['offset'];

    $data['nextRequest'] = true;
    if ( empty($args['offset']) || $args['offset'] >= $query->found_posts )
      $data['nextRequest'] = false;

    $data['foundPosts'] = $query->found_posts;
    $data['imgPath'] = get_template_directory_uri().'/img/google-map/';

    return $data;
  }



//Функция добавления комментария
add_action('wp_ajax_addcommentanswer', 'ajax_get_answer');
add_action('wp_ajax_nopriv_addcommentanswer', 'ajax_get_answer');
  
  function ajax_get_answer(){
    date_default_timezone_set('Europe/Kiev');
    $currentUs = wp_get_current_user();
     if ( is_user_logged_in() ) {

          $post_data = array(
            'comment_content'      =>  $_POST['commentContent'],
            'comment_author'       =>  $currentUs->user_login,
            'comment_parent'       =>  $_POST['commentID'],
            'comment_approved'     =>  0,
            'comment_post_ID'      =>  $_POST['postID']

          );
          wp_insert_comment($post_data);
      }


    $data = array(
      'commentContent'      =>  $_POST['commentContent'],
      'answerUser'          =>  $currentUs->user_login,
      'commentID'           =>  $_POST['commentID'],
      'comment_author_ava'  =>  get_avatar_url($currentUs->user_email),
      'date'                =>  'СЕГОДНЯ в ' . date('H:i'),
      'comment_approved'    =>  1 
       );

    echo json_encode($data);  
    die();
  }


//Функция отрисовки и обновления рейтинга заведения
add_action('wp_ajax_addplacerating', 'ajax_set_place_rating');
add_action('wp_ajax_nopriv_addplacerating', 'ajax_set_place_rating');
  function ajax_set_place_rating(){

      $postCountPoll = get_post_meta($_POST['postId'], 'rating_poll_count', true);
      if(empty($postCountPoll)){
          add_post_meta($_POST['postId'], 'rating_poll_count', 1);
      }
      else{
          $pollCount = $postCountPoll + 1;
          update_post_meta($_POST['postId'], 'rating_poll_count', $pollCount);
      }


      $postAvarageRtng = get_post_meta($_POST['postId'], 'rating_average', true);
      $rating = $_POST['rating'];
   
      if(empty($postAvarageRtng)){
          add_post_meta($_POST['postId'], 'rating_average', $rating);
      }
      else{
          $total = $postAvarageRtng*$postCountPoll+$rating;
          $users = $postCountPoll + 1;
          $totalFinal = $total/$users;

          update_post_meta($_POST['postId'], 'rating_average', $totalFinal);
      }


      $arrayPosts = get_user_meta($_POST['userId'], 'placerating_voted_posts', true);
          $aaa =  $arrayPosts;
          if ( empty($arrayPosts) )
              update_user_meta($_POST['userId'], 'placerating_voted_posts', array($_POST['postId']));
          else
          {
            if(!in_array($_POST['postId'], $arrayPosts)){
              array_push($arrayPosts, $_POST['postId']);
              update_user_meta($_POST['userId'], 'placerating_voted_posts', $arrayPosts);
            }
      }
 
      $data = array(
          'place_rating'        => get_post_meta($_POST['postId'], 'rating_average', true),
          'post'                => $_POST,
          'rating_aver'         => $postAvarageRtng,
          'arrayPosts'          => $aaa,
          'arrayPostsLater'     => $arrayPosts 

        );

      
      echo json_encode($data);
      die();
  }


