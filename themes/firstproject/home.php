<?php 
	get_header();
	print_var('home.php');
?>

<div class="container-fluid">


	<div class="row contentDiv1 blockMargin">
		<div  class="col-xs-8">
			<div class="row firstDivContainer">

						<a href="#" class="col-xs-12 blackoutPictureLink typeBlock1">
							<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/620x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
								<figcaption>
									<h5 class="cardHearedH5">CULTURE</h5>
									<p class="paragraf">3 удивительных факта о ресторанах столицы которых вы не знали</p>

								</figcaption>
							</figure>
						</a>
			</div>
			
			<div class="row firstDivContainer padding_zero blockMargin">
			
		
						<a href="#" class="col-xs-4 blackoutPictureLink typeBlock2">
							<figure class="blueBorderBlock">
								<figcaption>
									<p class="border-card-num">16</p>
									<p class="border-card-par">проверенных конференц-холлов</p>
								</figcaption>
								<div class="blockPictureDate">
									<img src="http://placehold.it/158x129" class="img-responsive categoryImages">
								
								</div>
							</figure>
						</a>

			
						<figure>
								<a href="#" class="col-xs-8 blackoutPictureLink typeBlock3">
								<div class="blockPictureDate">
									<img src="http://placehold.it/425x215" class="img-responsive categoryImages">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
								<figcaption>
									<h5 class="cardHearedH5">GASTRO LIFE</h5>
									<p class="paragraf">Любовь без границ в кампейне юбилейной Ukrainian Fashion Week</p>
								</figcaption>
							
								</a>
						</figure>
			
			</div>



			<div class="row firstDivContainer padding_zero blockMargin">
				<a href="#"  class="col-xs-4 blackoutPictureLink typeBlock4">
					<figure>
						<div class="blockPictureDate">
							<img src="http://placehold.it/189x215" class="img-responsive categoryImages">
							<p class="blockHiddenDate">20 декабря, 12:02</p>
						</div>
						<figcaption>
							<h5 class="cardHearedH5">FASHION&BEAUTY</h5>
							<p class="paragraf">Сон медленной волны</p>
						</figcaption>
					</figure>
				</a>

				<a href="#" class="col-xs-4 blackoutPictureLink typeBlock2">
					<figure class="blueBorderBlock">
						<figcaption>
							<p class="border-card-num">8</p>
							<p class="border-card-par">необычных ресторанов столицы</p>
						</figcaption>
						<div class="blockPictureDate"><img src="http://placehold.it/158x129" class="img-responsive categoryImages"></div>
					</figure>
				</a>

				<a href="#" class="col-xs-4 blackoutPictureLink  typeBlock4">
					<figure>
						<div class="blockPictureDate">
							<img src="http://placehold.it/189x215" class="img-responsive categoryImages">
							<p class="blockHiddenDate">20 декабря, 12:02</p>
						</div>
						<figcaption>
							<h5 class="cardHearedH5">CITY NEWS</h5>
							<p class="paragraf">Препятствия городских джунглей</p>
						</figcaption>
					</figure>
				</a>
			</div>	


			<div class="row firstDivContainer  padding_zero">
				
				<div class="col-xs-6">
					<a href="#">
						<div class="blockPictureDate">
							<img src="http://placehold.it/300x250" class="img-responsive categoryImages">
						</div>
					</a>
				</div>
				<div class="col-xs-6 blackoutPictureLink">
					<a href="#">
						<div class="blockPictureDate">
							<img src="http://placehold.it/300x250" class="img-responsive categoryImages">
						</div>
					</a>
				</div>

			</div>	
		
		</div>

		<div class="col-xs-4 lastNewsblock">

			<div class=" firstDivContainer  margin_zero padding_zero blockPictureDate">
				<a href="#" class="blackoutPictureLink">
					<div class="blockPictureDate">
						<img src="http://placehold.it/300x600" class="img-responsive categoryImagesNews">
					</div>
				</a>
			</div>

			<div class="firstDivContainer  ">
				<div class="col-xs-12 newsFirstDiv">
						
							<h4>Последние новости</h4>
	
							<a href="#" class="col-xs-12">
								<figure class="moreNewsblocks">
									<img src="http://placehold.it/250x160" class="img-responsive categoryImagesNews">
									<figcaption>Майстер-клас із виготовлення різдвяної зірки</figcaption>
								</figure>
							</a>

							<a href="#" class="col-xs-12 newsMargin">
								<figure class="moreNewsblocks">
									
									<img src="http://placehold.it/250x160" class="img-responsive categoryImagesNews">
									
									<figcaption>Солодкі години у Тесла Паб!</figcaption>
								</figure>
							</a>

							<a href="#" class="col-xs-12 newsMargin">
								<figure class="moreNewsblocks">
									<img src="http://placehold.it/250x160" class="img-responsive categoryImagesNews">
									<figcaption>День Святого Валентина: виноградные улитки от Ulitka bar</figcaption>
								</figure>
							</a>
							
							<div class="col-xs-12 news-block-buttons">
										
									<a href="#" class="btn-standart-border catfilter-more-news-btn">БОЛЬШЕ НОВОСТЕЙ</a>
									
									<div>	
										<a href="#" class="more-news-switchrs">
											<img src="<?php bloginfo('template_url'); ?>/img/right-arrow.png">
										</a>

										<a href="#" class="more-news-switchrs">
											
											<img src="<?php bloginfo('template_url'); ?>/img/left-arrow.png">
										</a>
										</div>
								</div>			
					</div>
			</div>
		</div>
	</div>
	



	<div class="row contentDiv3">
		<div class="col-xs-12">	
				<div class="col-md-12 catalogMainHead">
					<p class="bigHeaderParagraf-inx1">city guide</p>
				</div>

			
		
				
				<div class="col-md-4 col-sm-6">
				<div class="catalogElements">
					
					<p class="catalogHeaders">Рестораны</p>
					<div class="row block margin_zero padding_zero">
						
						<p class="catParagrafs">Мест:965<br>Отзывов:111</p>
						<img src="http://placehold.it/138x132" class="imgCatalog">
						<a href="#" class="catalogButtons">ПОСМОТРЕТЬ ВСЕ</a>
					</div>
				</div>
				</div>

				<div class="col-md-4 col-sm-6">
				<div class="catalogElements">
					
					<p class="catalogHeaders">Рестораны</p>
					<div class="row block margin_zero padding_zero">
						
						<p class="catParagrafs">Мест:965<br>Отзывов:111</p>
						<img src="http://placehold.it/138x132" class="imgCatalog">
						<a href="#" class="catalogButtons">ПОСМОТРЕТЬ ВСЕ</a>
					</div>
				</div>
				</div>

				<div class="col-md-4 col-sm-6">
				<div class="catalogElements">
					
					<p class="catalogHeaders">Рестораны</p>
					<div class="row block margin_zero padding_zero">
						
						<p class="catParagrafs">Мест:965<br>Отзывов:111</p>
						<img src="http://placehold.it/138x132" class="imgCatalog">
						<a href="#" class="catalogButtons">ПОСМОТРЕТЬ ВСЕ</a>
					</div>
				</div>
				</div>

				<div class="col-md-4 col-sm-6">
				<div class="catalogElements">
					
					<p class="catalogHeaders">Рестораны</p>
					<div class="row block margin_zero padding_zero">
						
						<p class="catParagrafs">Мест:965<br>Отзывов:111</p>
						<img src="http://placehold.it/138x132" class="imgCatalog">
						<a href="#" class="catalogButtons">ПОСМОТРЕТЬ ВСЕ</a>
					</div>
				</div>
				</div>

				<div class="col-md-4 col-sm-6">
				<div class="catalogElements">
					
					<p class="catalogHeaders">Рестораны</p>
					<div class="row block margin_zero padding_zero">
						
						<p class="catParagrafs">Мест:965<br>Отзывов:111</p>
						<img src="http://placehold.it/138x132" class="imgCatalog">
						<a href="#" class="catalogButtons">ПОСМОТРЕТЬ ВСЕ</a>
					</div>
				</div>
				</div>

				<div class="col-md-4 col-sm-6">
				<div class="catalogElements">
					
					<p class="catalogHeaders">Рестораны</p>
					<div class="row block margin_zero padding_zero">
						
						<p class="catParagrafs">Мест:965<br>Отзывов:111</p>
						<img src="http://placehold.it/138x132" class="imgCatalog">
						<a href="#" class="catalogButtons">ПОСМОТРЕТЬ ВСЕ</a>
					</div>
				</div>
				</div>
				


			<div class="row margin_zero padding_zero">
				<div class="col-xs-4 catalogIcons">
					<img class="" src="<?php bloginfo('template_url'); ?>/img/home-icon.png">
					<p>ДЛЯ ДОМА</p>
				</div>
				<div class="col-xs-4 catalogIcons">
					<img class="" src="<?php bloginfo('template_url'); ?>/img/kids-icon.png">
					<p>ДЕТИ</p>
				</div>
				<div class="col-xs-4 catalogIcons">
					<img class="" src="<?php bloginfo('template_url'); ?>/img/sport-icon.png">
					<p>СПОРТ</p>
				</div>
			</div>
		
		</div>
	</div>




	<div class="row contentDiv4">
			<a href="#">
				<div class="col-xs-8 blueBorder">
					<figure>
						<figcaption class="mainReview">
							<div class="blueBorder-blockImg">
								<img src="http://placehold.it/430x250" class="img-responsive blueBorderImg">
							</div>
							<h5 class="cardHearedH5">FASION&BEATY</h5>
							<p class="paragraf">Почему она плачет, когда смотрит на картины Рафаэля, забрасывая свой скетчбук подальше.</p>
						</figcaption>
					</figure>
				</div>
			</a>

		<div class="col-xs-4">
			<div class="reviews">
				
				<h4 >Последние отзывы заведений</h4>
				<a href="#">
					<figure class="col-xs-12 padding_zero reviewBlocks">
						
						<figcaption>
							<p class="ind-rewTime">16:15</p>
							<h5 class="ind-rewheadBlock">Студия кухонь Погген Поль</h5>
							<p class="secondNewsParagrafs">Не советую связыватся с салоном мебели на Саксаганского 24 …</p>
						</figcaption>
					</figure>
				</a>
		
				<a href="#">
					<figure class="col-xs-12 padding_zero reviewBlocks">
						<figcaption>
							<p class="ind-rewTime">16:15</p>
							<h5 class="ind-rewheadBlock">Студия кухонь Погген Поль</h5>
							<p class="secondNewsParagrafs">Не советую связыватся с салоном мебели на Саксаганского 24 …</p>
						</figcaption>

					</figure>
				</a>
				
				<a href="#">
					<figure class="col-xs-12 padding_zero reviewBlocks reviewLastBlock">
						<figcaption>
							<p class="ind-rewTime">16:15</p>
							<h5 class="ind-rewheadBlock">Студия кухонь Погген Поль</h5>
							<p class="secondNewsParagrafs">Не советую связыватся с салоном мебели на Саксаганского 24 …</p>
						</figcaption>

					</figure>
				</a>
				<div>
					<a href="#" class="btn-standart-border ind-review-btn">БОЛЬШЕ ОТЗЫВОВ</a>
				</div>
				
			</div>
		</div>
	</div>
		


	<div  class="row contentDiv5">
		<div class="row margin_zero">
			<div class="col-xs-8 div5Block1">
				<a href="#"><img src="http://placehold.it/700x275" class="img-responsive"></a>
			</div>
			<div class="col-xs-4 div5Block2">
				<a href="#"><img src="http://placehold.it/326x263" class="img-responsive"></a>
			</div>
		</div>
	</div>


	<div class="row margin_zero padding_zero">
		<div class="row">
		<div class="col-xs-12 bigHeader">
			<p class="bigHeaderParagraf-inx2">премьеры</p>
		</div>
		</div>	
  	</div>


		<div class="row ind-secondslider">
			  <div id="myCarousel-Two" class="col-xs-12 padding_zero carousel slide" data-ride="carousel">
			  								
					    <!-- Carousel indicators -->
					    <ol class="carousel-indicators">
					        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					        <li data-target="#myCarousel" data-slide-to="1"></li>
					        <li data-target="#myCarousel" data-slide-to="2"></li>
					    </ol>   
					    <!-- Wrapper for carousel items -->

					    <div class="carousel-inner">
					        <div class="item active">
					            <img src="http://placehold.it/1000x240" alt="First Slide">
					        </div>
					        <div class="item">
					            <img src="http://placehold.it/1000x240" alt="Second Slide">
					        </div>
					        <div class="item">
					            <img src="http://placehold.it/1000x240" alt="Third Slide">
					        </div>
					    </div>
					    <!-- Carousel controls -->
					    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
					        <span class="glyphicon glyphicon-chevron-left"></span>
					    </a>
					    <a class="carousel-control right" href="#myCarousel" data-slide="next">
					        <span class="glyphicon glyphicon-chevron-right"></span>
					    </a>
											    
				</div>
		</div>


	<div class="row contentDiv6">
		<div class="col-xs-12">
			<div class="col-xs-12 premiereRevBanner">
				<div class="col-xs-7 ">
					<p class="bigHeaderParagraf-inx3">обзор</p>
					<a href="">
						<p class="premiereRevH2">«Фьюжн Экспресс» на Street Food Weekend 2016</p>
						<p class="premiereRevParagraf">Они не просто берут стандартные наработки, а адаптируют их под наши реалии, относятся к делу профессионально и нестандартно. Ребята не пытаются совместить итальянскую еду, борщ и суши.</p>
					</a>
				</div>
			</div>

		</div>


				<div class="row padding_zero margin_zero">
					<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
						<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">CITY NEWS</h5>
								<p class="paragraf">Чужой на карте среди своих</p>
							</figcaption>
						</figure>
					</a>	

					<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
						<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">FASHION&BEAUTY</h5>
								<p class="paragraf">Дух не спрятать за немецкими зданиями</p>
							</figcaption>
						</figure>
					</a>

					<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
						<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">GASTRO LIFE</h5>
								<p class="paragraf">Хозяева без золотой середины</p>
							</figcaption>
						</figure>
					</a>
				</div>


				<div class="row padding_zero margin_zero">
					<a href="#" class="col-xs-8 blackoutPictureLink">
						<figure class="divWithBlueBorder">
								<div class="blockPictureDate">
									<img src="http://placehold.it/460x410" class="img-responsive imgBorder">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption class="borderDescription">
								<h5 class="cardHearedH5">CULTURE</h5>
								<p class="paragraf">5 ричин любить летний отдых в Киеве таким, какой он есть</p>
							</figcaption>
						</figure>
					</a>

					<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
						<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">FASHION&BEAUTY</h5>
								<p class="paragraf">Сквозь призму янтаря</p>
							</figcaption>
						</figure>
					</a>
					</div>

					<div class="row padding_zero margin_zero  contDiv6-cardblock">
					<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
						<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">FASHION&BEAUTY</h5>
								<p class="paragraf">Как проходил пикник Street Food Weekend</p>
							</figcaption>
						</figure>
					</a>

					<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
						<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">CULTURE</h5>
								<p class="paragraf">Хорошо, когда соседи — врачи</p>
							</figcaption>
						</figure>
					</a>

					<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
						<figure>
								<div class="blockPictureDate">
									<img src="http://placehold.it/310x335" class="img-responsive">
									<p class="blockHiddenDate">20 декабря, 12:02</p>
								</div>
							<figcaption>
								<h5 class="cardHearedH5">GASTRO LIFE</h5>
								<p class="paragraf">Gotland Round — шопинг по-новому</p>
							</figcaption>
						</figure>
					</a>
			</div>
	</div>	


	<div class="row margin_zero padding_zero">
			<div class="row">
			<div class="col-xs-12 bigHeader">
				<p class="bigHeaderParagraf-inx2">follow us</p>
			</div>
			</div>	
	</div>



	<div class="row socialBanner  margin_zero padding_zero">
		<div class="col-xs-12 socialBannerIcons">
			<p class="catalogHeaders">Подпишитесь на нас в социальных сетях</p>
			<div>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/facebook.png" alt="..."></a>
				<a href="#" class="instagramIcon"><img src="<?php bloginfo('template_url'); ?>/img/instagram.png" alt="..."></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/r-s-s.png" alt="..."></a>
			</div>	
		</div>
	</div>

	<div class="row contentDiv7">

								<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
									<figure>
												<div class="blockPictureDate">
													<img src="http://placehold.it/310x335" class="img-responsive">
													<p class="blockHiddenDate">20 декабря, 12:02</p>
												</div>
											<figcaption>
												<h5 class="cardHearedH5">CITY NEWS</h5>
												<p class="paragraf">Чужой на карте среди своих</p>
											</figcaption>
									</figure>
								</a>

								<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
									<figure>
												<div class="blockPictureDate">
													<img src="http://placehold.it/310x335" class="img-responsive">
													<p class="blockHiddenDate">20 декабря, 12:02</p>
												</div>
											<figcaption>
												<h5 class="cardHearedH5">FASHION&BEAUTY</h5>
												<p class="paragraf">Дух не спрятать за немецкими зданиями</p>
											</figcaption>
									</figure>
								</a>

								<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
									<figure>
												<div class="blockPictureDate">
													<img src="http://placehold.it/310x335" class="img-responsive">
													<p class="blockHiddenDate">20 декабря, 12:02</p>
												</div>
											<figcaption>
												<h5 class="cardHearedH5">GASTRO LIFE</h5>
												<p class="paragraf">Хозяева без золотой середины</p>	
											</figcaption>
									</figure>
								</a>

								<a href="#" class=" col-xs-8 blackoutPictureLink typeBlock_300x335">
									<figure>
												<div class="blockPictureDate">
													<img src="http://placehold.it/650x335" class="img-responsive">
													<p class="blockHiddenDate">20 декабря, 12:02</p>
												</div>
											<figcaption>
												<h5 class="cardHearedH5">FASHION&BEAUTY</h5>
												<p class="paragraf">Сколько денег и времени тратится на то, что в итоге не оправдывает себя…</p>
											</figcaption>

									</figure>
								</a>

								<a href="#" class="col-xs-4 blackoutPictureLink typeBlock_300x335">
									<figure>
												<div class="blockPictureDate">
													<img src="http://placehold.it/310x335" class="img-responsive">
													<p class="blockHiddenDate">20 декабря, 12:02</p>
												</div>
											<figcaption>
												<h5 class="cardHearedH5">CITY NEWS</h5>
												<p class="paragraf">Как влиться в яхтную тусовку столицы</p>
											</figcaption>
									</figure>
								</a>

			<div class="col-xs-12 item-lastBtnBlk">
				<a href="#" class="cityGuideMoreViewBtn ind-lastBtn">
					<span class="MoreViewBtnPar">ЕЩЕ СТАТЬИ</span>
					<span  class="MoreViewBtPoint"></span>
					<span class="MoreViewBtPoint"></span>
					<span class="MoreViewBtPoint"></span>

				</a>
			</div>
	</div>

</div>

<?php get_footer(); ?>