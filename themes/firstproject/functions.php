<?php
function br_theme_setup() { // Устанавливаем настройки темы
    add_theme_support('post-thumbnails', array('post', 'page', 'places', 'review'));
    add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));
}
add_action('after_setup_theme', 'br_theme_setup');

function add_theme_scripts() { // Подключаем стили и скрипты темы
    global $MAIN_CATEGORY;
    $template_directory_uri = preg_replace("/http:|https:/", "", get_template_directory_uri());
    $stylesheet_uri = preg_replace("/http:|https:/", "", get_stylesheet_uri());
    
    wp_enqueue_style('bootstrap', $template_directory_uri.'/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-theme', $template_directory_uri.'/css/bootstrap-theme.min.css');
    wp_enqueue_style('et-googleFonts', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext');
    
    wp_enqueue_style('selectize', $template_directory_uri.'/css/selectize.default.css');
    wp_enqueue_style('font-awesome', $template_directory_uri. '/css/font-awesome.min.css');
    wp_enqueue_style('style',  $stylesheet_uri);
    wp_enqueue_script('bootstrap-script', $template_directory_uri.'/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('selectize-script', $template_directory_uri.'/js/selectize.min.js', array('jquery'));
    wp_enqueue_script('theme-script', $template_directory_uri. '/js/index.js', array('jquery'));
    wp_enqueue_script('theme-functions-script', $template_directory_uri. '/js/functions.js');
    wp_enqueue_script('mobile-menu', $template_directory_uri.'/js/mobile_head_menu.js', array('jquery'));
    
    if ( is_single() )
    {
      wp_enqueue_style('owl-carousel', $template_directory_uri.'/css/owl.carousel.min.css');
      wp_enqueue_style('owl-carousel-theme', $template_directory_uri.'/css/owl.theme.default.min.css');
      wp_enqueue_style('pgwslideshow', $template_directory_uri.'/css/pgwslideshow.min.css');
      wp_enqueue_style('pgwslideshow-light', $template_directory_uri.'/css/pgwslideshow_light.min.css');
      wp_enqueue_script('owl-carousel-script', $template_directory_uri.'/js/owl.carousel.min.js', array('jquery'), '1', true);
      wp_enqueue_script('pgwslideshow-script', $template_directory_uri.'/js/pgwslideshow.js', array('jquery'));

      wp_enqueue_script('google-maps-script', $template_directory_uri.'/js/googlemaps.js', array(), '1', true);
      wp_enqueue_script('init-google-maps-script', "https://maps.googleapis.com/maps/api/js?key=AIzaSyBQomAbuKfYjPuOwyqJbyxQAAhbY98I5h8&callback=initMap", array(), '1', true);
      wp_enqueue_script('theme-single-post-script', $template_directory_uri. '/js/single_post.js', array(), '1', true);

    }
    if ( is_search() )
    {
      wp_enqueue_script('theme-search-script', $template_directory_uri. '/js/search.js', array('jquery'));
      wp_enqueue_script('google-maps-script', $template_directory_uri.'/js/googlemaps.js', array(), '1', true);
      wp_enqueue_script('google-maps-markerclusterer-script', $template_directory_uri.'/js/markerclusterer.js', array(), '1', true);
      wp_enqueue_script('init-google-maps-script', "https://maps.googleapis.com/maps/api/js?key=AIzaSyBQomAbuKfYjPuOwyqJbyxQAAhbY98I5h8&callback=initMap", array(), '1', true);
    }

    if ( is_category( $MAIN_CATEGORY ) )
    {
      wp_enqueue_script('theme-main-places-script', $template_directory_uri. '/js/main_places_cat.js', array('jquery'));
      wp_enqueue_script('google-maps-script', $template_directory_uri.'/js/googlemaps.js', array(), '1', true);
      wp_enqueue_script('google-maps-markerclusterer-script', $template_directory_uri.'/js/markerclusterer.js', array(), '1', true);
      wp_enqueue_script('init-google-maps-script', "https://maps.googleapis.com/maps/api/js?key=AIzaSyBQomAbuKfYjPuOwyqJbyxQAAhbY98I5h8&callback=initMap", array(), '1', true);
    }

    if ( is_category( 'city-guide' ) )
      wp_enqueue_script('theme-page-city-guide-script', $template_directory_uri. '/js/page-city-guide.js', array('jquery'));

    if ( is_category( 'city-guide' ) && strpos($_SERVER['QUERY_STRING'], 'map') !== false)
    {
      wp_enqueue_script('google-maps-script', $template_directory_uri.'/js/googlemaps.js', array(), '1', true);
      wp_enqueue_script('google-maps-markerclusterer-script', $template_directory_uri.'/js/markerclusterer.js', array(), '1', true);
      wp_enqueue_script('init-google-maps-script', "https://maps.googleapis.com/maps/api/js?key=AIzaSyBQomAbuKfYjPuOwyqJbyxQAAhbY98I5h8&callback=initMap", array(), '1', true);
    }

    if ( is_page('dobavit-zavedenie') )
      wp_enqueue_script('theme-add-place-script', $template_directory_uri. '/js/add_place.js', array('jquery'));    

    // if( is_singular( 'post') )
    //   wp_enqueue_script('theme-single-post-script', $template_directory_uri. '/js/single_post.js');
    
    // if( is_singular( 'places') )
    // wp_enqueue_script('theme-single-post-script', $template_directory_uri. '/js/single_post.js');


    wp_localize_script('theme-script', 'ajax_request_object', array( 
          'ajaxurl' => admin_url('admin-ajax.php'),
          'redirecturl' => home_url(),
          'loadingmessage' => __('Обработка запроса...')
      ));
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

// Если пользователь не администратор отключаем панель инструментов (консоль)
  function is_user_role( $role, $user_id = null )
  {
    $user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();

    if( ! $user )
      return false;

    return in_array( $role, (array) $user->roles );
  }

  if( !is_user_role( 'administrator' ) )
    show_admin_bar( false );
 
// Нужно для юзеров, которые скопированны с старой базы данных
  add_filter( 'check_password', 'my_pass', 10, 4);
  function my_pass($check, $password, $hash, $user_id)
  { 
   if ( !$check )
   {
      $check = hash_equals( $hash, md5(md5( $password )) );

      if ( $check && $user_id ) {
        wp_hash_password($password);
      }
   }

    return $check;
  }

// Регистрируем типы постов
  add_action( 'init', 'register_post_type_review' );
  function register_post_type_review()
  {
    $labels =array
    (
      'name'                => 'Обзоры',
      'singular_name'       => 'Обзор',
      'add_new_item'        => 'Добавить новое обзор',
      'edit_item'           => 'Изменить обзор',
      'new_item'            => 'Новое обзор',
      'all_items'           => 'Все обзоры',
      'view_item'           => 'Смотреть обзор',
      'search_items'        => 'Найти обзоры',
      'not_found'           => 'Обзоры не найдены',
      'not_found_in_trash'  => 'Обзоры не найдены в карзине',
      'parent_item_colon'   => '',
      'menu_name'           => 'Обзоры'
    );

    $args = array
    (
      'labels'        => $labels,
      'public'        => true,
      'has_archive'   => true,
      'hierarchical'  => true,
      'taxonomies'    => array('category'),
      'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'comments'),
      'menu_position' => 4
    );

    register_post_type('review', $args);
  }

// Подключаем поддержку меню
  if (function_exists('add_theme_support')) {
  add_theme_support('menus');
  }

  register_nav_menus( array(
    'top_menu' => 'Меню сверху'
  ) );

// Перелинковка для записей из старой базы данных
  add_action('init', 'do_rewrite');
  function do_rewrite(){
    // Правило перезаписи
    add_rewrite_rule( 'kiev/restorany/(.+?)(?:/([0-9]+))?/?$',                                  'index.php?places=$matches[1]&page=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/(.+?)/(feed|rdf|rss|rss2|atom)/?$',                       'index.php?places=$matches[1]&feed=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/(.+?)/(.+?)/comment-page-([0-9]{1,})/?$',                 'index.php?places=$matches[1]&cpage=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/(.+?)/embed/?$',                                          'index.php?places=$matches[1]&embed=true', 'top' );
    add_rewrite_rule( 'kiev/restorany/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$',                  'index.php?places=$matches[1]&feed=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/(.+?)/page/?([0-9]{1,})/?$',                              'index.php?places=$matches[1]&cpage=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/(.+?)/trackback/?$',                                      'index.php?places=$matches[1]&tb=1', 'top' );
    add_rewrite_rule( 'kiev/restorany/(feed|rdf|rss|rss2|atom)/?$',                             'index.php?post_type=places$feed=matches[1]', 'top' );
    add_rewrite_rule( 'kiev/restorany/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$',      'index.php?attachment=matches[1]&feed=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/.+?/attachment/([^/]+)/?$',                               'index.php?attachment=matches[1]', 'top' );
    add_rewrite_rule( 'kiev/restorany/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$',      'index.php?attachment=matches[1]&cpage=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/.+?/attachment/([^/]+)/embed/?$',                         'index.php?attachment=matches[1]&embed=true', 'top' );
    add_rewrite_rule( 'kiev/restorany/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$', 'index.php?attachment=matches[1]&feed=$matches[2]', 'top' );
    add_rewrite_rule( 'kiev/restorany/.+?/attachment/([^/]+)/trackback/?$',                     'index.php?attachment=matches[1]&tb=1', 'top' );
    add_rewrite_rule( 'kiev/restorany/?$',                                                      'index.php?post_type=places', 'top' );
    add_rewrite_rule( 'kiev/restorany/feed/(feed|rdf|rss|rss2|atom)/?$',                        'index.php?post_type=places&feed=$matches[1]', 'top' );
    add_rewrite_rule( 'kiev/restorany/page/([0-9]{1,})/?$',                                     'index.php?post_type=places&paged=$matches[1]', 'top' );
  }

// Файл функций ajax запросов клиента
  include 'php/functions/ajax_request.php';

// Файл основных ф-ций разработаных для gvult
  include 'php/functions/functions_gvult.php';

// Файл функций описывающих карточки
  include 'php/functions/functions_cards.php';

// Файл функций-фильтров
  include 'php/functions/functions_filters.php';

// Файл функций shortcodes
  include 'php/functions/shortcodes.php';


function insertThumbnailRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
get_the_post_thumbnail(array(150,100));
$content = '<div style="float: left; margin: 0px 10px 5px 0px;">'. get_the_post_thumbnail( $post->ID, 'thumbnail' ) . '</div>' . $content;
}
return $content;
}
add_filter('the_excerpt_rss', 'insertThumbnailRSS');
add_filter('the_content_feed', 'insertThumbnailRSS');