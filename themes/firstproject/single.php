<?php

get_header();

?>

<div class="container-fluid">
	<div class="row itemrow itemrow-mainBanr">
		<div class="col-sm-12">
			<div class="col-sm-12 itembig-imageblock" style="background-image: url(<?php echo get_the_post_thumbnail_url();?>); ">
			
				<div>
					<span class="itemimage-par1"><?php the_category(', '); ?></span>
				 	<span class="itemimage-par1"><?php echo get_the_date( "j F Y, H:i", $post->ID);?></span>
				</div>
				
				<p class="itemimage-par2"><?php the_title(); ?></p>
				<div class="itemimage-par3">
					

					<span class="itemimage-par3-icon1"><?php echo ss_getPostViews(get_the_ID()); ?></span>

					<span class="itemimage-par3-icon2"><?php 
					 $args = array(
					      'post_id' => $post->ID,
					        'count' => true
					      );
					  echo  $comments = get_comments($args);?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row itemrow">
		<div class="col-sm-8">
			<div class="item-breadCrumbs">
				 <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			</div>
			<article class="item-article">
				<?php
					while( have_posts() ) : the_post();
						the_content();
					endwhile;
				?> 
			</article>
			<div class="row item-phone_socblock">
				<div  class="col-sm-12 restBtnSclBlock">
					<!-- <a href="#" class="btn-small-blue restSocialBtn fbBtnBlock">
						<img src="<?php bloginfo('template_url'); ?>/img/f-b-share.png">
						<span>Share</span>
					</a> -->
					<div class="col-sm-6 fb-btn-block">
						<div id="shareFBBtn" class="btn_share_fb" data-url="<?php echo get_permalink($post); ?>">Share</div>
			 		</div>
			 		<div class="col-sm-6 tw-btn-block">
			 			<a class="btn_share_tw" href="https://twitter.com/share?
						  url=https%3A%2F%2Fdev.twitter.com%2Fweb%2Ftweet-button&
						  via=twitterdev&
						  related=twitterapi%2Ctwitter&
						  hashtags=example%2Cdemo&
						  text=custom%20share%20text">
						Tweet
						</a>
						
					</div>
					<!-- <a href="#" class="btn-small-blue restSocialBtn TwBtnBlock">
						<img src="<?php bloginfo('template_url'); ?>/img/fill-1.png">
						<span>Tweet</span>
					</a> -->
				</div>
				<div class="col-sm-6 restCard1">
					<a href="#">
						<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages" alt="adwert">
					</a>
				</div>
				<div class="col-sm-6 restCard1">
					<a href="#">
						<img src="http://lorempixel.com/300/250" class="img-responsive categoryImages" alt="adwert">
					</a>
				</div>
			</div>
			<div class="row item_comment_row">
				<div class="col-sm-12">
					<form>
						<div class="form-group">
							<label for="comment" class="leaveCommentParagraf">Оставить комментарий</label>
							<textarea id = "item-textarea" class="form-control" rows="6"></textarea>
							<div class="col-sm-8 padding_zero item-send-comment">
								<a href="" class="btn-small-blue restSendCommBtn" data-postid="<?php echo $post->ID ?>">ОТПРАВИТЬ</a>
							</div>
						</div>
					</form>
					<div class="restPeopleComments" data-postid="<?php echo $post->ID ?>">
						<?php
						$args = array(
							'post__in'	=> $post->ID,
							'number'	=> 10,	
							'parent'	=> 0,
							'hierarchical'	=> false
						);
						
						$comments = get_comments($args);
									
						get_the_comment_life($comments)
						?>

						<!-- <div class="col-sm-12 padding_zero restCommDeleted">
							<a href="#"><span>ПОКАЗАТЬ ВСЕ КОММЕНТАРИИ</span></a>
						</div> -->
					</div>
				</div>
				<div class="row item-readmore-block">
					<div class="col-sm-12">
						<p class="item-readmoreHead">Читайте далее</p>
						<?php

						if(get_the_category( $post->ID )[0]->parent != 0){
							$postParent = get_the_category( $post->ID )[0]->parent;
						}
						else{
							$postParent = get_the_category( $post->ID )[0]->cat_ID;
						}
						$args = array
						(
							'posts_per_page'	=> 5,
							'offset'			=> 50,
							'post_type'			=> array('post'),
							'cat'				=> $postParent
						);

						$query = new WP_Query($args);
						while ( $query->have_posts() ) {
							$query->the_post();

							?> 
							<a href="<?php echo get_post_permalink(); ?>"><p class="item-readmore"><?php the_title();?></p></a>
						<?php
						}?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 item-bestofweak">
				<div class="blockPictureDate">
					<img src="http://placehold.it/300x600" class="img-responsive categoryImagesNews">
				</div>
				<?php
					if(get_the_category( $post->ID )[0]->parent != 0){
								$postParent = get_the_category( $post->ID )[0]->parent;
					}
					else{
						$postParent = get_the_category( $post->ID )[0]->cat_ID;
					}
					$args = array(
								'posts_per_page'	=> 1,
								'post_type'			=> array('post'),
								'cat'				=> $postParent*(-1)
							);
					$query = new WP_Query($args);
					$count = 0;
					while ( $query->have_posts())
					{
						$query->the_post();
							echo '<div class="blackoutPictureLink typeBlock_300x335 item-sndCard300x335 item-phone-card-delete">';
							echo getCardSimpleSingle($post);
							echo '</div>';
							$count++;
					}
					wp_reset_postdata();
				?>
				<div class="item-bstWeek">
					<p class="item-bstHead">Лучшее за неделю</p>
					<?php
						$args = array
								(
									'posts_per_page'	=> 7,
									'post_type'			=> array('post'),
									'cat'				=> array('news', 'news-2')
								);
						$count = 0;
						$query = new WP_Query($args);
						while ( $query->have_posts())
						{
						$query->the_post();?>			
							<a href="<?php echo get_the_permalink();?>" class="item-bstWkBlock">
								<div class="item-bstWk-img">
									<img src="<?php echo get_the_post_thumbnail_url() ; ?>" alt="best of week">
								</div>
								<div class="item-bstWk-descr">
									<p class="item-bstWk-par"><?php the_title(); ?></p>
									<div>
										<img src="<?php echo get_template_directory_uri(); ?>/img/eye.png" class="item-bstWk-eye" alt="eye">
										<span class="item-countviews"><?php echo get_post_meta($post->ID, 'post_views_count', true); ?></span>
									</div>
								</div>
							</a>
						<?php
						$count++;
						}
						wp_reset_postdata();?>
				</div>
				<?php include 'php/slider-news.php';?>
				<div class="item-thirdbanner col-sm-12 padding_zero">
					<img src="http://placehold.it/300x600" class="img-responsive categoryImagesNews">
				</div>
		</div>
	</div>
	<div class="row itemrow item-cards-block">
			<?php
				$args = array
					(
						'posts_per_page'	=> 5,
						'offset'					=> 50,
						'post_type'				=> array('post', 'places')
					);

				$query = new WP_Query($args);
				
				$count = 0;
				while ( $query->have_posts() )
				{
					$query->the_post();
					if( $count == 2 || $count == 4){
						echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink phone_card_delete typeBlock_300x335">';
						echo getCardSimpleSingle($post);
						echo '</div>';
					}					
					elseif ( $count != 3 )
					{
						echo '<div class="col-sm-4 col-xs-12 blackoutPictureLink  typeBlock_300x335">';
						echo getCardSimpleSingle($post);
						echo '</div>';
					}
					
					else
					{
						echo '<div class="col-sm-8 blackoutPictureLink phone_card_delete typeBlock_300x335">';
						echo getCardSimpleDouble($post);
						echo '</div>';
					}
					
					$count++;
				}
				
				wp_reset_postdata();
			?>
			<div id="wr_btnMorePosts" class="col-sm-12 item-lastBtnBlk">
				<?php include 'php/btn_load_more_posts.php';?>
			</div>
		</div>
</div>
<?php
	get_footer();
?>