///////////////  Страница search.php ////////////////////////
jQuery( document ).ready(function($)
{
	console.log( "File 'search.js' is started!" );

	// Слушаем поле ввода поисковой фразы
		$('#search_string').on('input', function()
		{
			if (!this.value || this.value[0] == ' ')
			{
				$('#btn_search_page').addClass('noactive')
				return
			}

				$('#btn_search_page').removeClass('noactive')
		})

	//Активация меню параметров расширенного поиска
		$('#btn_open_menu_advance_search').on('click', function(event)
		{
			$(this).css('opacity', '0').slideUp(1000)
			
			$(advance_param_search).slideDown(
				1000, 
				function()
				{
					$(this).css('top', '0')
				}
			)

			$('.rst-param-block').hide()

			if ( location.search.indexOf('cat') )
				get_sub_category(main_category, true)
			
			event.preventDefault()
		})

		$('#btn_search_page').on('click', function()
        {
            location = location.origin + '?s=' + $('#search_string').attr('value')
        })

	// Выбираем условие сортировки
		$('.sort_field').on('click', function(event)
		{
			if ( $(this).hasClass('active') )
				$(this).removeClass('active')
			else
			{
				$(".sort_field").removeClass('active')
				$(this).addClass('active')	
			}

			event.preventDefault()
		})

	// Выбираем средний чек
		$(".average_checks").on('click', function()
		{
			if ( $(this).hasClass('active') )
				$(this).removeClass('active')
			else
			{
				$(".average_checks").removeClass('active')
				$(this).addClass('active')	
			}	
			
			console.log(this.parentElement)
		})

	// Слушаем поля выбора
		$('.input_list').on('click', function(event)
		{
			if ( $("ul.active").length )
			{
				closeInputField()
				return
			}

			$(this).parent().addClass('active')

			var container = $(this).parent()
			var speedShow = 400

			$("ul.active").removeClass('active').slideUp(speedShow)
			container.children('ul').addClass('active').slideDown(speedShow)

			container.find('li').on('click', function(event)
			{
				if ( !this.dataset.value )
				{
					container.children('.input_list').attr(
					{
						value: '',
						'data-value': '',
						'data-id': '',
						'data-min': ''
					})

					$('#sub_category').next().remove()
				}
				else
				{
					container.children('.input_list').attr(
					{
						value: this.innerHTML,
						'data-value':  this.dataset.value,
						'data-id': this.dataset.id ? this.dataset.id : '',
						'data-min': this.dataset.min ? this.dataset.min : ''
					})

					get_sub_category(this, true)
				}
				
			
				$("ul.active").removeClass('active').slideUp(speedShow)

				$(this).off('click')

				event.preventDefault();
			})


			$('body').on('click', closeInputField)

			event.stopPropagation()
		})		
			
	// Кнопка расширенного поиска
        $(btn_additional_search).on('click', function()
        {
        	var link
        	var strSearch = $('#search_string').val()
        	var orderby = $('.sort_field.active').attr('data-sort')
        	        		
        	var meta = (orderby !== undefined && orderby != 'date' 
        	        		? 'rating_total'
        	        		: '')

        	var cat  = $('#main_category').attr('data-id')
        	var subcat = $('#sub_category').attr('data-id')
        	
        	var district = $('#input_district').val()
        	var subway = $('#subway').val()
        	var address = $('#input_address').val()
        	var openig = $('#checkbox_place_open').attr('data-checked')
        	var check = $('.average_checks.active').attr('data-min')
			//console.log($('#checkbox_place_open').checked)
        	var filterField = 
        	{
        		s			: strSearch,
        		orderby 	: orderby,
        		meta	  	: meta,
        		cat 		: cat,
        		subcat		: subcat,
        		district	: district,
        		subway	 	: subway,
        		address 	: address,
				openig		: openig,
				check 		: check        		
        	}

        	if ( filterField.s )
				link = location.origin + '?'
			else
				link = location.origin + '?s=&'

        	for ( var key in filterField )
        	{
        		if ( filterField[key] )
        			link += key + '=' + filterField[key] + '&'
        	}

        	link = link.slice(0, -1)

        	location = link
        	// console.log(link)
        	// console.log(filterField)
        })

		$('#checkbox_place_open').on('change', function()
		{
			if ( !this.dataset.checked )
				this.dataset.checked = 'true'
			else
				this.dataset.checked = 'false'
		})

});