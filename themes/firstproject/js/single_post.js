jQuery( document ).ready(function($)
{
	console.log( "File 'single_post.js' is started!" );

	//Функция отрисовки комментрария
	function get_comment(respond){	
			var comment;
			comment = $('<div class="col-sm-11 padding_zero restCommentBlock" data-idcommentblock="' + respond.comment_ID + '">');
			
			comment.append('<div class="col-sm-1 padding_zero comment_avatar_block">');
			comment.append('<div class="col-sm-11 padding_zero restCommBody">');

			$(comment.children('.comment_avatar_block')).append('<img src="' + respond.comment_author_ava + '" class="restMemberAvatar">');
			$(comment.children('.restCommBody')).append('<div class="comment_header">', '<div class="restCommText">', '<div class="restCommStatus answer_comment_block">');
			$(comment.find('.comment_header')).append('<p class="restRewNickname">'+ respond.comment_author +'</p>',
			 												'<p class="restCommDate">' + respond.comment_date + '</p>',
															'<div class="restCommRating">');

			if(respond.comment_approved == 1){
					$(comment.find('.restCommText')).append('<p>'+ respond.comment_content +'</p>');
				}
				else if(respond.comment_approved == 0){
					$(comment.find('.restCommText')).append('<div class="restCommStatus comment_wait_moder">');
					$(comment.find('.comment_wait_moder')).append('<p class="restCommWait">ОЖИДАЕТ МОДЕРАЦИИ</p>');
				}

			$(comment.find('.answer_comment_block')).append('<span data-idcomment="<?php echo $comment->comment_ID; ?>">ОТВЕТИТЬ</span>');
			$('.restPeopleComments').prepend(comment)
			
	}

//Функция отправки и добавления комментария
	$('.restSendCommBtn').on("click",function(event){
		var commentContent = $("#item-textarea").val();
		var postId = $('.restSendCommBtn').data('postid');
		var date = new Date().toString('dd MMM yyyy HH:mm');


	    $.ajax({
	        url: ajax_request_object.ajaxurl,
	        type: 'POST',
	        data: {
	        	commentContent: commentContent,
	        	postID: postId,
	        	commentDate: date,
	        	action: 'ajaxcomment'
	        },
	        dataType: 'json',
	        success: function (respond, textStatus, jqXHR ){
	 			
	 			get_comment(respond);
	 		}

	    });	
	    event.preventDefault();
   });

// Устанавливает класс для рейтинга комментария 
	function change_rating_class(element)
	{
		element.removeClass('ratingNull negativeRating positiveRating');

		if( element.text() > 0 )
			element.addClass('positiveRating');
		else if( element.text() < 0 )
			element.addClass('negativeRating');
		else if( element.text() == 0 )
			element.addClass('ratingNull');
	}

// Проверка на регистрацию пользователя при голосовании в комментариях
	function is_user_auth(element){
		var ratingBlock = $(element).parents('.restCommRating');

		if(ratingBlock.hasClass('user-logout')){
			ratingBlock.prepend('<span class="warning-not-logged">Авторизируйтесь на сайте</span>');
			setTimeout(function(){$('.warning-not-logged').fadeOut('500')}, 1000);
			return false;
		}

		return true
	}


//Функция - минус к рейтингу комментария
	$('.minus_vote_rating').on("click",function(event){

		if ( !is_user_auth(this) )
			return

		var commentBlock = $(this).parents('.restCommentBlock');
		var commentID = commentBlock.data('idcommentblock');
		
		var userID = $(this).parents('.restCommRating').data('userid');
		
		var number = $(this).next();
		number.text(Number(number.text()) - 1);

		change_rating_class(number);


		$(this)
			.parents(".restCommRating")
			.find(".plus_vote_rating")
			.fadeOut( '200');
		$(this).fadeOut( '200');
		

			$.ajax({
	        url: ajax_request_object.ajaxurl,
	        type: 'POST',
	        data: {
	        	commentID: commentID,
	        	commentKarma: number.text(),
	        	userID: userID,
	        	action: 'ajaxcommentratingminus'
	        },
	        dataType: 'json',
	        success: function (respond, textStatus, jqXHR ){
	 			
	 		}
	    	});	
	 		
	    $(this).off('click');
	});


	
//Функция - плюс к рейтингу комментария
	$('.plus_vote_rating').on("click",function(event){
		if ( !is_user_auth(this) )
			return
		
		var commentBlock = $(this).parents('.restCommentBlock');
		var commentID = commentBlock.data('idcommentblock');
		var userID = $('.comment_header').find('.user-logged').data('userid');
		console.log(userID);
		var number = $(this).prev();
		number.text(Number(number.text()) + 1);

		change_rating_class(number);

		$(this)
			.parents(".restCommRating")
			.find(".minus_vote_rating")
			.fadeOut( '200');
		
		$(this).fadeOut( '200');

			$.ajax({
	        url: ajax_request_object.ajaxurl,
	        type: 'POST',
	        data: {
	        	commentID: commentID,
	        	commentKarma: number.text(),
	        	userID: userID,
	        	action: 'ajaxcommentratingplus'
	        },
	        dataType: 'json',
	        success: function (respond, textStatus, jqXHR ){
	 			console.log(respond);
	 		}
	    	});	
	 	
	    $(this).off('click');
	});


//Функция отрисовки комментрария
	function get_answer(respond){	
		var comment = $('[data-idcommentblock="' + respond.commentID + '"]');
		var answer = $('<div class="answer_block col-sm-11">');
		comment.after(answer);
		
		console.log(respond);


		
		answer.append('<div class="col-sm-1 padding_zero comment_avatar_block">');
		answer.append('<div class="col-sm-11 padding_zero restCommBody">');

		$(answer.children('.comment_avatar_block')).append('<img src="' + respond.comment_author_ava + '" class="restMemberAvatar">');
		$(answer.children('.restCommBody')).append('<div class="comment_header">', '<div class="restCommText">', '<div class="restCommStatus answer_comment_block" data-idcomment="'+respond.commentID+'">');
		$(answer.find('.comment_header')).append('<p class="restRewNickname">'+ respond.answerUser +'</p>',
														'<p class="restCommDate">' + respond.date + '</p>',
														'<div class="restCommRating">');
		if(respond.comment_approved == 1){
		 		$(answer.find('.restCommText')).append('<p>'+ respond.commentContent +'</p>');
		 	}
		 	else if(respond.comment_approved == 0){
				$(answer.find('.restCommText')).append('<div class="restCommStatus comment_wait_moder">');
		 		$(answer.find('.comment_wait_moder')).append('<p class="restCommWait">ОЖИДАЕТ МОДЕРАЦИИ</p>');
		 	}

		
		// answer.find('.answer_comment_block').append('<span class="btn_answer_comment">ОТВЕТИТЬ</span>');
			
	}



	$('.btn_answer_comment').on('click', function(event){
		$(this).before('<form class="answer_comment_form"></form>');
		$(this).prev().append('<textarea class="form-control" rows="4" id="answer_textarea">');
		$(this).off('click');

		$(this).on('click', function(event){
			var commentContent = $(this).parents('.answer_comment_block').find('#answer_textarea').val();

			$(this).prev().remove();
			var date = new Date().toString('dd MMM yyyy HH:mm');

			$.ajax({
		  	    url: ajax_request_object.ajaxurl,
		        type: 'POST',
		        data: {
		        action: 'addcommentanswer',
		      
		        commentID: $(this).parents('.answer_comment_block').data('idcomment'),
		        commentContent: commentContent,
	        	postID: $(this).parents('.restPeopleComments').data('postid'),
	        	commentDate: date,
	        	answerUser: $(this).parents('.restCommentBlock').find('.restCommRating').data('userid'),
	        	date: date
		        },
		        dataType: 'json',
		        success: function (respond, textStatus, jqXHR ){
		 			//answerBlock.find('#answer_textarea').remove();
		 			get_answer(respond);
		 		}
   		 	});
		});
	});


// Функция меняет цвет звездочек в рейтинге при наведенни мышки
	$('.rating-block span').on('mouseover', function(e)
    {	
	
       var spans = this.parentNode.querySelectorAll('span')
       var level = this.dataset.level
       for (var i = 0; i < level; i++)
            $(spans[i]).addClass('single-gold-star');
        	$(spans[i]-1).removeClass('single-grey-star');
        	$(spans[level]).addClass('single-blue-star');
       e.preventDefault()
    })

// Функция возращает цвет звездочек при отведении мышки с блока
   $('.rating-block').on('mouseout', function(e)
    {
       	var stars = $(this).find('span');
    	var rat = $('.rest_rating_count').text();
    	
        for (var i = 0; i < 5; i++){
    		if(i*2 <= rat ){

    			$(stars[i]).removeClass('single-blue-star');
       			$(stars[i]).removeClass('single-grey-star');
       			$(stars[i]).addClass('single-gold-star');
    		}
    		else{
    			$(stars[i]).removeClass('single-blue-star');
       			$(stars[i]).removeClass('single-gold-star');
       			$(stars[i]).addClass('single-grey-star');
    		}
    	}

       e.preventDefault();
    });

//Функция обновления рейтинга заведения при клике
function update_rating(averRatingUp){
	var rating = $('.rating-block');
	var upRatingBlock = rating.find('.rest_rating_count');
	upRatingBlock.text(averRatingUp);
}

//Функция проверки юзера при клике на рейтинг заведения
function is_user_auth_rating(respond, userId){
	
	if(userId == 0){
		var rating = $('.rating-block');
		rating.prepend('<p class="auth_rating_alert">Авторизируйтесь на сайте</p>');
		setTimeout(function(){$('.auth_rating_alert').fadeOut('1000')}, 1000);
	}
	else{
		if(respond.is_user_voted == true){
			var rating = $('.rating-block');
			rating.prepend('<p class="auth_rating_alert">Вы уже голосовали</p>');
			setTimeout(function(){$('.auth_rating_alert').fadeOut('1000')}, 1000);
		}
	}
	
}

//Отрисовывает заглушку на блоке райтинга при клике, если пользователь не залогинен
	$('#rating-block-id-logout').on('click', function(e){
		var rating = $('.rating-block');
		rating.prepend('<p class="auth_rating_alert">Авторизируйтесь на сайте</p>');
		setTimeout(function(){$('.auth_rating_alert').fadeOut('1000')}, 1000);
	});

//Отрисовывает заглушку на блоке райтинга при клике, если пользователь уже голосовал
	$('#rating-block-id-voted').on('click', function(e){
		var rating = $('.rating-block');
		rating.prepend('<p class="auth_rating_alert">Вы уже голосовали</p>');
		setTimeout(function(){$('.auth_rating_alert').fadeOut('1000')}, 1000);
	});


//Функция обновления рейтинга заведения
	$('#rating-block-id span').on('click', function(e)
	{	
	    var star = $(this)[0];
	    var rating = star.dataset.level*2;

	   	var userId = $('.rating-block').data('userRatingid')
	   	var postId = $('.rating-block').data('postRatingid')


	   	var avarageRating = $('.rest_rating_count').data('ratingAvarege');
	   	var usersCount	= $('.rest_rating_count').data('postPollCount');

	 	totalRat = avarageRating*usersCount + rating;
	   	usersCount = usersCount + 1;

	   	ratUpdateFinal = (totalRat/usersCount).toFixed(1);
	   	
	   	update_rating(ratUpdateFinal);
	   	
	   	$.ajax({
		  	    url: ajax_request_object.ajaxurl,
		        type: 'POST',
		        data: {
		        action: 'addplacerating',
			        rating: rating,
			        userId: userId,
			        postId: postId
		        },
		        dataType: 'json',
		        success: function (respond, textStatus, jqXHR ){
		 			console.log(respond);
		 			is_user_auth_rating(respond, userId);
		 		}
			 	});
	   e.preventDefault();
	   $(this).off('click');
	});
});	


document.getElementById('shareFBBtn').onclick = function() {
	var url = this.dataset.url

	FB.ui(
	{
		method: 'share',
		display: 'popup',
		href: url,
	}, 
	function(response){});
}