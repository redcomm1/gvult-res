///////////////  Страница add-place.js ////////////////////////
console.log( "File 'googlemaps.js' is started!" );

function setIconMarker(link)
{
	var styles = []
	var sizes = [53, 56, 66, 78, 90]
	for ( i = 0; i < 5; i++ )
	{
		styles[i] = 
		{
			'url': link.imgPath + 'm' + (i + 1) + '.png',
			'height': sizes[i],
			'width': sizes[i],
			'textColor': 'white'
		}
	}

	return styles
}

function setDataMap(data)
{
	var containerMap = document.getElementById('map')
	var dataLength = data.length, coordinatesLenght = 0, count = 0
	var i, j, markers = [];
				       
	var map = new google.maps.Map(containerMap, {
      zoom: 10,
      center: {lat:50.435,lng:30.55},
      disableDefaultUI: true
    })

    var infoWindow = new google.maps.InfoWindow(
	{
		content: ''
	})

	for ( i = 0;  i < dataLength; i++ )
	{
		coordinatesLenght = data[i].coordinates.length
		for ( j = 0; j < coordinatesLenght; j++)
		{
			markers[count] = new google.maps.Marker({
				map: 			map,
	            position: 		data[i].coordinates[j],
	            title: 			data[i].title[j],
	            icon: 			data[i].imgPath + 'map-pin.png'
	        })

	        markers[count].contentString = '<div class="info_google_place">' +
					'<p class="title_info"><b>' + data[i].title[j] + '</b></p>'
			if ( data[i].address[j] != '' )	
				markers[count].contentString +=	'<p class="address_info">' + data[i].address[j] + '</p>'
			if ( data[i].email[j] != '' )
				markers[count].contentString += '<p class="email_info">' + data[i].email[j] + '</p>'
			if ( data[i].phone[j] != '' )
				markers[count].contentString += '<p class="phone_info">' + data[i].phone[j] + '</p>'
			

	    	markers[count].addListener('click', function()
			{
				infoWindow.setContent(this.contentString)
				infoWindow.open(map, this)
			})

			count++
		}
		 	
	}

	var markerCluster = new MarkerClusterer(map, markers,
	{
		styles: setIconMarker(data[0])
	})
}

sendRequest.index = 0
sendRequest.array = []
function sendRequest( formData)
{
	var arr = []
	var xhr = new XMLHttpRequest()
	xhr.open('POST', ajax_request_object.ajaxurl)

	if (xhr.readyState)
		xhr.send(formData)

	xhr.onreadystatechange = function()
	{
		if ( this.readyState != 4 ) return;

		if ( this.status == 200 )
		{
			var data = JSON.parse(this.responseText)
        	// var responseText = this.responseText
        	// console.log(data)

        	sendRequest.array[sendRequest.index++] = data

        	if ( data.nextRequest )
        	{
        		formData.append('offset', data.offset)
        		sendRequest( formData )
        	}
        	else
	        	setDataMap(sendRequest.array)
		}
	}
}

function initMap()
{
	var containerMap = document.getElementById('map')
	var postID = [];

	var xhrMap = new XMLHttpRequest()
	var fd = new FormData()

	xhrMap.open('POST', ajax_request_object.ajaxurl)
	
	if ( location.search.indexOf('maps') != -1 )
	{
		fd.append('action', 'placesofcityguide')
		fd.append('query', query_data.dataset.query)
		fd.append('offset', 0)

		sendRequest(fd)
	}
	else if ( location.search.indexOf('map') != -1 )
	{
		fd.append('action', 'placesofcategory')
		fd.append('search_string', location.search)
		fd.append('catid', containerMap.dataset.catid)
		fd.append('offset', 0)

		sendRequest(fd)
	}
	else if ( location.search.indexOf('s=') != -1 )
	{
		jQuery('.post').each( function( index, el )
		{
			postID.push(Number(el.dataset.id))
		})
		fd.append('action', 'searchposts')
		fd.append('postid', postID)
	}
	else
	{
		fd.append('action', 'coordinates')
		fd.append('postid', containerMap.dataset.postid)	
	}


	if (xhrMap.readyState && location.search.indexOf('map') == -1)
	{
	    xhrMap.send(fd)
	    
    	xhrMap.onreadystatechange = function()
		{
			if (this.readyState != 4) 
				return
		
	        if ( this.status == 200 )
	        {
	        	var data = JSON.parse(this.responseText)
	        	//var responseText = this.responseText
	        	console.log(data)

	        	if ( data.coordinates === undefined  || data.coordinates.lat == 0 )
				{
					var map = new google.maps.Map(containerMap, {
		          		zoom: 12,
			        	center: {lat:50.4390397,lng:30.5211747},
			        	disableDefaultUI: true
			        })	

					return
				}

				var contentString = ''
				var infoWindow = ''

				if ( location.search.indexOf('s=') != -1 )
				{
					var coordinatesLenght = data.coordinates.length
					var i, markers = [];
													       
		        	var map = new google.maps.Map(containerMap, {
			          zoom: 10,
			          center: {lat:50.4622432,lng:30.5},
			          disableDefaultUI: true
			        })

			        infoWindow = new google.maps.InfoWindow(
					{
						content: ''
					})

		        	for ( i = 0;  i < coordinatesLenght; i++ )
		        	{
		        		markers[i] = new google.maps.Marker({
		        			map: 			map,
				            position: 		data.coordinates[i],
				            title: 			data.title[i],
				            icon: 			data.imgPath + 'map-pin.png'
				        })

		        		markers[i].contentString = '<div class="info_google_place">' +
		        				'<p class="title_info"><b>' + data.title[i] + '</b></p>'
		        		if ( data.address[i] != '' )	
		        			markers[i].contentString +=	'<p class="address_info">' + data.address[i] + '</p>'
		        		if ( data.email[i] != '' )
		        			markers[i].contentString += '<p class="email_info">' + data.email[i] + '</p>'
		        		if ( data.phone[i] != '' )
		        			markers[i].contentString += '<p class="phone_info">' + data.phone[i] + '</p>'
		        		

				    	markers[i].addListener('click', function()
						{
							infoWindow.setContent(this.contentString)
							infoWindow.open(map, this)
						})    	
		        	}

		        	var markerCluster = new MarkerClusterer(map, markers,
	        		{
	        			styles: setIconMarker(data)
	        		})
				}
				else
				{
				  	var map = new google.maps.Map(containerMap, {
			          zoom: 14,
			          center: data.coordinates,
			          disableDefaultUI: true
			        })

			        var marker = new google.maps.Marker({
			          map: 				map,
			          title: 			data.title,
			          position: 		data.coordinates,
			          icon: 			data.imgPath + 'map-pin.png'
			        })
				}
			}
		}
	}
}

