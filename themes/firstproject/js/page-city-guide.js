jQuery( document ).ready(function($)
{
  console.log( "File 'page-city-guide.js' is started!" );

  var baseLink = location.origin + location.pathname
  var searchLink = location.search

  // функция отправляет запрос на получение топ карточек
  	function ajax_get_top_cards($el)
  	{
  		var query = new XMLHttpRequest()
  		var fd = new FormData()

  		query.open('POST', ajax_request_object.ajaxurl)
  		fd.append('action', 'ajaxtopcards')
  		fd.append('catid', $el.dataset.id)

  		if ( query.readyState )
  		{
  			query.send(fd)

  			query.onreadystatechange = function ()
  			{
  				if ( this.readyState != 4 )
  					return

  				if ( this.status == 200 )
  				{
  					var response = JSON.parse(this.responseText)
			       	console.log(response)
			       	$('.top_cards').remove()
			       	$(container_name_category).find('.active').removeClass('active')
			       	$($el).addClass('active').find('span').addClass('active')
			       	$('#container_top_cards').children('div').before(response)
  				}
  			}
  		}
  	}

  $('[data-page="cityguide"]').on('click', function()
  {
  	if ( this.classList.contains('active') )
  		return

  	ajax_get_top_cards(this)
  })

  // активируем кнопку "Еще комментарии"
	$('#btnMoreComments').on('click', function(event)
	{
		sendDataMoreComments(
		{
			id: this.id,                
			maxcountcomments: this.dataset.maxcountcomments, 
			offset: this.dataset.offset, 
			countcomments: this.dataset.countcomments,
			catid: this.dataset.catid,
			classes: this.dataset.classes
		})

		event.preventDefault();
	})

	// активируем кнопку "Еще Заведения"
		$('#btnMorePlaces').on('click', function(event)
		{ console.log(this)
			sendDataMorePosts(
			{
				id: this.id,                
				maxcountposts: this.dataset.maxcountposts, 
				offset: this.dataset.offset, 
				posttype: this.dataset.posttype,
				search: this.dataset.search,
				type: this.dataset.type,
				countposts: this.dataset.countposts
			})

			event.preventDefault();
		})

	// Активируем переключатель список-карта
		$(switcher_list_map).on('click', function()
		{
			switch_list_map(this)
		})

	////     Режим карта    ////

	// В режиме карта для списка категорий, отменяем действие по-умолчанию,
	// оменеям фильтр данной категории
		$('.fullmapCategoryNames.active')
			.parent()
			.on('click', function(event)
			{
				location = location.origin + location.pathname + '?maps'
				event.preventDefault()
			}) 

	// Активируем кномпку быстрого поиска
		$('#btn_fast_search').on('click', function(event)
		{
			console.log(field_check_all_cat.checked)
			if ( field_check_all_cat.checked )
				location = baseLink + '?maps&allcat&search=' + field_search.value
			else
				location = baseLink + searchLink + '&search=' + field_search.value
			
			event.preventDefault()
		})

	// Активируем кнопку очистить все
		$('#btn_clean_all').on('click', function(event)
		{
			var categoryLink = $('.fullmapCategoryNames.active')
				.removeClass('active')
				.parent()

				categoryLink
				.attr('href', baseLink + '?maps&category=' + categoryLink.attr('data-id'))
				.off('click')

			field_search.value = ''
			field_check_all_cat.checked = false

			event.preventDefault()
		})		
})