jQuery( document ).ready(function($)
{
  console.log( "File 'index.js' is started!" );

    //Ф-ция затемняет экран при нажатии на поиск и/или форму пользователя
	  function disableMainPartWindow(enableScroll = true)
	  {
	  	var objCss = 
	  	{
	  		overflow: 'hidden',
	  		paddingRight: '17px' 
	  	}

			var top = $('.header').height() + $('#wpadminbar').height()
	  	var shadow = '<div class="disable_window" style="top: '+ top +'px"></div>'

			if ( typeof enableScroll != 'boolean' )
	  		enableScroll = true

	  	if ( !enableScroll )
	  	{
	  		objCss.paddingRight = '0px';
	  	}

	  	if ( $('.disable_window').length )
	  		shadow = ''

	  	$('body')
	  		.css(objCss)
	  		.append(shadow)
	  }

	//Ф-ция возвращает экран в исходное положение при нажатии на отмену
	  function enableMainPartWindow()
	  {
	  	var objCss = 
	  	{
	  		overflow: '',
	  		paddingRight: '' 
	  	}

	  	$('body').css(objCss)
	  	$('.wr_header_menu_and_search').css('overflow', '')
	  }

	// Ф-ция закрывает панель входа пользователя
		function closeUserMenu()
		{
			enableMainPartWindow()

			if ($('#login-form').length)
	  		$('#login-form').slideUp(500)

	  	if ($('#reg-form').length)
	  		$('#reg-form').slideUp(500)

	  	if ($('#profile-form').length)
	  		$('#profile-form').slideUp(500)

    	$('.disable_window').remove()
		}

	// Активируем поисковую строку
	  $('#search').click(function()
	  {
	  	disableMainPartWindow()
	  	
		$('.disable_window').click(function()
	    {
	    	enableMainPartWindow()
	  		
	  		//$('#header_search').css("display", "")
	  		$('#header_search').css("display", "none")
	  		$('#header_menu').show(700)
	  		

	    	$(this).remove()
	    })

	  	$('#header_menu').hide(700)
	  	$('#header_search').css("display", "flex")

	  	$('.wr_header_menu_and_search').css('overflow', 'hidden')

	  	event.preventDefault()
	})

// Меню авторизации
	// Активируем панель входа пользователя
		$('#btn_login_header').click(function(event)
		{
			disableMainPartWindow()
			console.log("gggg")
			console.log($('#nicknameUser').length)
			$('.disable_window').click(closeUserMenu)

			if ( $('#nicknameUser').length ){
				$('#profile-form').slideDown(500)
				console.log("TTTT")		}
						
			else{
				$('#login-form').slideDown(500)
				console.log("UUUU")
			}
				

			event.preventDefault()
		})

	// Закрываем панель входа пользователя при нажатии на "стрелку вверх"
		$('.header-close-arrow').click(closeUserMenu)

	// При входе отправляем данные пользователя на сервер
		var $loginSingin = $('[name="login_singin"]'),
				$passSingin = $('[name="pass_singin"]')

		var dataUser = 
		{
			login: '',
			password: ''
		}

		var regExpLogin = /^[а-яА-ЯёЁa-zA-Z0-9]+$/
		var regExpPass = /^[а-яА-ЯёЁa-zA-Z0-9\-\_\.\,\+\=]+$/

		function clearInput() 
		{
			$loginSingin.placeholder = "Логин"
			$passSingin.placeholder = "Пароль"
		}

		function errorInputClear(event)
		{
			var target = event.target

			if (target.value != "") 
			target.classList.remove("error_input")  
		}

		function removeErrorMessage()
		{
			if ( $('.error_message_auth').length )
				$('.error_message_auth').remove()
		}

		document.querySelector('[name="login_singin"]').addEventListener('input', listenLoginSingin, false)

		function listenLoginSingin(event)
		{
			removeErrorMessage()

			errorInputClear(event)

			dataUser.login = event.target.value	
		}

		document.querySelector('[name="pass_singin"]').addEventListener('input', listenPassSingin, false)

		function listenPassSingin(event)
		{
			removeErrorMessage()
			errorInputClear(event) 
			
			dataUser.password = event.target.value
		}

		$('#btn_sign_in').on('click', loginUser)

		function loginUser(event)
		{
			if ( dataUser.login == '' || !regExpLogin.test(dataUser.login) ) 
			{
				$loginSingin.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Вы не ввели логин</p>')
				$loginSingin.val('')
				$loginSingin.attr('placeholder', "Логин")
			}
			else if ( dataUser.password == '' || !regExpPass.test(dataUser.password) ) 
			{
				$passSingin.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Вы не ввели пароль</p>')
				$passSingin.val('')
				$passSingin.attr('placeholder', "Пароль")
			}
			else
			{
				clearInput()
				send_auth_data(dataUser)
			}

			event.preventDefault()
		}

		function send_auth_data(dataUser)
		{
			var ajaxDataAuthenticate = new XMLHttpRequest()
						
			var fd = new FormData()
			
			ajaxDataAuthenticate.open('POST', ajax_request_object.ajaxurl)
			fd.append('action', 'ajaxuploaddataauthenticate')
			fd.append('log', dataUser.login)
			fd.append('pwd', dataUser.password)
			fd.append('rememberme', true)
			fd.append('curUrl', location.href)

			if (ajaxDataAuthenticate.readyState)
			{
      	ajaxDataAuthenticate.send(fd)

        ajaxDataAuthenticate.onreadystatechange = function()
				{
					if (this.readyState != 4) 
						return
				
	        if ( this.status == 200 )
	        {
	        	var responseText = JSON.parse(this.responseText)
	        	//var responseText = this.responseText
	        	//console.log(responseText)
	        	//location = responseText
	        	//console.log('The data has been loaded!')

	        	if ( 'err' in responseText )
	        		$('#singin_form').append('<p class="error_message_auth">' + responseText.err + '</p>')
	        	else
	        		location = responseText.url.slice(0,-1)
	        }
	      }
			}
		}

	// активируем переход к регистрации
		$('#btnShowRegForm').click(function()
		{
			dataForm.initialDataForm('form_register') // Указываем id формы

			$('#login-form').hide()
			$('#reg-form').show()
			$('#reg-form').toggleClass('active')
		})

	// Активируем переход ко входу пользователя
		$('#btnShowLoginForm').click(function()
		{
			$('#reg-form').hide()
			$('#login-form').show()
		})
 
	// При регистрации отправляем данные на сервер
		// Форма должна содержать id самой формы, для inputs должна содержать
		// placeholder, data-name
		// В скрипте указываем, только id формы
		var dataForm = 
		{
			regExpLogin: 				/^[а-яА-ЯёЁa-zA-Z0-9]+$/,
		  regExpPass: 			  /^[а-яА-ЯёЁa-zA-Z0-9\-\_\.\,\+\=]+$/,
		  regExpEmail: 				/^[-\w.]+@([A-z0-9][-A-z0-9]+.)+[A-z]{2,4}$/,

		  initialDataForm(idNameForm)
		  {
		  	var $form_element = document.querySelector('#' + idNameForm)
	  		var fieldsInput = $form_element.querySelectorAll('input')
				var countFieldsInput = fieldsInput.length
				this.dataUser = {}
				this.fields = {}

				for ( var i = 0; i < countFieldsInput; i++ )
				{
					this.dataUser[fieldsInput[i].dataset.name] = ''
					this[fieldsInput[i].dataset.name + 'Placeholder'] = fieldsInput[i].placeholder
					this.fields['$' + fieldsInput[i].dataset.name] = fieldsInput[i]
					this['$' + fieldsInput[i].dataset.name + 'Jquery'] = $(fieldsInput[i])
					
					fieldsInput[i].addEventListener('input', function(event)
					{
						removeErrorMessage()

						errorInputClear(event)

						dataForm.dataUser[event.target.dataset.name] = event.target.value
					}, false)
				}
		  }
		}

		//dataForm.initialDataForm('form_register') // Указываем id формы

		function new_clearInput() 
		{
			dataForm.fields.$login.placeholder 			= dataForm.loginPlaceholder
			dataForm.fields.$password.placeholder 	= dataForm.passwordPlaceholder
			dataForm.fields.$passRepiat.placeholder = dataForm.passRepiatPlaceholder
			dataForm.fields.$email.placeholder			= dataForm.emailPlaceholder
		}

		function new_errorInputClear(event)
		{
			var target = event.target

			if (target.value != "") 
				target.classList.remove("error_input")  
		}

		function new_removeErrorMessage()
		{
			if ( $('.error_message_auth').length )
				$('.error_message_auth').remove()
		}

		function checkRegData(event)
		{
			if ( dataForm.dataUser.login == '' || !dataForm.regExpLogin.test(dataForm.dataUser.login) ) 
			{
				dataForm.$loginJquery.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Вы не ввели логин!</p>')
				dataForm.$loginJquery.val('')
				dataForm.loginPlaceholder
			}
			else if ( dataForm.dataUser.password == '' || !dataForm.regExpPass.test(dataForm.dataUser.password) ) 
			{
				dataForm.$passwordJquery.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Вы не ввели пароль!</p>')
				dataForm.$passwordJquery.val('')
				dataForm.passwordPlaceholder
			}
			else if ( dataForm.dataUser.passRepiat == '' || !dataForm.regExpPass.test(dataForm.dataUser.passRepiat) ) 
			{
				dataForm.$passRepiatJquery.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Вы не ввели пароль!</p>')
				dataForm.$passRepiatJquery.val('')
				dataForm.passRepiatPlaceholder
			}
			else if ( dataForm.dataUser.email == '' || !dataForm.regExpEmail.test(dataForm.dataUser.email) ) 
			{
				dataForm.$emailJquery.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Вы не ввели e-mail!</p>')
				dataForm.$emailJquery.val('')
				dataForm.emailPlaceholder
			}
			else if ( dataForm.dataUser.password.length < 6 )
			{
				dataForm.$passwordJquery.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Сильно короткий пароль!</p>')
				dataForm.$passwordJquery.val('')
				dataForm.$passRepiatJquery.val('')
				dataForm.passwordPlaceholder
				dataForm.passRepiatPlaceholder
			}
			else if ( dataForm.dataUser.password != dataForm.dataUser.passRepiat )
			{
				dataForm.$passwordJquery.addClass('error_input')
				dataForm.$passRepiatJquery.addClass('error_input')
				dataForm.$passwordJquery.val('')
				dataForm.$passRepiatJquery.val('')
				dataForm.passwordPlaceholder
				dataForm.passRepiatPlaceholder
				dataForm.$passRepiatJquery.after('<p class="error_message_auth" style="position: static;">Пароль не совпадает!</p>')
			}
			else
			{
				clearInput()
				send_reg_data()
			}

			event.preventDefault()
		}

		$('#btn_reg_user').on('click', checkRegData)

		function send_reg_data()
		{
			$('#btn_reg_user')
				.off('click')
				.addClass('btn_wait')
				.removeClass('btn-standart-blue')

			var ajaxDataRegister = new XMLHttpRequest()
						
			var fd = new FormData()
			
			ajaxDataRegister.open('POST', ajax_request_object.ajaxurl)
			fd.append('action', 'ajaxuploaddataregister')
			fd.append('login', dataForm.dataUser.login)
			fd.append('password', dataForm.dataUser.password)
			fd.append('passRepiat', dataForm.dataUser.passRepiat)
			fd.append('email', dataForm.dataUser.email)
			fd.append('security', dataForm.fields.$security.value)
			fd.append('curUrl', location.href)

			if (ajaxDataRegister.readyState)
			{
		      	ajaxDataRegister.send(fd)

		        ajaxDataRegister.onreadystatechange = function()
				{
					if (this.readyState != 4) 
						return
				
			        if ( this.status == 200 )
			        {
			        	var responseText = JSON.parse(this.responseText)
			        	//var responseText = this.responseText
			        	
			        	$('#btn_reg_user').on('click', checkRegData)
			      		//  	console.log(responseText)

			        	if ( 'err' in responseText )
			        	{
			        		$('#btn_reg_user')
			        			.removeClass('btn_wait')
			        			.addClass('btn-standart-blue')

			        		if ( 'name' in responseText.err )
			        		{
										dataForm.$loginJquery
											.addClass('error_input')
											.after('<p class="error_message_auth" style="position: static;">' + responseText.err.name + '</p>')
										dataForm.$loginJquery.val('')
										dataForm.loginPlaceholder
			        		}
			        		else
			        		{
			        			dataForm.$emailJquery
			        				.addClass('error_input')
			        				.after('<p class="error_message_auth" style="position: static;">' + responseText.err.email + '</p>')
										dataForm.$emailJquery.val('')
										dataForm.emailPlaceholder
			        		}
			        	}
			        	else
			        	{
			        		send_auth_data(responseText.dataUser)
			        		//location = responseText.url.slice(0,-1)
			        	}
			        }
			    }
			}
		}


// Восстановление пароля
	var copyForm
	$('#link_pass_recovery').on('click', function()
	{
		copyForm = singin_form.cloneNode(true)
		document.querySelector('[name="login_singin"]').removeEventListener('input', listenLoginSingin)
		document.querySelector('[name="pass_singin"]').removeEventListener('input', listenPassSingin)
		$(singin_form).find('label.reg-form-label').text('Восстановление пароля')
		$(singin_form)
			.find('input[type="text"]')
			.attr(
			{
				type: 'email',
				name: 'email_login',
				class: 'form-control border_radius_zero reg-form-margin-form',
				placeholder: 'Почта'
			})
			.attr('data-name', 'email')

		$(singin_form).find('input[type="password"]').remove()
		$(this).remove()

		$(btn_sign_in).off('click')
		$(btn_sign_in).attr('id', 'btn_pass_recovery').text('Отправить')
		  
		dataForm.initialDataForm('singin_form')
		$('#btn_pass_recovery').on('click', checkRecoveryData)
	})

	function recovery_clearInput() 
	{
		dataForm.fields.$email.placeholder			= dataForm.emailPlaceholder
	}

	function checkRecoveryData(event)
	{
		if ( dataForm.dataUser.email == '' || !dataForm.regExpEmail.test(dataForm.dataUser.email) ) 
		{
			dataForm.$emailJquery.addClass('error_input').after('<p class="error_message_auth" style="position: static;">Вы не ввели e-mail!</p>')
			dataForm.$emailJquery.val('')
			//dataForm.emailPlaceholder
		}
		else
		{
			recovery_clearInput()
			send_recovery_data()
		}

		event.preventDefault()
	}

	function send_recovery_data()
	{
		$('#btn_pass_recovery')
			.off('click')
			.addClass('btn_wait')
			.removeClass('btn-standart-blue')

		var ajaxDataRegister = new XMLHttpRequest()
					
		var fd = new FormData()
		
		ajaxDataRegister.open('POST', ajax_request_object.ajaxurl)
		fd.append('action', 'ajaxrecoverypass')
		fd.append('email', dataForm.dataUser.email)
		fd.append('security', dataForm.fields.$security.value)
		fd.append('curUrl', location.href)

		if (ajaxDataRegister.readyState)
		{
	      	ajaxDataRegister.send(fd)

	        ajaxDataRegister.onreadystatechange = function()
			{
				if (this.readyState != 4) 
					return
			
		        if ( this.status == 200 )
		        {
		        	var responseText = JSON.parse(this.responseText)
		        	//var responseText = this.responseText
		        	
		        	//$('#btn_pass_recovery').on('click', checkRegData)
		      		  	console.log(responseText)

	      		  	if ( 'err' in responseText )
	      		  	{
	      		  		dataForm.$emailJquery.after('<p class="error_message_auth" style="position: static;">' + responseText['err'] + '</p>')
	      		  		$(btn_pass_recovery).removeClass('btn_wait')
	      		  		$('#btn_pass_recovery').on('click', checkRecoveryData)
	      		  	}
	      		  	else
	      		  	{
	      		  		$(btn_pass_recovery).on('click', loginUser)
						$(btn_pass_recovery)
						.attr(
						{
							id: 'btn_sign_in'
							//href: location.origin + location.pathname
						})
						.text('Войти')
						.removeClass('btn_wait')

		      		  	$(copyForm).replaceAll($(singin_form))
		      		  	document.querySelector('[name="login_singin"]').addEventListener('input', listenLoginSingin, false)
		      		  	document.querySelector('[name="pass_singin"]').addEventListener('input', listenPassSingin, false)
		      		  	$('[name="pass_singin"]').after($('<p class="success_message">На почту вам отправлен новый пароль!</p>'))
		        		//console.log(location)
		        		//location = responseText.url.slice(0,-1)	
	      		  	}
		        }
		    }
		}
	}

// Обычный поиск
	function activate_search_page( strSearch )
	{
		location = location.origin + '?s=' + strSearch
	}

	function setDataSearch(data)
	{
		var objLength = data.length
		var dataLength = 0
		var newData = []
		var count = 0;
		var searchValue = $('.wr_header_menu_and_search').find('#auxiliary_search').val()

		for ( var i = 0; i < objLength; i++ )
		{
			dataLength = data[i].data.length

			for ( var j = 0; j < dataLength; j++ )
			{

				newData[count++] = data[i].data[j]

			}
		}

		$('.wr_header_menu_and_search').find('#auxiliary_search').hide()

		$('#select-tools').selectize({
			maxItems: 1,
			valueField: 'guid',
			labelField: 'post_title',
			searchField: 'post_title',
			options: newData,
			create: false
		});

		if ( searchValue )
		{
			$('.wr_header_menu_and_search').find('.selectize-input').click()
			
			setTimeout(function()
			{
				$('.wr_header_menu_and_search').find('#select-tools-selectized')
					.val(searchValue)
					.width('100%')
					.attr('data-find', searchValue)
			}, 1000)
		}

		$('.wr_header_menu_and_search').find("select").change(function()
		{
			location = $('.wr_header_menu_and_search').find('select option:selected').attr('value')	
		})

		$('.wr_header_menu_and_search').find('#select-tools-selectized').on('input', function()
		{
			this.dataset.find = this.value
		})
	}

	sendRequestSearch.index = 0
	sendRequestSearch.array = []
	function sendRequestSearch(fd)
	{
		var xhr = new XMLHttpRequest()
		
		xhr.open('POST', ajax_request_object.ajaxurl)

		if (xhr.readyState)
		{
			xhr.send(fd)

			xhr.onreadystatechange = function()
			{
				if (this.readyState != 4) 
					return
			
		        if ( this.status == 200 )
		        {
		        	$('.wr_header_menu_and_search').css('overflow', '')
		        	
		        	var posts = JSON.parse(this.responseText)
		       		 //	console.log(posts)

		       		sendRequestSearch.array[sendRequestSearch.index++] = posts

		        	if ( posts.nextRequest )
		        	{
		        		fd.append('offset', posts.offset)
		        		sendRequestSearch(fd)
		        	}
		        	else
		        		setDataSearch(sendRequestSearch.array)
		        }
		    }
		}
	}

	$('#search').on('click', function()
	{
		var fd = new FormData()

		fd.append('action', 'ajaxpoststіtle')
		fd.append('offset', 0)

		sendRequestSearch(fd)
	})

	$('#btn_search').on('click', function()
	{
		// console.log($('#select-tools-selectized').attr('data-find'))
		// console.log($('#auxiliary_search').val())
		if ( $('#select-tools-selectized').attr('data-find') != undefined )
			activate_search_page( $('#select-tools-selectized').attr('data-find') )
		else if ( $('#auxiliary_search').val() != undefined )
			activate_search_page( $('#auxiliary_search').val() )
		else
			activate_search_page( ' ' )
	})
	
// Кнопка "Еще статьи"
	// активируем кнопку "Еще статьи"
		$('#btnMorePosts').on('click', function(event)
		{
			sendDataMorePosts(
			{
				id: this.id,
				maxcountposts: this.dataset.maxcountposts, 
				offset: this.dataset.offset,
				posttype: this.dataset.posttype,
				countposts: this.dataset.countposts
			})

			event.preventDefault();
		})

	 $(window).load(function () {
	 			var header = $('header');
			 	var regForm = header.find('#reg-form');
			 	var loginForm = header.find('#login-form');
			 	var profileform = header.find('#profile-form');
			 	var mobileMenu = $('.main_phone_menu');


	            if ($(window).width() <= '768'){
	            	regForm.appendTo(mobileMenu);
					loginForm.appendTo(mobileMenu);
					profileform.appendTo(mobileMenu);

					$('.header-close-arrow').remove();
	       		}
		});	

	//Функция перемещает блоки регистрации в мобильное меню
		 $(window).resize(function() {
		 	var header = $('header');
		 	var regForm = header.find('#reg-form');
		 	var loginForm = header.find('#login-form');
		 	var profileform = header.find('#profile-form');


		 	var mobileMenu = $('.main_phone_menu');

	            if ($(window).width() <= '768'){
	            	regForm.appendTo(mobileMenu);
					loginForm.appendTo(mobileMenu);
					profileform.appendTo(mobileMenu);

					$('.header-close-arrow').remove();
	       		}

		});

	//Устанавливаем картинки на стрелочки карусели
		$('.carousel').carousel()

		$('#myCarousel').on('slid.bs.carousel', function()
		{
			$(this).find('.item.active').children('img').attr('src')

			var itemLength = $(this).find('.item').length
			var imgPrev = '', imgNext = ''

			if ($(this).find('.item.active').prev().length)		
				imgPrev = $(this).find('.item.active').prev().children('img').attr('src')
			else
				imgPrev = $($(this).find('.item')[itemLength-1]).children('img').attr('src')
				

			if ($(this).find('.item.active').next().length)		
				imgNext = $(this).find('.item.active').next().children('img').attr('src')
			else
				imgNext = $($(this).find('.item')[0]).children('img').attr('src')

			$('.main-slider-controlers.left').css(
			{
				backgroundImage: 'url(' + imgPrev + ')',
			})

			$('.main-slider-controlers.right').css(
			{
				backgroundImage: 'url(' + imgNext + ')',
			})
		})
});


